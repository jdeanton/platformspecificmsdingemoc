/**
 */
package TAM.TAM_Task;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Mem Req</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.TamMemReq#getAccessFreq <em>Access Freq</em>}</li>
 *   <li>{@link TAM.TAM_Task.TamMemReq#getTransfMem <em>Transf Mem</em>}</li>
 *   <li>{@link TAM.TAM_Task.TamMemReq#getMem <em>Mem</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Task.TAM_TaskPackage#getTamMemReq()
 * @model
 * @generated
 */
public interface TamMemReq extends ResourceUsage {
	/**
	 * Returns the value of the '<em><b>Access Freq</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Freq</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Freq</em>' attribute.
	 * @see #setAccessFreq(String)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamMemReq_AccessFreq()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getAccessFreq();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamMemReq#getAccessFreq <em>Access Freq</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Freq</em>' attribute.
	 * @see #getAccessFreq()
	 * @generated
	 */
	void setAccessFreq(String value);

	/**
	 * Returns the value of the '<em><b>Transf Mem</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transf Mem</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transf Mem</em>' attribute.
	 * @see #setTransfMem(String)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamMemReq_TransfMem()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getTransfMem();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamMemReq#getTransfMem <em>Transf Mem</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transf Mem</em>' attribute.
	 * @see #getTransfMem()
	 * @generated
	 */
	void setTransfMem(String value);

	/**
	 * Returns the value of the '<em><b>Mem</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mem</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mem</em>' reference.
	 * @see #setMem(TamMemType)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamMemReq_Mem()
	 * @model ordered="false"
	 * @generated
	 */
	TamMemType getMem();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamMemReq#getMem <em>Mem</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mem</em>' reference.
	 * @see #getMem()
	 * @generated
	 */
	void setMem(TamMemType value);

} // TamMemReq
