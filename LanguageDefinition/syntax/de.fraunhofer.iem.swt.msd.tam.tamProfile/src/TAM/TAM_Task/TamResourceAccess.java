/**
 */
package TAM.TAM_Task;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Resource Access</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.TamResourceAccess#getNumAccess <em>Num Access</em>}</li>
 *   <li>{@link TAM.TAM_Task.TamResourceAccess#getPerResource <em>Per Resource</em>}</li>
 *   <li>{@link TAM.TAM_Task.TamResourceAccess#getOsResource <em>Os Resource</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Task.TAM_TaskPackage#getTamResourceAccess()
 * @model
 * @generated
 */
public interface TamResourceAccess extends ResourceUsage {
	/**
	 * Returns the value of the '<em><b>Num Access</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Access</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Access</em>' attribute.
	 * @see #setNumAccess(String)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamResourceAccess_NumAccess()
	 * @model default="1" dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getNumAccess();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamResourceAccess#getNumAccess <em>Num Access</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Access</em>' attribute.
	 * @see #getNumAccess()
	 * @generated
	 */
	void setNumAccess(String value);

	/**
	 * Returns the value of the '<em><b>Per Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Per Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Per Resource</em>' reference.
	 * @see #setPerResource(TamPeripheryType)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamResourceAccess_PerResource()
	 * @model ordered="false"
	 * @generated
	 */
	TamPeripheryType getPerResource();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamResourceAccess#getPerResource <em>Per Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Per Resource</em>' reference.
	 * @see #getPerResource()
	 * @generated
	 */
	void setPerResource(TamPeripheryType value);

	/**
	 * Returns the value of the '<em><b>Os Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Os Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os Resource</em>' reference.
	 * @see #setOsResource(TamSharedOSResourcType)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamResourceAccess_OsResource()
	 * @model ordered="false"
	 * @generated
	 */
	TamSharedOSResourcType getOsResource();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamResourceAccess#getOsResource <em>Os Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os Resource</em>' reference.
	 * @see #getOsResource()
	 * @generated
	 */
	void setOsResource(TamSharedOSResourcType value);

} // TamResourceAccess
