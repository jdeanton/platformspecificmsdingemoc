/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam OS Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getMemFootprint <em>Mem Footprint</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getBackgroundUtilization <em>Background Utilization</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamOSResource()
 * @model
 * @generated
 */
public interface TamOSResource extends Resource {
	/**
	 * Returns the value of the '<em><b>Mem Footprint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mem Footprint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mem Footprint</em>' attribute.
	 * @see #setMemFootprint(String)
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamOSResource_MemFootprint()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getMemFootprint();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getMemFootprint <em>Mem Footprint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mem Footprint</em>' attribute.
	 * @see #getMemFootprint()
	 * @generated
	 */
	void setMemFootprint(String value);

	/**
	 * Returns the value of the '<em><b>Background Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Background Utilization</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Background Utilization</em>' attribute.
	 * @see #setBackgroundUtilization(String)
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamOSResource_BackgroundUtilization()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getBackgroundUtilization();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getBackgroundUtilization <em>Background Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Background Utilization</em>' attribute.
	 * @see #getBackgroundUtilization()
	 * @generated
	 */
	void setBackgroundUtilization(String value);

} // TamOSResource
