/**
 */
package TAM.TAM_Platform.TAM_Communication.impl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;
import TAM.TAM_Platform.TAM_Communication.TamComConnection;
import TAM.TAM_Platform.TAM_Communication.TamComInterface;
import TAM.TAM_Platform.TAM_Communication.TamComService;
import TAM.TAM_Platform.TAM_Communication.TamProtocol;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaCommHostImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Com Connection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl#getNetworkOvhd <em>Network Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl#getNetworkBlockT <em>Network Block T</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl#getUsedProtocol <em>Used Protocol</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl#getUsedComServices <em>Used Com Services</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl#getInterface1 <em>Interface1</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl#getInterface2 <em>Interface2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamComConnectionImpl extends GaCommHostImpl implements TamComConnection {
	/**
	 * The default value of the '{@link #getNetworkOvhd() <em>Network Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkOvhd()
	 * @generated
	 * @ordered
	 */
	protected static final String NETWORK_OVHD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNetworkOvhd() <em>Network Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkOvhd()
	 * @generated
	 * @ordered
	 */
	protected String networkOvhd = NETWORK_OVHD_EDEFAULT;

	/**
	 * The default value of the '{@link #getNetworkBlockT() <em>Network Block T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkBlockT()
	 * @generated
	 * @ordered
	 */
	protected static final String NETWORK_BLOCK_T_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNetworkBlockT() <em>Network Block T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkBlockT()
	 * @generated
	 * @ordered
	 */
	protected String networkBlockT = NETWORK_BLOCK_T_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<TamComInterface> interfaces;

	/**
	 * The cached value of the '{@link #getUsedProtocol() <em>Used Protocol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsedProtocol()
	 * @generated
	 * @ordered
	 */
	protected TamProtocol usedProtocol;

	/**
	 * The cached value of the '{@link #getUsedComServices() <em>Used Com Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsedComServices()
	 * @generated
	 * @ordered
	 */
	protected EList<TamComService> usedComServices;

	/**
	 * The cached value of the '{@link #getInterface1() <em>Interface1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterface1()
	 * @generated
	 * @ordered
	 */
	protected TamComInterface interface1;

	/**
	 * The cached value of the '{@link #getInterface2() <em>Interface2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterface2()
	 * @generated
	 * @ordered
	 */
	protected TamComInterface interface2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamComConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_CommunicationPackage.Literals.TAM_COM_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNetworkOvhd() {
		return networkOvhd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNetworkOvhd(String newNetworkOvhd) {
		String oldNetworkOvhd = networkOvhd;
		networkOvhd = newNetworkOvhd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_OVHD, oldNetworkOvhd, networkOvhd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNetworkBlockT() {
		return networkBlockT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNetworkBlockT(String newNetworkBlockT) {
		String oldNetworkBlockT = networkBlockT;
		networkBlockT = newNetworkBlockT;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_BLOCK_T, oldNetworkBlockT, networkBlockT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamComInterface> getInterfaces() {
		if (interfaces == null) {
			interfaces = new EObjectResolvingEList<TamComInterface>(TamComInterface.class, this, TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACES);
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamProtocol getUsedProtocol() {
		if (usedProtocol != null && usedProtocol.eIsProxy()) {
			InternalEObject oldUsedProtocol = (InternalEObject)usedProtocol;
			usedProtocol = (TamProtocol)eResolveProxy(oldUsedProtocol);
			if (usedProtocol != oldUsedProtocol) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_PROTOCOL, oldUsedProtocol, usedProtocol));
			}
		}
		return usedProtocol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamProtocol basicGetUsedProtocol() {
		return usedProtocol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsedProtocol(TamProtocol newUsedProtocol) {
		TamProtocol oldUsedProtocol = usedProtocol;
		usedProtocol = newUsedProtocol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_PROTOCOL, oldUsedProtocol, usedProtocol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamComService> getUsedComServices() {
		if (usedComServices == null) {
			usedComServices = new EObjectResolvingEList<TamComService>(TamComService.class, this, TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_COM_SERVICES);
		}
		return usedComServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComInterface getInterface1() {
		if (interface1 != null && interface1.eIsProxy()) {
			InternalEObject oldInterface1 = (InternalEObject)interface1;
			interface1 = (TamComInterface)eResolveProxy(oldInterface1);
			if (interface1 != oldInterface1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE1, oldInterface1, interface1));
			}
		}
		return interface1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComInterface basicGetInterface1() {
		return interface1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterface1(TamComInterface newInterface1) {
		TamComInterface oldInterface1 = interface1;
		interface1 = newInterface1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE1, oldInterface1, interface1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComInterface getInterface2() {
		if (interface2 != null && interface2.eIsProxy()) {
			InternalEObject oldInterface2 = (InternalEObject)interface2;
			interface2 = (TamComInterface)eResolveProxy(oldInterface2);
			if (interface2 != oldInterface2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE2, oldInterface2, interface2));
			}
		}
		return interface2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComInterface basicGetInterface2() {
		return interface2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterface2(TamComInterface newInterface2) {
		TamComInterface oldInterface2 = interface2;
		interface2 = newInterface2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE2, oldInterface2, interface2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_OVHD:
				return getNetworkOvhd();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_BLOCK_T:
				return getNetworkBlockT();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACES:
				return getInterfaces();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_PROTOCOL:
				if (resolve) return getUsedProtocol();
				return basicGetUsedProtocol();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_COM_SERVICES:
				return getUsedComServices();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE1:
				if (resolve) return getInterface1();
				return basicGetInterface1();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE2:
				if (resolve) return getInterface2();
				return basicGetInterface2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_OVHD:
				setNetworkOvhd((String)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_BLOCK_T:
				setNetworkBlockT((String)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACES:
				getInterfaces().clear();
				getInterfaces().addAll((Collection<? extends TamComInterface>)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_PROTOCOL:
				setUsedProtocol((TamProtocol)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_COM_SERVICES:
				getUsedComServices().clear();
				getUsedComServices().addAll((Collection<? extends TamComService>)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE1:
				setInterface1((TamComInterface)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE2:
				setInterface2((TamComInterface)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_OVHD:
				setNetworkOvhd(NETWORK_OVHD_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_BLOCK_T:
				setNetworkBlockT(NETWORK_BLOCK_T_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACES:
				getInterfaces().clear();
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_PROTOCOL:
				setUsedProtocol((TamProtocol)null);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_COM_SERVICES:
				getUsedComServices().clear();
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE1:
				setInterface1((TamComInterface)null);
				return;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE2:
				setInterface2((TamComInterface)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_OVHD:
				return NETWORK_OVHD_EDEFAULT == null ? networkOvhd != null : !NETWORK_OVHD_EDEFAULT.equals(networkOvhd);
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_BLOCK_T:
				return NETWORK_BLOCK_T_EDEFAULT == null ? networkBlockT != null : !NETWORK_BLOCK_T_EDEFAULT.equals(networkBlockT);
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACES:
				return interfaces != null && !interfaces.isEmpty();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_PROTOCOL:
				return usedProtocol != null;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_COM_SERVICES:
				return usedComServices != null && !usedComServices.isEmpty();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE1:
				return interface1 != null;
			case TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE2:
				return interface2 != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (networkOvhd: ");
		result.append(networkOvhd);
		result.append(", networkBlockT: ");
		result.append(networkBlockT);
		result.append(')');
		return result.toString();
	}

} //TamComConnectionImpl
