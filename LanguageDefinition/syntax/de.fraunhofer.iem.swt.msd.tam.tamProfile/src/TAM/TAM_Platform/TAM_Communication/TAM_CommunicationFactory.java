/**
 */
package TAM.TAM_Platform.TAM_Communication;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage
 * @generated
 */
public interface TAM_CommunicationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_CommunicationFactory eINSTANCE = TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Tam Com Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Com Interface</em>'.
	 * @generated
	 */
	TamComInterface createTamComInterface();

	/**
	 * Returns a new object of class '<em>Tam Protocol</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Protocol</em>'.
	 * @generated
	 */
	TamProtocol createTamProtocol();

	/**
	 * Returns a new object of class '<em>Tam Com Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Com Service</em>'.
	 * @generated
	 */
	TamComService createTamComService();

	/**
	 * Returns a new object of class '<em>Tam Com Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Com Connection</em>'.
	 * @generated
	 */
	TamComConnection createTamComConnection();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TAM_CommunicationPackage getTAM_CommunicationPackage();

} //TAM_CommunicationFactory
