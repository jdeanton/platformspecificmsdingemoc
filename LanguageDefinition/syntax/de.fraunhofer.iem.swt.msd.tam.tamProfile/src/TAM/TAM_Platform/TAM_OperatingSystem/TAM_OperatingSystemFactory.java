/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage
 * @generated
 */
public interface TAM_OperatingSystemFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_OperatingSystemFactory eINSTANCE = TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Tam OS Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam OS Resource</em>'.
	 * @generated
	 */
	TamOSResource createTamOSResource();

	/**
	 * Returns a new object of class '<em>Tam Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Scheduler</em>'.
	 * @generated
	 */
	TamScheduler createTamScheduler();

	/**
	 * Returns a new object of class '<em>Tam OS Com Channel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam OS Com Channel</em>'.
	 * @generated
	 */
	TamOSComChannel createTamOSComChannel();

	/**
	 * Returns a new object of class '<em>Tam Shared OS Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Shared OS Resource</em>'.
	 * @generated
	 */
	TamSharedOSResource createTamSharedOSResource();

	/**
	 * Returns a new object of class '<em>Tam OS Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam OS Service</em>'.
	 * @generated
	 */
	TamOSService createTamOSService();

	/**
	 * Returns a new object of class '<em>Tam RTOS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam RTOS</em>'.
	 * @generated
	 */
	TamRTOS createTamRTOS();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TAM_OperatingSystemPackage getTAM_OperatingSystemPackage();

} //TAM_OperatingSystemFactory
