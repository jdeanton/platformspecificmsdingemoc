/**
 */
package TAM.TAM_Platform.TAM_ControlUnit.impl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;
import TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit;

import TAM.TAM_Task.TamMemType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.StorageResourceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Memory Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl#getMemorySize <em>Memory Size</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl#getThroughput <em>Throughput</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl#getType <em>Type</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl#getAccessDelay <em>Access Delay</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamMemoryUnitImpl extends StorageResourceImpl implements TamMemoryUnit {
	/**
	 * The default value of the '{@link #getMemorySize() <em>Memory Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemorySize()
	 * @generated
	 * @ordered
	 */
	protected static final String MEMORY_SIZE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMemorySize() <em>Memory Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemorySize()
	 * @generated
	 * @ordered
	 */
	protected String memorySize = MEMORY_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getThroughput() <em>Throughput</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThroughput()
	 * @generated
	 * @ordered
	 */
	protected static final String THROUGHPUT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getThroughput() <em>Throughput</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThroughput()
	 * @generated
	 * @ordered
	 */
	protected String throughput = THROUGHPUT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected TamMemType type;

	/**
	 * The default value of the '{@link #getAccessDelay() <em>Access Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessDelay()
	 * @generated
	 * @ordered
	 */
	protected static final String ACCESS_DELAY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAccessDelay() <em>Access Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessDelay()
	 * @generated
	 * @ordered
	 */
	protected String accessDelay = ACCESS_DELAY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamMemoryUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_ControlUnitPackage.Literals.TAM_MEMORY_UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMemorySize() {
		return memorySize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemorySize(String newMemorySize) {
		String oldMemorySize = memorySize;
		memorySize = newMemorySize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_MEMORY_UNIT__MEMORY_SIZE, oldMemorySize, memorySize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getThroughput() {
		return throughput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThroughput(String newThroughput) {
		String oldThroughput = throughput;
		throughput = newThroughput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_MEMORY_UNIT__THROUGHPUT, oldThroughput, throughput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamMemType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (TamMemType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_ControlUnitPackage.TAM_MEMORY_UNIT__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamMemType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(TamMemType newType) {
		TamMemType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_MEMORY_UNIT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAccessDelay() {
		return accessDelay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessDelay(String newAccessDelay) {
		String oldAccessDelay = accessDelay;
		accessDelay = newAccessDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_MEMORY_UNIT__ACCESS_DELAY, oldAccessDelay, accessDelay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__MEMORY_SIZE:
				return getMemorySize();
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__THROUGHPUT:
				return getThroughput();
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__ACCESS_DELAY:
				return getAccessDelay();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__MEMORY_SIZE:
				setMemorySize((String)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__THROUGHPUT:
				setThroughput((String)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__TYPE:
				setType((TamMemType)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__ACCESS_DELAY:
				setAccessDelay((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__MEMORY_SIZE:
				setMemorySize(MEMORY_SIZE_EDEFAULT);
				return;
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__THROUGHPUT:
				setThroughput(THROUGHPUT_EDEFAULT);
				return;
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__TYPE:
				setType((TamMemType)null);
				return;
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__ACCESS_DELAY:
				setAccessDelay(ACCESS_DELAY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__MEMORY_SIZE:
				return MEMORY_SIZE_EDEFAULT == null ? memorySize != null : !MEMORY_SIZE_EDEFAULT.equals(memorySize);
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__THROUGHPUT:
				return THROUGHPUT_EDEFAULT == null ? throughput != null : !THROUGHPUT_EDEFAULT.equals(throughput);
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__TYPE:
				return type != null;
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT__ACCESS_DELAY:
				return ACCESS_DELAY_EDEFAULT == null ? accessDelay != null : !ACCESS_DELAY_EDEFAULT.equals(accessDelay);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (memorySize: ");
		result.append(memorySize);
		result.append(", throughput: ");
		result.append(throughput);
		result.append(", accessDelay: ");
		result.append(accessDelay);
		result.append(')');
		return result.toString();
	}

} //TamMemoryUnitImpl
