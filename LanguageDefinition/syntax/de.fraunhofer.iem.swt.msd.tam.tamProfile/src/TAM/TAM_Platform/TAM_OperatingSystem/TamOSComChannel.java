/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;

import TAM.TAM_Platform.TAM_Communication.TamComConnection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam OS Com Channel</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamOSComChannel()
 * @model
 * @generated
 */
public interface TamOSComChannel extends TamComConnection, TamOSResource {
} // TamOSComChannel
