/**
 */
package TAM.TAM_AnalysisContext.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;
import TAM.TAM_AnalysisContext.TamWorkloadBehavior;

import TAM.TAM_AnalysisContext.util.TAM_AnalysisContextValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaWorkloadBehaviorImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Workload Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TamWorkloadBehaviorImpl extends GaWorkloadBehaviorImpl implements TamWorkloadBehavior {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamWorkloadBehaviorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_AnalysisContextPackage.Literals.TAM_WORKLOAD_BEHAVIOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean Constraint1(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TAM_AnalysisContextValidator.DIAGNOSTIC_SOURCE,
						 TAM_AnalysisContextValidator.TAM_WORKLOAD_BEHAVIOR__CONSTRAINT1,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "Constraint1", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case TAM_AnalysisContextPackage.TAM_WORKLOAD_BEHAVIOR___CONSTRAINT1__DIAGNOSTICCHAIN_MAP:
				return Constraint1((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //TamWorkloadBehaviorImpl
