/**
 */
package TAM.TAM_Platform.TAM_Communication.impl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;
import TAM.TAM_Platform.TAM_Communication.TamComService;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Com Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TamComServiceImpl extends TamProtocolImpl implements TamComService {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamComServiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_CommunicationPackage.Literals.TAM_COM_SERVICE;
	}

} //TamComServiceImpl
