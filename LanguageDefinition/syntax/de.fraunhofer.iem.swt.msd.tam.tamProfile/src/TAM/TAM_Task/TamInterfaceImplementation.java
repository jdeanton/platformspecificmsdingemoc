/**
 */
package TAM.TAM_Task;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Interface Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.TamInterfaceImplementation#getBase_Port <em>Base Port</em>}</li>
 *   <li>{@link TAM.TAM_Task.TamInterfaceImplementation#getImplExecOvhd <em>Impl Exec Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Task.TamInterfaceImplementation#getImplMemOvhd <em>Impl Mem Ovhd</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Task.TAM_TaskPackage#getTamInterfaceImplementation()
 * @model
 * @generated
 */
public interface TamInterfaceImplementation extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Port</em>' reference.
	 * @see #setBase_Port(Port)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamInterfaceImplementation_Base_Port()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Port getBase_Port();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamInterfaceImplementation#getBase_Port <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Port</em>' reference.
	 * @see #getBase_Port()
	 * @generated
	 */
	void setBase_Port(Port value);

	/**
	 * Returns the value of the '<em><b>Impl Exec Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impl Exec Ovhd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impl Exec Ovhd</em>' attribute.
	 * @see #setImplExecOvhd(String)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamInterfaceImplementation_ImplExecOvhd()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getImplExecOvhd();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamInterfaceImplementation#getImplExecOvhd <em>Impl Exec Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impl Exec Ovhd</em>' attribute.
	 * @see #getImplExecOvhd()
	 * @generated
	 */
	void setImplExecOvhd(String value);

	/**
	 * Returns the value of the '<em><b>Impl Mem Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impl Mem Ovhd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impl Mem Ovhd</em>' attribute.
	 * @see #setImplMemOvhd(String)
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamInterfaceImplementation_ImplMemOvhd()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getImplMemOvhd();

	/**
	 * Sets the value of the '{@link TAM.TAM_Task.TamInterfaceImplementation#getImplMemOvhd <em>Impl Mem Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impl Mem Ovhd</em>' attribute.
	 * @see #getImplMemOvhd()
	 * @generated
	 */
	void setImplMemOvhd(String value);

} // TamInterfaceImplementation
