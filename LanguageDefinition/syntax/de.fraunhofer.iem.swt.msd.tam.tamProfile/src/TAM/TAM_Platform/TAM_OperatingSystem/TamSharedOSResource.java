/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;

import TAM.TAM_Task.TamSharedOSResourcType;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.MutualExclusionResource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Shared OS Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getAccessDelay <em>Access Delay</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamSharedOSResource()
 * @model
 * @generated
 */
public interface TamSharedOSResource extends MutualExclusionResource, TamOSResource {
	/**
	 * Returns the value of the '<em><b>Access Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Delay</em>' attribute.
	 * @see #setAccessDelay(String)
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamSharedOSResource_AccessDelay()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getAccessDelay();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getAccessDelay <em>Access Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Delay</em>' attribute.
	 * @see #getAccessDelay()
	 * @generated
	 */
	void setAccessDelay(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(TamSharedOSResourcType)
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamSharedOSResource_Type()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	TamSharedOSResourcType getType();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(TamSharedOSResourcType value);

} // TamSharedOSResource
