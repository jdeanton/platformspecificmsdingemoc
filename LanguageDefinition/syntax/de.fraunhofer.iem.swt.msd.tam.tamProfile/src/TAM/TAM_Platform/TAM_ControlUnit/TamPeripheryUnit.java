/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;

import TAM.TAM_Task.TamPeripheryType;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Periphery Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getDelay <em>Delay</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamPeripheryUnit()
 * @model abstract="true"
 * @generated
 */
public interface TamPeripheryUnit extends Resource {
	/**
	 * Returns the value of the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay</em>' attribute.
	 * @see #setDelay(String)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamPeripheryUnit_Delay()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getDelay();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getDelay <em>Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay</em>' attribute.
	 * @see #getDelay()
	 * @generated
	 */
	void setDelay(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(TamPeripheryType)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamPeripheryUnit_Type()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	TamPeripheryType getType();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(TamPeripheryType value);

} // TamPeripheryUnit
