/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Scheduler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler#getTaskSwitchT <em>Task Switch T</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamScheduler()
 * @model
 * @generated
 */
public interface TamScheduler extends Scheduler, TamOSResource {
	/**
	 * Returns the value of the '<em><b>Task Switch T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Switch T</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Switch T</em>' attribute.
	 * @see #setTaskSwitchT(String)
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamScheduler_TaskSwitchT()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getTaskSwitchT();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler#getTaskSwitchT <em>Task Switch T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Switch T</em>' attribute.
	 * @see #getTaskSwitchT()
	 * @generated
	 */
	void setTaskSwitchT(String value);

} // TamScheduler
