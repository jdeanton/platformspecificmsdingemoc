/**
 */
package TAM.TAM_Platform.TAM_Communication;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationFactory
 * @model kind="package"
 * @generated
 */
public interface TAM_CommunicationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TAM_Communication";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///TAM/TAM_Platform/TAM_Communication.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "TAM.TAM_Platform.TAM_Communication";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_CommunicationPackage eINSTANCE = TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl.init();

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl <em>Tam Com Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamComInterface()
	 * @generated
	 */
	int TAM_COM_INTERFACE = 0;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__RES_MULT = GQAMPackage.GA_EXEC_HOST__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__IS_PROTECTED = GQAMPackage.GA_EXEC_HOST__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__IS_ACTIVE = GQAMPackage.GA_EXEC_HOST__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__BASE_PROPERTY = GQAMPackage.GA_EXEC_HOST__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__BASE_INSTANCE_SPECIFICATION = GQAMPackage.GA_EXEC_HOST__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__BASE_CLASSIFIER = GQAMPackage.GA_EXEC_HOST__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__BASE_LIFELINE = GQAMPackage.GA_EXEC_HOST__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__BASE_CONNECTABLE_ELEMENT = GQAMPackage.GA_EXEC_HOST__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Preemptible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__IS_PREEMPTIBLE = GQAMPackage.GA_EXEC_HOST__IS_PREEMPTIBLE;

	/**
	 * The feature id for the '<em><b>Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__SCHED_POLICY = GQAMPackage.GA_EXEC_HOST__SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Other Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__OTHER_SCHED_POLICY = GQAMPackage.GA_EXEC_HOST__OTHER_SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__SCHEDULE = GQAMPackage.GA_EXEC_HOST__SCHEDULE;

	/**
	 * The feature id for the '<em><b>Processing Units</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__PROCESSING_UNITS = GQAMPackage.GA_EXEC_HOST__PROCESSING_UNITS;

	/**
	 * The feature id for the '<em><b>Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__HOST = GQAMPackage.GA_EXEC_HOST__HOST;

	/**
	 * The feature id for the '<em><b>Protected Shared Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__PROTECTED_SHARED_RESOURCES = GQAMPackage.GA_EXEC_HOST__PROTECTED_SHARED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Schedulable Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__SCHEDULABLE_RESOURCES = GQAMPackage.GA_EXEC_HOST__SCHEDULABLE_RESOURCES;

	/**
	 * The feature id for the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__SPEED_FACTOR = GQAMPackage.GA_EXEC_HOST__SPEED_FACTOR;

	/**
	 * The feature id for the '<em><b>Main Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__MAIN_SCHEDULER = GQAMPackage.GA_EXEC_HOST__MAIN_SCHEDULER;

	/**
	 * The feature id for the '<em><b>Comm Tx Ovh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__COMM_TX_OVH = GQAMPackage.GA_EXEC_HOST__COMM_TX_OVH;

	/**
	 * The feature id for the '<em><b>Comm Rcv Ovh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__COMM_RCV_OVH = GQAMPackage.GA_EXEC_HOST__COMM_RCV_OVH;

	/**
	 * The feature id for the '<em><b>Cntxt Sw T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__CNTXT_SW_T = GQAMPackage.GA_EXEC_HOST__CNTXT_SW_T;

	/**
	 * The feature id for the '<em><b>Clock Ovh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__CLOCK_OVH = GQAMPackage.GA_EXEC_HOST__CLOCK_OVH;

	/**
	 * The feature id for the '<em><b>Sched Pri Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__SCHED_PRI_RANGE = GQAMPackage.GA_EXEC_HOST__SCHED_PRI_RANGE;

	/**
	 * The feature id for the '<em><b>Mem Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__MEM_SIZE = GQAMPackage.GA_EXEC_HOST__MEM_SIZE;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__UTILIZATION = GQAMPackage.GA_EXEC_HOST__UTILIZATION;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__THROUGHPUT = GQAMPackage.GA_EXEC_HOST__THROUGHPUT;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__DURATION = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Periodic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__IS_PERIODIC = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Arbitration T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__ARBITRATION_T = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE__CONNECTION = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Tam Com Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE_FEATURE_COUNT = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Tam Com Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_INTERFACE_OPERATION_COUNT = GQAMPackage.GA_EXEC_HOST_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl <em>Tam Protocol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamProtocol()
	 * @generated
	 */
	int TAM_PROTOCOL = 1;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__RES_MULT = GQAMPackage.GA_COMM_CHANNEL__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__IS_PROTECTED = GQAMPackage.GA_COMM_CHANNEL__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__IS_ACTIVE = GQAMPackage.GA_COMM_CHANNEL__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__BASE_PROPERTY = GQAMPackage.GA_COMM_CHANNEL__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__BASE_INSTANCE_SPECIFICATION = GQAMPackage.GA_COMM_CHANNEL__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__BASE_CLASSIFIER = GQAMPackage.GA_COMM_CHANNEL__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__BASE_LIFELINE = GQAMPackage.GA_COMM_CHANNEL__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__BASE_CONNECTABLE_ELEMENT = GQAMPackage.GA_COMM_CHANNEL__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Sched Params</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__SCHED_PARAMS = GQAMPackage.GA_COMM_CHANNEL__SCHED_PARAMS;

	/**
	 * The feature id for the '<em><b>Dependent Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__DEPENDENT_SCHEDULER = GQAMPackage.GA_COMM_CHANNEL__DEPENDENT_SCHEDULER;

	/**
	 * The feature id for the '<em><b>Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__HOST = GQAMPackage.GA_COMM_CHANNEL__HOST;

	/**
	 * The feature id for the '<em><b>Packet Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__PACKET_SIZE = GQAMPackage.GA_COMM_CHANNEL__PACKET_SIZE;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__UTILIZATION = GQAMPackage.GA_COMM_CHANNEL__UTILIZATION;

	/**
	 * The feature id for the '<em><b>Transm Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__TRANSM_OVHD = GQAMPackage.GA_COMM_CHANNEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Msg Ctrl Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__MSG_CTRL_OVHD = GQAMPackage.GA_COMM_CHANNEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Comm Tx T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__COMM_TX_T = GQAMPackage.GA_COMM_CHANNEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Comm Rcv T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL__COMM_RCV_T = GQAMPackage.GA_COMM_CHANNEL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Tam Protocol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL_FEATURE_COUNT = GQAMPackage.GA_COMM_CHANNEL_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Tam Protocol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROTOCOL_OPERATION_COUNT = GQAMPackage.GA_COMM_CHANNEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamComServiceImpl <em>Tam Com Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TamComServiceImpl
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamComService()
	 * @generated
	 */
	int TAM_COM_SERVICE = 2;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__RES_MULT = TAM_PROTOCOL__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__IS_PROTECTED = TAM_PROTOCOL__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__IS_ACTIVE = TAM_PROTOCOL__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__BASE_PROPERTY = TAM_PROTOCOL__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__BASE_INSTANCE_SPECIFICATION = TAM_PROTOCOL__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__BASE_CLASSIFIER = TAM_PROTOCOL__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__BASE_LIFELINE = TAM_PROTOCOL__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__BASE_CONNECTABLE_ELEMENT = TAM_PROTOCOL__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Sched Params</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__SCHED_PARAMS = TAM_PROTOCOL__SCHED_PARAMS;

	/**
	 * The feature id for the '<em><b>Dependent Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__DEPENDENT_SCHEDULER = TAM_PROTOCOL__DEPENDENT_SCHEDULER;

	/**
	 * The feature id for the '<em><b>Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__HOST = TAM_PROTOCOL__HOST;

	/**
	 * The feature id for the '<em><b>Packet Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__PACKET_SIZE = TAM_PROTOCOL__PACKET_SIZE;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__UTILIZATION = TAM_PROTOCOL__UTILIZATION;

	/**
	 * The feature id for the '<em><b>Transm Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__TRANSM_OVHD = TAM_PROTOCOL__TRANSM_OVHD;

	/**
	 * The feature id for the '<em><b>Msg Ctrl Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__MSG_CTRL_OVHD = TAM_PROTOCOL__MSG_CTRL_OVHD;

	/**
	 * The feature id for the '<em><b>Comm Tx T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__COMM_TX_T = TAM_PROTOCOL__COMM_TX_T;

	/**
	 * The feature id for the '<em><b>Comm Rcv T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE__COMM_RCV_T = TAM_PROTOCOL__COMM_RCV_T;

	/**
	 * The number of structural features of the '<em>Tam Com Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE_FEATURE_COUNT = TAM_PROTOCOL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam Com Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_SERVICE_OPERATION_COUNT = TAM_PROTOCOL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl <em>Tam Com Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl
	 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamComConnection()
	 * @generated
	 */
	int TAM_COM_CONNECTION = 3;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__RES_MULT = GQAMPackage.GA_COMM_HOST__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__IS_PROTECTED = GQAMPackage.GA_COMM_HOST__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__IS_ACTIVE = GQAMPackage.GA_COMM_HOST__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__BASE_PROPERTY = GQAMPackage.GA_COMM_HOST__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__BASE_INSTANCE_SPECIFICATION = GQAMPackage.GA_COMM_HOST__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__BASE_CLASSIFIER = GQAMPackage.GA_COMM_HOST__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__BASE_LIFELINE = GQAMPackage.GA_COMM_HOST__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__BASE_CONNECTABLE_ELEMENT = GQAMPackage.GA_COMM_HOST__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__SPEED_FACTOR = GQAMPackage.GA_COMM_HOST__SPEED_FACTOR;

	/**
	 * The feature id for the '<em><b>Main Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__MAIN_SCHEDULER = GQAMPackage.GA_COMM_HOST__MAIN_SCHEDULER;

	/**
	 * The feature id for the '<em><b>Element Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__ELEMENT_SIZE = GQAMPackage.GA_COMM_HOST__ELEMENT_SIZE;

	/**
	 * The feature id for the '<em><b>Base Connector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__BASE_CONNECTOR = GQAMPackage.GA_COMM_HOST__BASE_CONNECTOR;

	/**
	 * The feature id for the '<em><b>Transm Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__TRANSM_MODE = GQAMPackage.GA_COMM_HOST__TRANSM_MODE;

	/**
	 * The feature id for the '<em><b>Block T</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__BLOCK_T = GQAMPackage.GA_COMM_HOST__BLOCK_T;

	/**
	 * The feature id for the '<em><b>Packet T</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__PACKET_T = GQAMPackage.GA_COMM_HOST__PACKET_T;

	/**
	 * The feature id for the '<em><b>Capacity</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__CAPACITY = GQAMPackage.GA_COMM_HOST__CAPACITY;

	/**
	 * The feature id for the '<em><b>Is Preemptible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__IS_PREEMPTIBLE = GQAMPackage.GA_COMM_HOST__IS_PREEMPTIBLE;

	/**
	 * The feature id for the '<em><b>Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__SCHED_POLICY = GQAMPackage.GA_COMM_HOST__SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Other Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__OTHER_SCHED_POLICY = GQAMPackage.GA_COMM_HOST__OTHER_SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__SCHEDULE = GQAMPackage.GA_COMM_HOST__SCHEDULE;

	/**
	 * The feature id for the '<em><b>Processing Units</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__PROCESSING_UNITS = GQAMPackage.GA_COMM_HOST__PROCESSING_UNITS;

	/**
	 * The feature id for the '<em><b>Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__HOST = GQAMPackage.GA_COMM_HOST__HOST;

	/**
	 * The feature id for the '<em><b>Protected Shared Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__PROTECTED_SHARED_RESOURCES = GQAMPackage.GA_COMM_HOST__PROTECTED_SHARED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Schedulable Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__SCHEDULABLE_RESOURCES = GQAMPackage.GA_COMM_HOST__SCHEDULABLE_RESOURCES;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__THROUGHPUT = GQAMPackage.GA_COMM_HOST__THROUGHPUT;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__UTILIZATION = GQAMPackage.GA_COMM_HOST__UTILIZATION;

	/**
	 * The feature id for the '<em><b>Network Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__NETWORK_OVHD = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Network Block T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__NETWORK_BLOCK_T = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__INTERFACES = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Used Protocol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__USED_PROTOCOL = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Used Com Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__USED_COM_SERVICES = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Interface1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__INTERFACE1 = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Interface2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION__INTERFACE2 = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Tam Com Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION_FEATURE_COUNT = GQAMPackage.GA_COMM_HOST_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Tam Com Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_COM_CONNECTION_OPERATION_COUNT = GQAMPackage.GA_COMM_HOST_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_Communication.TamComInterface <em>Tam Com Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Com Interface</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComInterface
	 * @generated
	 */
	EClass getTamComInterface();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_Communication.TamComInterface#getArbitrationT <em>Arbitration T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arbitration T</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComInterface#getArbitrationT()
	 * @see #getTamComInterface()
	 * @generated
	 */
	EAttribute getTamComInterface_ArbitrationT();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_Communication.TamComInterface#getConnection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Connection</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComInterface#getConnection()
	 * @see #getTamComInterface()
	 * @generated
	 */
	EReference getTamComInterface_Connection();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol <em>Tam Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Protocol</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamProtocol
	 * @generated
	 */
	EClass getTamProtocol();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getTransmOvhd <em>Transm Ovhd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transm Ovhd</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamProtocol#getTransmOvhd()
	 * @see #getTamProtocol()
	 * @generated
	 */
	EAttribute getTamProtocol_TransmOvhd();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getMsgCtrlOvhd <em>Msg Ctrl Ovhd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Msg Ctrl Ovhd</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamProtocol#getMsgCtrlOvhd()
	 * @see #getTamProtocol()
	 * @generated
	 */
	EAttribute getTamProtocol_MsgCtrlOvhd();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommTxT <em>Comm Tx T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comm Tx T</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommTxT()
	 * @see #getTamProtocol()
	 * @generated
	 */
	EAttribute getTamProtocol_CommTxT();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommRcvT <em>Comm Rcv T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comm Rcv T</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommRcvT()
	 * @see #getTamProtocol()
	 * @generated
	 */
	EAttribute getTamProtocol_CommRcvT();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_Communication.TamComService <em>Tam Com Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Com Service</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComService
	 * @generated
	 */
	EClass getTamComService();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection <em>Tam Com Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Com Connection</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection
	 * @generated
	 */
	EClass getTamComConnection();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkOvhd <em>Network Ovhd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Network Ovhd</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkOvhd()
	 * @see #getTamComConnection()
	 * @generated
	 */
	EAttribute getTamComConnection_NetworkOvhd();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkBlockT <em>Network Block T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Network Block T</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkBlockT()
	 * @see #getTamComConnection()
	 * @generated
	 */
	EAttribute getTamComConnection_NetworkBlockT();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interfaces</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterfaces()
	 * @see #getTamComConnection()
	 * @generated
	 */
	EReference getTamComConnection_Interfaces();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getUsedProtocol <em>Used Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Used Protocol</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection#getUsedProtocol()
	 * @see #getTamComConnection()
	 * @generated
	 */
	EReference getTamComConnection_UsedProtocol();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getUsedComServices <em>Used Com Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Used Com Services</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection#getUsedComServices()
	 * @see #getTamComConnection()
	 * @generated
	 */
	EReference getTamComConnection_UsedComServices();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface1 <em>Interface1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interface1</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface1()
	 * @see #getTamComConnection()
	 * @generated
	 */
	EReference getTamComConnection_Interface1();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface2 <em>Interface2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interface2</em>'.
	 * @see TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface2()
	 * @see #getTamComConnection()
	 * @generated
	 */
	EReference getTamComConnection_Interface2();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TAM_CommunicationFactory getTAM_CommunicationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl <em>Tam Com Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamComInterface()
		 * @generated
		 */
		EClass TAM_COM_INTERFACE = eINSTANCE.getTamComInterface();

		/**
		 * The meta object literal for the '<em><b>Arbitration T</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_COM_INTERFACE__ARBITRATION_T = eINSTANCE.getTamComInterface_ArbitrationT();

		/**
		 * The meta object literal for the '<em><b>Connection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_COM_INTERFACE__CONNECTION = eINSTANCE.getTamComInterface_Connection();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl <em>Tam Protocol</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamProtocol()
		 * @generated
		 */
		EClass TAM_PROTOCOL = eINSTANCE.getTamProtocol();

		/**
		 * The meta object literal for the '<em><b>Transm Ovhd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_PROTOCOL__TRANSM_OVHD = eINSTANCE.getTamProtocol_TransmOvhd();

		/**
		 * The meta object literal for the '<em><b>Msg Ctrl Ovhd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_PROTOCOL__MSG_CTRL_OVHD = eINSTANCE.getTamProtocol_MsgCtrlOvhd();

		/**
		 * The meta object literal for the '<em><b>Comm Tx T</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_PROTOCOL__COMM_TX_T = eINSTANCE.getTamProtocol_CommTxT();

		/**
		 * The meta object literal for the '<em><b>Comm Rcv T</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_PROTOCOL__COMM_RCV_T = eINSTANCE.getTamProtocol_CommRcvT();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamComServiceImpl <em>Tam Com Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TamComServiceImpl
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamComService()
		 * @generated
		 */
		EClass TAM_COM_SERVICE = eINSTANCE.getTamComService();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl <em>Tam Com Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl
		 * @see TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl#getTamComConnection()
		 * @generated
		 */
		EClass TAM_COM_CONNECTION = eINSTANCE.getTamComConnection();

		/**
		 * The meta object literal for the '<em><b>Network Ovhd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_COM_CONNECTION__NETWORK_OVHD = eINSTANCE.getTamComConnection_NetworkOvhd();

		/**
		 * The meta object literal for the '<em><b>Network Block T</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_COM_CONNECTION__NETWORK_BLOCK_T = eINSTANCE.getTamComConnection_NetworkBlockT();

		/**
		 * The meta object literal for the '<em><b>Interfaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_COM_CONNECTION__INTERFACES = eINSTANCE.getTamComConnection_Interfaces();

		/**
		 * The meta object literal for the '<em><b>Used Protocol</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_COM_CONNECTION__USED_PROTOCOL = eINSTANCE.getTamComConnection_UsedProtocol();

		/**
		 * The meta object literal for the '<em><b>Used Com Services</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_COM_CONNECTION__USED_COM_SERVICES = eINSTANCE.getTamComConnection_UsedComServices();

		/**
		 * The meta object literal for the '<em><b>Interface1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_COM_CONNECTION__INTERFACE1 = eINSTANCE.getTamComConnection_Interface1();

		/**
		 * The meta object literal for the '<em><b>Interface2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_COM_CONNECTION__INTERFACE2 = eINSTANCE.getTamComConnection_Interface2();

	}

} //TAM_CommunicationPackage
