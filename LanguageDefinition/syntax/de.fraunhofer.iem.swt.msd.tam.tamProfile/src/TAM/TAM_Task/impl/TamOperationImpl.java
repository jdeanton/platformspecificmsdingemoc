/**
 */
package TAM.TAM_Task.impl;

import TAM.TAM_Task.TAM_TaskPackage;
import TAM.TAM_Task.TamMemReq;
import TAM.TAM_Task.TamOperation;
import TAM.TAM_Task.TamResourceAccess;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.ResourceUsageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.impl.TamOperationImpl#getMemReq <em>Mem Req</em>}</li>
 *   <li>{@link TAM.TAM_Task.impl.TamOperationImpl#getResAccess <em>Res Access</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamOperationImpl extends ResourceUsageImpl implements TamOperation {
	/**
	 * The cached value of the '{@link #getMemReq() <em>Mem Req</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemReq()
	 * @generated
	 * @ordered
	 */
	protected EList<TamMemReq> memReq;

	/**
	 * The cached value of the '{@link #getResAccess() <em>Res Access</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResAccess()
	 * @generated
	 * @ordered
	 */
	protected EList<TamResourceAccess> resAccess;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_TaskPackage.Literals.TAM_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamMemReq> getMemReq() {
		if (memReq == null) {
			memReq = new EObjectResolvingEList<TamMemReq>(TamMemReq.class, this, TAM_TaskPackage.TAM_OPERATION__MEM_REQ);
		}
		return memReq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamResourceAccess> getResAccess() {
		if (resAccess == null) {
			resAccess = new EObjectResolvingEList<TamResourceAccess>(TamResourceAccess.class, this, TAM_TaskPackage.TAM_OPERATION__RES_ACCESS);
		}
		return resAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_OPERATION__MEM_REQ:
				return getMemReq();
			case TAM_TaskPackage.TAM_OPERATION__RES_ACCESS:
				return getResAccess();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_OPERATION__MEM_REQ:
				getMemReq().clear();
				getMemReq().addAll((Collection<? extends TamMemReq>)newValue);
				return;
			case TAM_TaskPackage.TAM_OPERATION__RES_ACCESS:
				getResAccess().clear();
				getResAccess().addAll((Collection<? extends TamResourceAccess>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_OPERATION__MEM_REQ:
				getMemReq().clear();
				return;
			case TAM_TaskPackage.TAM_OPERATION__RES_ACCESS:
				getResAccess().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_OPERATION__MEM_REQ:
				return memReq != null && !memReq.isEmpty();
			case TAM_TaskPackage.TAM_OPERATION__RES_ACCESS:
				return resAccess != null && !resAccess.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TamOperationImpl
