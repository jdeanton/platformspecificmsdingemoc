/**
 */
package TAM.TAM_Platform.TAM_ControlUnit.impl;

import TAM.TAM_Platform.TAM_ControlUnit.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_ControlUnitFactoryImpl extends EFactoryImpl implements TAM_ControlUnitFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TAM_ControlUnitFactory init() {
		try {
			TAM_ControlUnitFactory theTAM_ControlUnitFactory = (TAM_ControlUnitFactory)EPackage.Registry.INSTANCE.getEFactory(TAM_ControlUnitPackage.eNS_URI);
			if (theTAM_ControlUnitFactory != null) {
				return theTAM_ControlUnitFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TAM_ControlUnitFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_ControlUnitFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TAM_ControlUnitPackage.TAM_MEMORY_UNIT: return createTamMemoryUnit();
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT: return createTamProcessingUnit();
			case TAM_ControlUnitPackage.TAM_SENSOR: return createTamSensor();
			case TAM_ControlUnitPackage.TAM_IO: return createTamIO();
			case TAM_ControlUnitPackage.TAM_ACTUATOR: return createTamActuator();
			case TAM_ControlUnitPackage.TAM_ECU: return createTamECU();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamMemoryUnit createTamMemoryUnit() {
		TamMemoryUnitImpl tamMemoryUnit = new TamMemoryUnitImpl();
		return tamMemoryUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamProcessingUnit createTamProcessingUnit() {
		TamProcessingUnitImpl tamProcessingUnit = new TamProcessingUnitImpl();
		return tamProcessingUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamSensor createTamSensor() {
		TamSensorImpl tamSensor = new TamSensorImpl();
		return tamSensor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamIO createTamIO() {
		TamIOImpl tamIO = new TamIOImpl();
		return tamIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamActuator createTamActuator() {
		TamActuatorImpl tamActuator = new TamActuatorImpl();
		return tamActuator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamECU createTamECU() {
		TamECUImpl tamECU = new TamECUImpl();
		return tamECU;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_ControlUnitPackage getTAM_ControlUnitPackage() {
		return (TAM_ControlUnitPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TAM_ControlUnitPackage getPackage() {
		return TAM_ControlUnitPackage.eINSTANCE;
	}

} //TAM_ControlUnitFactoryImpl
