/**
 */
package TAM.TAM_Platform.TAM_Communication.impl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;
import TAM.TAM_Platform.TAM_Communication.TamComConnection;
import TAM.TAM_Platform.TAM_Communication.TamComInterface;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaExecHostImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.TimerResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.TimingResource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Com Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl#isIsPeriodic <em>Is Periodic</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl#getArbitrationT <em>Arbitration T</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamComInterfaceImpl#getConnection <em>Connection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamComInterfaceImpl extends GaExecHostImpl implements TamComInterface {
	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final String DURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected String duration = DURATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsPeriodic() <em>Is Periodic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsPeriodic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_PERIODIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsPeriodic() <em>Is Periodic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsPeriodic()
	 * @generated
	 * @ordered
	 */
	protected boolean isPeriodic = IS_PERIODIC_EDEFAULT;

	/**
	 * The default value of the '{@link #getArbitrationT() <em>Arbitration T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArbitrationT()
	 * @generated
	 * @ordered
	 */
	protected static final String ARBITRATION_T_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArbitrationT() <em>Arbitration T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArbitrationT()
	 * @generated
	 * @ordered
	 */
	protected String arbitrationT = ARBITRATION_T_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConnection() <em>Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnection()
	 * @generated
	 * @ordered
	 */
	protected TamComConnection connection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamComInterfaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_CommunicationPackage.Literals.TAM_COM_INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(String newDuration) {
		String oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_INTERFACE__DURATION, oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsPeriodic() {
		return isPeriodic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPeriodic(boolean newIsPeriodic) {
		boolean oldIsPeriodic = isPeriodic;
		isPeriodic = newIsPeriodic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_INTERFACE__IS_PERIODIC, oldIsPeriodic, isPeriodic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getArbitrationT() {
		return arbitrationT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArbitrationT(String newArbitrationT) {
		String oldArbitrationT = arbitrationT;
		arbitrationT = newArbitrationT;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_INTERFACE__ARBITRATION_T, oldArbitrationT, arbitrationT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComConnection getConnection() {
		if (connection != null && connection.eIsProxy()) {
			InternalEObject oldConnection = (InternalEObject)connection;
			connection = (TamComConnection)eResolveProxy(oldConnection);
			if (connection != oldConnection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_CommunicationPackage.TAM_COM_INTERFACE__CONNECTION, oldConnection, connection));
			}
		}
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComConnection basicGetConnection() {
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnection(TamComConnection newConnection) {
		TamComConnection oldConnection = connection;
		connection = newConnection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_COM_INTERFACE__CONNECTION, oldConnection, connection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__DURATION:
				return getDuration();
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__IS_PERIODIC:
				return isIsPeriodic();
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__ARBITRATION_T:
				return getArbitrationT();
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__CONNECTION:
				if (resolve) return getConnection();
				return basicGetConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__DURATION:
				setDuration((String)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__IS_PERIODIC:
				setIsPeriodic((Boolean)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__ARBITRATION_T:
				setArbitrationT((String)newValue);
				return;
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__CONNECTION:
				setConnection((TamComConnection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__DURATION:
				setDuration(DURATION_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__IS_PERIODIC:
				setIsPeriodic(IS_PERIODIC_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__ARBITRATION_T:
				setArbitrationT(ARBITRATION_T_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__CONNECTION:
				setConnection((TamComConnection)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__DURATION:
				return DURATION_EDEFAULT == null ? duration != null : !DURATION_EDEFAULT.equals(duration);
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__IS_PERIODIC:
				return isPeriodic != IS_PERIODIC_EDEFAULT;
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__ARBITRATION_T:
				return ARBITRATION_T_EDEFAULT == null ? arbitrationT != null : !ARBITRATION_T_EDEFAULT.equals(arbitrationT);
			case TAM_CommunicationPackage.TAM_COM_INTERFACE__CONNECTION:
				return connection != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == TimingResource.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == TimerResource.class) {
			switch (derivedFeatureID) {
				case TAM_CommunicationPackage.TAM_COM_INTERFACE__DURATION: return GRMPackage.TIMER_RESOURCE__DURATION;
				case TAM_CommunicationPackage.TAM_COM_INTERFACE__IS_PERIODIC: return GRMPackage.TIMER_RESOURCE__IS_PERIODIC;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == TimingResource.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == TimerResource.class) {
			switch (baseFeatureID) {
				case GRMPackage.TIMER_RESOURCE__DURATION: return TAM_CommunicationPackage.TAM_COM_INTERFACE__DURATION;
				case GRMPackage.TIMER_RESOURCE__IS_PERIODIC: return TAM_CommunicationPackage.TAM_COM_INTERFACE__IS_PERIODIC;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(", isPeriodic: ");
		result.append(isPeriodic);
		result.append(", arbitrationT: ");
		result.append(arbitrationT);
		result.append(')');
		return result.toString();
	}

} //TamComInterfaceImpl
