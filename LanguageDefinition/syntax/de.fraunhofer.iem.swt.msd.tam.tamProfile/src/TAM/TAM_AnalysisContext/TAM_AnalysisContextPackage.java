/**
 */
package TAM.TAM_AnalysisContext;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextFactory
 * @model kind="package"
 * @generated
 */
public interface TAM_AnalysisContextPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TAM_AnalysisContext";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///TAM/TAM_AnalysisContext.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "TAM.TAM_AnalysisContext";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_AnalysisContextPackage eINSTANCE = TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl.init();

	/**
	 * The meta object id for the '{@link TAM.TAM_AnalysisContext.impl.TamWorkloadBehaviorImpl <em>Tam Workload Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_AnalysisContext.impl.TamWorkloadBehaviorImpl
	 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamWorkloadBehavior()
	 * @generated
	 */
	int TAM_WORKLOAD_BEHAVIOR = 0;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_WORKLOAD_BEHAVIOR__BEHAVIOR = GQAMPackage.GA_WORKLOAD_BEHAVIOR__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Demand</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_WORKLOAD_BEHAVIOR__DEMAND = GQAMPackage.GA_WORKLOAD_BEHAVIOR__DEMAND;

	/**
	 * The feature id for the '<em><b>Base Named Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_WORKLOAD_BEHAVIOR__BASE_NAMED_ELEMENT = GQAMPackage.GA_WORKLOAD_BEHAVIOR__BASE_NAMED_ELEMENT;

	/**
	 * The number of structural features of the '<em>Tam Workload Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_WORKLOAD_BEHAVIOR_FEATURE_COUNT = GQAMPackage.GA_WORKLOAD_BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Constraint1</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_WORKLOAD_BEHAVIOR___CONSTRAINT1__DIAGNOSTICCHAIN_MAP = GQAMPackage.GA_WORKLOAD_BEHAVIOR_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam Workload Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_WORKLOAD_BEHAVIOR_OPERATION_COUNT = GQAMPackage.GA_WORKLOAD_BEHAVIOR_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link TAM.TAM_AnalysisContext.impl.TamAnalysisContextImpl <em>Tam Analysis Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_AnalysisContext.impl.TamAnalysisContextImpl
	 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamAnalysisContext()
	 * @generated
	 */
	int TAM_ANALYSIS_CONTEXT = 1;

	/**
	 * The feature id for the '<em><b>Base Structured Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__BASE_STRUCTURED_CLASSIFIER = GQAMPackage.GA_ANALYSIS_CONTEXT__BASE_STRUCTURED_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__BASE_PACKAGE = GQAMPackage.GA_ANALYSIS_CONTEXT__BASE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__MODE = GQAMPackage.GA_ANALYSIS_CONTEXT__MODE;

	/**
	 * The feature id for the '<em><b>Base Named Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__BASE_NAMED_ELEMENT = GQAMPackage.GA_ANALYSIS_CONTEXT__BASE_NAMED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Context</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__CONTEXT = GQAMPackage.GA_ANALYSIS_CONTEXT__CONTEXT;

	/**
	 * The feature id for the '<em><b>Workload</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__WORKLOAD = GQAMPackage.GA_ANALYSIS_CONTEXT__WORKLOAD;

	/**
	 * The feature id for the '<em><b>Platform</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__PLATFORM = GQAMPackage.GA_ANALYSIS_CONTEXT__PLATFORM;

	/**
	 * The feature id for the '<em><b>Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__EXECUTION_TIME = GQAMPackage.GA_ANALYSIS_CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraints Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED = GQAMPackage.GA_ANALYSIS_CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam Analysis Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT_FEATURE_COUNT = GQAMPackage.GA_ANALYSIS_CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Constraint1</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT___CONSTRAINT1__DIAGNOSTICCHAIN_MAP = GQAMPackage.GA_ANALYSIS_CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam Analysis Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ANALYSIS_CONTEXT_OPERATION_COUNT = GQAMPackage.GA_ANALYSIS_CONTEXT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link TAM.TAM_AnalysisContext.impl.TamResourcePlatformImpl <em>Tam Resource Platform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_AnalysisContext.impl.TamResourcePlatformImpl
	 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamResourcePlatform()
	 * @generated
	 */
	int TAM_RESOURCE_PLATFORM = 2;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_PLATFORM__RESOURCES = GQAMPackage.GA_RESOURCES_PLATFORM__RESOURCES;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_PLATFORM__BASE_CLASSIFIER = GQAMPackage.GA_RESOURCES_PLATFORM__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Connections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_PLATFORM__CONNECTIONS = GQAMPackage.GA_RESOURCES_PLATFORM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EC Us</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_PLATFORM__EC_US = GQAMPackage.GA_RESOURCES_PLATFORM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam Resource Platform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_PLATFORM_FEATURE_COUNT = GQAMPackage.GA_RESOURCES_PLATFORM_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tam Resource Platform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_PLATFORM_OPERATION_COUNT = GQAMPackage.GA_RESOURCES_PLATFORM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_AnalysisContext.impl.TamAssumptionMSDImpl <em>Tam Assumption MSD</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_AnalysisContext.impl.TamAssumptionMSDImpl
	 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamAssumptionMSD()
	 * @generated
	 */
	int TAM_ASSUMPTION_MSD = 3;

	/**
	 * The feature id for the '<em><b>Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD__PATTERN = GQAMPackage.GA_WORKLOAD_EVENT__PATTERN;

	/**
	 * The feature id for the '<em><b>Generator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD__GENERATOR = GQAMPackage.GA_WORKLOAD_EVENT__GENERATOR;

	/**
	 * The feature id for the '<em><b>Trace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD__TRACE = GQAMPackage.GA_WORKLOAD_EVENT__TRACE;

	/**
	 * The feature id for the '<em><b>Effect</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD__EFFECT = GQAMPackage.GA_WORKLOAD_EVENT__EFFECT;

	/**
	 * The feature id for the '<em><b>Timed Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD__TIMED_EVENT = GQAMPackage.GA_WORKLOAD_EVENT__TIMED_EVENT;

	/**
	 * The feature id for the '<em><b>Base Named Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD__BASE_NAMED_ELEMENT = GQAMPackage.GA_WORKLOAD_EVENT__BASE_NAMED_ELEMENT;

	/**
	 * The number of structural features of the '<em>Tam Assumption MSD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD_FEATURE_COUNT = GQAMPackage.GA_WORKLOAD_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam Assumption MSD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ASSUMPTION_MSD_OPERATION_COUNT = GQAMPackage.GA_WORKLOAD_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_AnalysisContext.impl.TamRequirementMSDImpl <em>Tam Requirement MSD</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_AnalysisContext.impl.TamRequirementMSDImpl
	 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamRequirementMSD()
	 * @generated
	 */
	int TAM_REQUIREMENT_MSD = 4;

	/**
	 * The feature id for the '<em><b>Exec Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__EXEC_TIME = GQAMPackage.GA_SCENARIO__EXEC_TIME;

	/**
	 * The feature id for the '<em><b>Allocated Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__ALLOCATED_MEMORY = GQAMPackage.GA_SCENARIO__ALLOCATED_MEMORY;

	/**
	 * The feature id for the '<em><b>Used Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__USED_MEMORY = GQAMPackage.GA_SCENARIO__USED_MEMORY;

	/**
	 * The feature id for the '<em><b>Power Peak</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__POWER_PEAK = GQAMPackage.GA_SCENARIO__POWER_PEAK;

	/**
	 * The feature id for the '<em><b>Energy</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__ENERGY = GQAMPackage.GA_SCENARIO__ENERGY;

	/**
	 * The feature id for the '<em><b>Base Named Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__BASE_NAMED_ELEMENT = GQAMPackage.GA_SCENARIO__BASE_NAMED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Sub Usage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__SUB_USAGE = GQAMPackage.GA_SCENARIO__SUB_USAGE;

	/**
	 * The feature id for the '<em><b>Used Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__USED_RESOURCES = GQAMPackage.GA_SCENARIO__USED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Msg Size</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__MSG_SIZE = GQAMPackage.GA_SCENARIO__MSG_SIZE;

	/**
	 * The feature id for the '<em><b>On</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__ON = GQAMPackage.GA_SCENARIO__ON;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__BASE_ACTION = GQAMPackage.GA_SCENARIO__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Base Behavior</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__BASE_BEHAVIOR = GQAMPackage.GA_SCENARIO__BASE_BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Base Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__BASE_MESSAGE = GQAMPackage.GA_SCENARIO__BASE_MESSAGE;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__DURATION = GQAMPackage.GA_SCENARIO__DURATION;

	/**
	 * The feature id for the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__START = GQAMPackage.GA_SCENARIO__START;

	/**
	 * The feature id for the '<em><b>Finish</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__FINISH = GQAMPackage.GA_SCENARIO__FINISH;

	/**
	 * The feature id for the '<em><b>Cause</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__CAUSE = GQAMPackage.GA_SCENARIO__CAUSE;

	/**
	 * The feature id for the '<em><b>Host Demand</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__HOST_DEMAND = GQAMPackage.GA_SCENARIO__HOST_DEMAND;

	/**
	 * The feature id for the '<em><b>Host Demand Ops</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__HOST_DEMAND_OPS = GQAMPackage.GA_SCENARIO__HOST_DEMAND_OPS;

	/**
	 * The feature id for the '<em><b>Inter Occ T</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__INTER_OCC_T = GQAMPackage.GA_SCENARIO__INTER_OCC_T;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__THROUGHPUT = GQAMPackage.GA_SCENARIO__THROUGHPUT;

	/**
	 * The feature id for the '<em><b>Resp T</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__RESP_T = GQAMPackage.GA_SCENARIO__RESP_T;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__UTILIZATION = GQAMPackage.GA_SCENARIO__UTILIZATION;

	/**
	 * The feature id for the '<em><b>Utilization On Host</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__UTILIZATION_ON_HOST = GQAMPackage.GA_SCENARIO__UTILIZATION_ON_HOST;

	/**
	 * The feature id for the '<em><b>Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__ROOT = GQAMPackage.GA_SCENARIO__ROOT;

	/**
	 * The feature id for the '<em><b>Steps</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__STEPS = GQAMPackage.GA_SCENARIO__STEPS;

	/**
	 * The feature id for the '<em><b>Parent Step</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__PARENT_STEP = GQAMPackage.GA_SCENARIO__PARENT_STEP;

	/**
	 * The feature id for the '<em><b>Timing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__TIMING = GQAMPackage.GA_SCENARIO__TIMING;

	/**
	 * The feature id for the '<em><b>Activation Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__ACTIVATION_COUNT = GQAMPackage.GA_SCENARIO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraints Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED = GQAMPackage.GA_SCENARIO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam Requirement MSD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD_FEATURE_COUNT = GQAMPackage.GA_SCENARIO_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tam Requirement MSD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_REQUIREMENT_MSD_OPERATION_COUNT = GQAMPackage.GA_SCENARIO_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link TAM.TAM_AnalysisContext.TamWorkloadBehavior <em>Tam Workload Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Workload Behavior</em>'.
	 * @see TAM.TAM_AnalysisContext.TamWorkloadBehavior
	 * @generated
	 */
	EClass getTamWorkloadBehavior();

	/**
	 * Returns the meta object for the '{@link TAM.TAM_AnalysisContext.TamWorkloadBehavior#Constraint1(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Constraint1</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Constraint1</em>' operation.
	 * @see TAM.TAM_AnalysisContext.TamWorkloadBehavior#Constraint1(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTamWorkloadBehavior__Constraint1__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_AnalysisContext.TamAnalysisContext <em>Tam Analysis Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Analysis Context</em>'.
	 * @see TAM.TAM_AnalysisContext.TamAnalysisContext
	 * @generated
	 */
	EClass getTamAnalysisContext();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_AnalysisContext.TamAnalysisContext#getExecutionTime <em>Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Time</em>'.
	 * @see TAM.TAM_AnalysisContext.TamAnalysisContext#getExecutionTime()
	 * @see #getTamAnalysisContext()
	 * @generated
	 */
	EAttribute getTamAnalysisContext_ExecutionTime();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_AnalysisContext.TamAnalysisContext#getConstraintsSatisfied <em>Constraints Satisfied</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constraints Satisfied</em>'.
	 * @see TAM.TAM_AnalysisContext.TamAnalysisContext#getConstraintsSatisfied()
	 * @see #getTamAnalysisContext()
	 * @generated
	 */
	EAttribute getTamAnalysisContext_ConstraintsSatisfied();

	/**
	 * Returns the meta object for the '{@link TAM.TAM_AnalysisContext.TamAnalysisContext#Constraint1(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Constraint1</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Constraint1</em>' operation.
	 * @see TAM.TAM_AnalysisContext.TamAnalysisContext#Constraint1(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTamAnalysisContext__Constraint1__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_AnalysisContext.TamResourcePlatform <em>Tam Resource Platform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Resource Platform</em>'.
	 * @see TAM.TAM_AnalysisContext.TamResourcePlatform
	 * @generated
	 */
	EClass getTamResourcePlatform();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_AnalysisContext.TamResourcePlatform#getConnections <em>Connections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connections</em>'.
	 * @see TAM.TAM_AnalysisContext.TamResourcePlatform#getConnections()
	 * @see #getTamResourcePlatform()
	 * @generated
	 */
	EReference getTamResourcePlatform_Connections();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_AnalysisContext.TamResourcePlatform#getECUs <em>EC Us</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>EC Us</em>'.
	 * @see TAM.TAM_AnalysisContext.TamResourcePlatform#getECUs()
	 * @see #getTamResourcePlatform()
	 * @generated
	 */
	EReference getTamResourcePlatform_ECUs();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_AnalysisContext.TamAssumptionMSD <em>Tam Assumption MSD</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Assumption MSD</em>'.
	 * @see TAM.TAM_AnalysisContext.TamAssumptionMSD
	 * @generated
	 */
	EClass getTamAssumptionMSD();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_AnalysisContext.TamRequirementMSD <em>Tam Requirement MSD</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Requirement MSD</em>'.
	 * @see TAM.TAM_AnalysisContext.TamRequirementMSD
	 * @generated
	 */
	EClass getTamRequirementMSD();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_AnalysisContext.TamRequirementMSD#getActivationCount <em>Activation Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activation Count</em>'.
	 * @see TAM.TAM_AnalysisContext.TamRequirementMSD#getActivationCount()
	 * @see #getTamRequirementMSD()
	 * @generated
	 */
	EAttribute getTamRequirementMSD_ActivationCount();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_AnalysisContext.TamRequirementMSD#getConstraintsSatisfied <em>Constraints Satisfied</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constraints Satisfied</em>'.
	 * @see TAM.TAM_AnalysisContext.TamRequirementMSD#getConstraintsSatisfied()
	 * @see #getTamRequirementMSD()
	 * @generated
	 */
	EAttribute getTamRequirementMSD_ConstraintsSatisfied();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TAM_AnalysisContextFactory getTAM_AnalysisContextFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link TAM.TAM_AnalysisContext.impl.TamWorkloadBehaviorImpl <em>Tam Workload Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_AnalysisContext.impl.TamWorkloadBehaviorImpl
		 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamWorkloadBehavior()
		 * @generated
		 */
		EClass TAM_WORKLOAD_BEHAVIOR = eINSTANCE.getTamWorkloadBehavior();

		/**
		 * The meta object literal for the '<em><b>Constraint1</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TAM_WORKLOAD_BEHAVIOR___CONSTRAINT1__DIAGNOSTICCHAIN_MAP = eINSTANCE.getTamWorkloadBehavior__Constraint1__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link TAM.TAM_AnalysisContext.impl.TamAnalysisContextImpl <em>Tam Analysis Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_AnalysisContext.impl.TamAnalysisContextImpl
		 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamAnalysisContext()
		 * @generated
		 */
		EClass TAM_ANALYSIS_CONTEXT = eINSTANCE.getTamAnalysisContext();

		/**
		 * The meta object literal for the '<em><b>Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_ANALYSIS_CONTEXT__EXECUTION_TIME = eINSTANCE.getTamAnalysisContext_ExecutionTime();

		/**
		 * The meta object literal for the '<em><b>Constraints Satisfied</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED = eINSTANCE.getTamAnalysisContext_ConstraintsSatisfied();

		/**
		 * The meta object literal for the '<em><b>Constraint1</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TAM_ANALYSIS_CONTEXT___CONSTRAINT1__DIAGNOSTICCHAIN_MAP = eINSTANCE.getTamAnalysisContext__Constraint1__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link TAM.TAM_AnalysisContext.impl.TamResourcePlatformImpl <em>Tam Resource Platform</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_AnalysisContext.impl.TamResourcePlatformImpl
		 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamResourcePlatform()
		 * @generated
		 */
		EClass TAM_RESOURCE_PLATFORM = eINSTANCE.getTamResourcePlatform();

		/**
		 * The meta object literal for the '<em><b>Connections</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RESOURCE_PLATFORM__CONNECTIONS = eINSTANCE.getTamResourcePlatform_Connections();

		/**
		 * The meta object literal for the '<em><b>EC Us</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RESOURCE_PLATFORM__EC_US = eINSTANCE.getTamResourcePlatform_ECUs();

		/**
		 * The meta object literal for the '{@link TAM.TAM_AnalysisContext.impl.TamAssumptionMSDImpl <em>Tam Assumption MSD</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_AnalysisContext.impl.TamAssumptionMSDImpl
		 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamAssumptionMSD()
		 * @generated
		 */
		EClass TAM_ASSUMPTION_MSD = eINSTANCE.getTamAssumptionMSD();

		/**
		 * The meta object literal for the '{@link TAM.TAM_AnalysisContext.impl.TamRequirementMSDImpl <em>Tam Requirement MSD</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_AnalysisContext.impl.TamRequirementMSDImpl
		 * @see TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl#getTamRequirementMSD()
		 * @generated
		 */
		EClass TAM_REQUIREMENT_MSD = eINSTANCE.getTamRequirementMSD();

		/**
		 * The meta object literal for the '<em><b>Activation Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_REQUIREMENT_MSD__ACTIVATION_COUNT = eINSTANCE.getTamRequirementMSD_ActivationCount();

		/**
		 * The meta object literal for the '<em><b>Constraints Satisfied</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED = eINSTANCE.getTamRequirementMSD_ConstraintsSatisfied();

	}

} //TAM_AnalysisContextPackage
