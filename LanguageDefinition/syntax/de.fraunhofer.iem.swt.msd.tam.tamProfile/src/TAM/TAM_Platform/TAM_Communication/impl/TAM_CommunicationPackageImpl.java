/**
 */
package TAM.TAM_Platform.TAM_Communication.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;

import TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationFactory;
import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;
import TAM.TAM_Platform.TAM_Communication.TamComConnection;
import TAM.TAM_Platform.TAM_Communication.TamComInterface;
import TAM.TAM_Platform.TAM_Communication.TamComService;
import TAM.TAM_Platform.TAM_Communication.TamProtocol;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;

import TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;

import TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl;

import TAM.TAM_Task.TAM_TaskPackage;

import TAM.TAM_Task.impl.TAM_TaskPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.uml2.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_CommunicationPackageImpl extends EPackageImpl implements TAM_CommunicationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamComInterfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamProtocolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamComServiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamComConnectionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TAM_CommunicationPackageImpl() {
		super(eNS_URI, TAM_CommunicationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TAM_CommunicationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TAM_CommunicationPackage init() {
		if (isInited) return (TAM_CommunicationPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI);

		// Obtain or create and register package
		TAM_CommunicationPackageImpl theTAM_CommunicationPackage = (TAM_CommunicationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TAM_CommunicationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TAM_CommunicationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MARTEPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TAM_AnalysisContextPackageImpl theTAM_AnalysisContextPackage = (TAM_AnalysisContextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) instanceof TAM_AnalysisContextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) : TAM_AnalysisContextPackage.eINSTANCE);
		TAM_ControlUnitPackageImpl theTAM_ControlUnitPackage = (TAM_ControlUnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) instanceof TAM_ControlUnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) : TAM_ControlUnitPackage.eINSTANCE);
		TAM_OperatingSystemPackageImpl theTAM_OperatingSystemPackage = (TAM_OperatingSystemPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) instanceof TAM_OperatingSystemPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) : TAM_OperatingSystemPackage.eINSTANCE);
		TAM_TaskPackageImpl theTAM_TaskPackage = (TAM_TaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) instanceof TAM_TaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) : TAM_TaskPackage.eINSTANCE);

		// Create package meta-data objects
		theTAM_CommunicationPackage.createPackageContents();
		theTAM_AnalysisContextPackage.createPackageContents();
		theTAM_ControlUnitPackage.createPackageContents();
		theTAM_OperatingSystemPackage.createPackageContents();
		theTAM_TaskPackage.createPackageContents();

		// Initialize created meta-data
		theTAM_CommunicationPackage.initializePackageContents();
		theTAM_AnalysisContextPackage.initializePackageContents();
		theTAM_ControlUnitPackage.initializePackageContents();
		theTAM_OperatingSystemPackage.initializePackageContents();
		theTAM_TaskPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTAM_CommunicationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TAM_CommunicationPackage.eNS_URI, theTAM_CommunicationPackage);
		return theTAM_CommunicationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamComInterface() {
		return tamComInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamComInterface_ArbitrationT() {
		return (EAttribute)tamComInterfaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamComInterface_Connection() {
		return (EReference)tamComInterfaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamProtocol() {
		return tamProtocolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamProtocol_TransmOvhd() {
		return (EAttribute)tamProtocolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamProtocol_MsgCtrlOvhd() {
		return (EAttribute)tamProtocolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamProtocol_CommTxT() {
		return (EAttribute)tamProtocolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamProtocol_CommRcvT() {
		return (EAttribute)tamProtocolEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamComService() {
		return tamComServiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamComConnection() {
		return tamComConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamComConnection_NetworkOvhd() {
		return (EAttribute)tamComConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamComConnection_NetworkBlockT() {
		return (EAttribute)tamComConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamComConnection_Interfaces() {
		return (EReference)tamComConnectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamComConnection_UsedProtocol() {
		return (EReference)tamComConnectionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamComConnection_UsedComServices() {
		return (EReference)tamComConnectionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamComConnection_Interface1() {
		return (EReference)tamComConnectionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamComConnection_Interface2() {
		return (EReference)tamComConnectionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_CommunicationFactory getTAM_CommunicationFactory() {
		return (TAM_CommunicationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tamComInterfaceEClass = createEClass(TAM_COM_INTERFACE);
		createEAttribute(tamComInterfaceEClass, TAM_COM_INTERFACE__ARBITRATION_T);
		createEReference(tamComInterfaceEClass, TAM_COM_INTERFACE__CONNECTION);

		tamProtocolEClass = createEClass(TAM_PROTOCOL);
		createEAttribute(tamProtocolEClass, TAM_PROTOCOL__TRANSM_OVHD);
		createEAttribute(tamProtocolEClass, TAM_PROTOCOL__MSG_CTRL_OVHD);
		createEAttribute(tamProtocolEClass, TAM_PROTOCOL__COMM_TX_T);
		createEAttribute(tamProtocolEClass, TAM_PROTOCOL__COMM_RCV_T);

		tamComServiceEClass = createEClass(TAM_COM_SERVICE);

		tamComConnectionEClass = createEClass(TAM_COM_CONNECTION);
		createEAttribute(tamComConnectionEClass, TAM_COM_CONNECTION__NETWORK_OVHD);
		createEAttribute(tamComConnectionEClass, TAM_COM_CONNECTION__NETWORK_BLOCK_T);
		createEReference(tamComConnectionEClass, TAM_COM_CONNECTION__INTERFACES);
		createEReference(tamComConnectionEClass, TAM_COM_CONNECTION__USED_PROTOCOL);
		createEReference(tamComConnectionEClass, TAM_COM_CONNECTION__USED_COM_SERVICES);
		createEReference(tamComConnectionEClass, TAM_COM_CONNECTION__INTERFACE1);
		createEReference(tamComConnectionEClass, TAM_COM_CONNECTION__INTERFACE2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GQAMPackage theGQAMPackage = (GQAMPackage)EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		GRMPackage theGRMPackage = (GRMPackage)EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tamComInterfaceEClass.getESuperTypes().add(theGQAMPackage.getGaExecHost());
		tamComInterfaceEClass.getESuperTypes().add(theGRMPackage.getTimerResource());
		tamProtocolEClass.getESuperTypes().add(theGQAMPackage.getGaCommChannel());
		tamComServiceEClass.getESuperTypes().add(this.getTamProtocol());
		tamComConnectionEClass.getESuperTypes().add(theGQAMPackage.getGaCommHost());

		// Initialize classes, features, and operations; add parameters
		initEClass(tamComInterfaceEClass, TamComInterface.class, "TamComInterface", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamComInterface_ArbitrationT(), theTypesPackage.getString(), "arbitrationT", null, 0, 1, TamComInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamComInterface_Connection(), this.getTamComConnection(), null, "connection", null, 0, 1, TamComInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamProtocolEClass, TamProtocol.class, "TamProtocol", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamProtocol_TransmOvhd(), theTypesPackage.getString(), "transmOvhd", null, 0, 1, TamProtocol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamProtocol_MsgCtrlOvhd(), theTypesPackage.getString(), "msgCtrlOvhd", null, 0, 1, TamProtocol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamProtocol_CommTxT(), theTypesPackage.getString(), "commTxT", null, 0, 1, TamProtocol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamProtocol_CommRcvT(), theTypesPackage.getString(), "commRcvT", null, 0, 1, TamProtocol.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamComServiceEClass, TamComService.class, "TamComService", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tamComConnectionEClass, TamComConnection.class, "TamComConnection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamComConnection_NetworkOvhd(), theTypesPackage.getString(), "networkOvhd", null, 0, 1, TamComConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamComConnection_NetworkBlockT(), theTypesPackage.getString(), "networkBlockT", null, 0, 1, TamComConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamComConnection_Interfaces(), this.getTamComInterface(), null, "interfaces", null, 0, -1, TamComConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamComConnection_UsedProtocol(), this.getTamProtocol(), null, "usedProtocol", null, 0, 1, TamComConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamComConnection_UsedComServices(), this.getTamComService(), null, "usedComServices", null, 0, -1, TamComConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTamComConnection_Interface1(), this.getTamComInterface(), null, "interface1", null, 1, 1, TamComConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamComConnection_Interface2(), this.getTamComInterface(), null, "interface2", null, 1, 1, TamComConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TAM_CommunicationPackageImpl
