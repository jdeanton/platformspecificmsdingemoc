/**
 */
package TAM.TAM_Platform.TAM_ControlUnit.impl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;
import TAM.TAM_Platform.TAM_ControlUnit.TamSensor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Sensor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TamSensorImpl extends TamPeripheryUnitImpl implements TamSensor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamSensorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_ControlUnitPackage.Literals.TAM_SENSOR;
	}

} //TamSensorImpl
