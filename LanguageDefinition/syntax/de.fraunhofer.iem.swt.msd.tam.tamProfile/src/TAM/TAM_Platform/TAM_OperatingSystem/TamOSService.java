/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam OS Service</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamOSService()
 * @model
 * @generated
 */
public interface TamOSService extends TamOSResource {
} // TamOSService
