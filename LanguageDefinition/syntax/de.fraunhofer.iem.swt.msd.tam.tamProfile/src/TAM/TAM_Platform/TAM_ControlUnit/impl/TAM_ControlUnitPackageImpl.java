/**
 */
package TAM.TAM_Platform.TAM_ControlUnit.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;

import TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;

import TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitFactory;
import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;
import TAM.TAM_Platform.TAM_ControlUnit.TamActuator;
import TAM.TAM_Platform.TAM_ControlUnit.TamECU;
import TAM.TAM_Platform.TAM_ControlUnit.TamIO;
import TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit;
import TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit;
import TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit;
import TAM.TAM_Platform.TAM_ControlUnit.TamSensor;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;

import TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl;

import TAM.TAM_Task.TAM_TaskPackage;

import TAM.TAM_Task.impl.TAM_TaskPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.uml2.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_ControlUnitPackageImpl extends EPackageImpl implements TAM_ControlUnitPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamMemoryUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamProcessingUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamPeripheryUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamSensorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamIOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamActuatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamECUEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TAM_ControlUnitPackageImpl() {
		super(eNS_URI, TAM_ControlUnitFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TAM_ControlUnitPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TAM_ControlUnitPackage init() {
		if (isInited) return (TAM_ControlUnitPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI);

		// Obtain or create and register package
		TAM_ControlUnitPackageImpl theTAM_ControlUnitPackage = (TAM_ControlUnitPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TAM_ControlUnitPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TAM_ControlUnitPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MARTEPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TAM_AnalysisContextPackageImpl theTAM_AnalysisContextPackage = (TAM_AnalysisContextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) instanceof TAM_AnalysisContextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) : TAM_AnalysisContextPackage.eINSTANCE);
		TAM_OperatingSystemPackageImpl theTAM_OperatingSystemPackage = (TAM_OperatingSystemPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) instanceof TAM_OperatingSystemPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) : TAM_OperatingSystemPackage.eINSTANCE);
		TAM_CommunicationPackageImpl theTAM_CommunicationPackage = (TAM_CommunicationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) instanceof TAM_CommunicationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) : TAM_CommunicationPackage.eINSTANCE);
		TAM_TaskPackageImpl theTAM_TaskPackage = (TAM_TaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) instanceof TAM_TaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) : TAM_TaskPackage.eINSTANCE);

		// Create package meta-data objects
		theTAM_ControlUnitPackage.createPackageContents();
		theTAM_AnalysisContextPackage.createPackageContents();
		theTAM_OperatingSystemPackage.createPackageContents();
		theTAM_CommunicationPackage.createPackageContents();
		theTAM_TaskPackage.createPackageContents();

		// Initialize created meta-data
		theTAM_ControlUnitPackage.initializePackageContents();
		theTAM_AnalysisContextPackage.initializePackageContents();
		theTAM_OperatingSystemPackage.initializePackageContents();
		theTAM_CommunicationPackage.initializePackageContents();
		theTAM_TaskPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTAM_ControlUnitPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TAM_ControlUnitPackage.eNS_URI, theTAM_ControlUnitPackage);
		return theTAM_ControlUnitPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamMemoryUnit() {
		return tamMemoryUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamMemoryUnit_MemorySize() {
		return (EAttribute)tamMemoryUnitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamMemoryUnit_Throughput() {
		return (EAttribute)tamMemoryUnitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamMemoryUnit_Type() {
		return (EReference)tamMemoryUnitEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamMemoryUnit_AccessDelay() {
		return (EAttribute)tamMemoryUnitEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamProcessingUnit() {
		return tamProcessingUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamProcessingUnit_NumCores() {
		return (EAttribute)tamProcessingUnitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamProcessingUnit_CoreSyncOvhd() {
		return (EAttribute)tamProcessingUnitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamPeripheryUnit() {
		return tamPeripheryUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamPeripheryUnit_Delay() {
		return (EAttribute)tamPeripheryUnitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamPeripheryUnit_Type() {
		return (EReference)tamPeripheryUnitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamSensor() {
		return tamSensorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamIO() {
		return tamIOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamActuator() {
		return tamActuatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamECU() {
		return tamECUEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamECU_ProcessingUnit() {
		return (EReference)tamECUEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamECU_Memory() {
		return (EReference)tamECUEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamECU_NetworkInterfaces() {
		return (EReference)tamECUEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamECU_Peripherals() {
		return (EReference)tamECUEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamECU_RTOS() {
		return (EReference)tamECUEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_ControlUnitFactory getTAM_ControlUnitFactory() {
		return (TAM_ControlUnitFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tamMemoryUnitEClass = createEClass(TAM_MEMORY_UNIT);
		createEAttribute(tamMemoryUnitEClass, TAM_MEMORY_UNIT__MEMORY_SIZE);
		createEAttribute(tamMemoryUnitEClass, TAM_MEMORY_UNIT__THROUGHPUT);
		createEReference(tamMemoryUnitEClass, TAM_MEMORY_UNIT__TYPE);
		createEAttribute(tamMemoryUnitEClass, TAM_MEMORY_UNIT__ACCESS_DELAY);

		tamProcessingUnitEClass = createEClass(TAM_PROCESSING_UNIT);
		createEAttribute(tamProcessingUnitEClass, TAM_PROCESSING_UNIT__NUM_CORES);
		createEAttribute(tamProcessingUnitEClass, TAM_PROCESSING_UNIT__CORE_SYNC_OVHD);

		tamPeripheryUnitEClass = createEClass(TAM_PERIPHERY_UNIT);
		createEAttribute(tamPeripheryUnitEClass, TAM_PERIPHERY_UNIT__DELAY);
		createEReference(tamPeripheryUnitEClass, TAM_PERIPHERY_UNIT__TYPE);

		tamSensorEClass = createEClass(TAM_SENSOR);

		tamIOEClass = createEClass(TAM_IO);

		tamActuatorEClass = createEClass(TAM_ACTUATOR);

		tamECUEClass = createEClass(TAM_ECU);
		createEReference(tamECUEClass, TAM_ECU__PROCESSING_UNIT);
		createEReference(tamECUEClass, TAM_ECU__MEMORY);
		createEReference(tamECUEClass, TAM_ECU__NETWORK_INTERFACES);
		createEReference(tamECUEClass, TAM_ECU__PERIPHERALS);
		createEReference(tamECUEClass, TAM_ECU__RTOS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GRMPackage theGRMPackage = (GRMPackage)EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		TAM_TaskPackage theTAM_TaskPackage = (TAM_TaskPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI);
		GQAMPackage theGQAMPackage = (GQAMPackage)EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		TAM_CommunicationPackage theTAM_CommunicationPackage = (TAM_CommunicationPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI);
		TAM_OperatingSystemPackage theTAM_OperatingSystemPackage = (TAM_OperatingSystemPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tamMemoryUnitEClass.getESuperTypes().add(theGRMPackage.getStorageResource());
		tamProcessingUnitEClass.getESuperTypes().add(theGQAMPackage.getGaExecHost());
		tamPeripheryUnitEClass.getESuperTypes().add(theGRMPackage.getResource());
		tamSensorEClass.getESuperTypes().add(this.getTamPeripheryUnit());
		tamIOEClass.getESuperTypes().add(this.getTamPeripheryUnit());
		tamActuatorEClass.getESuperTypes().add(this.getTamPeripheryUnit());
		tamECUEClass.getESuperTypes().add(theGRMPackage.getResource());

		// Initialize classes, features, and operations; add parameters
		initEClass(tamMemoryUnitEClass, TamMemoryUnit.class, "TamMemoryUnit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamMemoryUnit_MemorySize(), theTypesPackage.getString(), "memorySize", null, 0, 1, TamMemoryUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamMemoryUnit_Throughput(), theTypesPackage.getString(), "throughput", null, 0, 1, TamMemoryUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamMemoryUnit_Type(), theTAM_TaskPackage.getTamMemType(), null, "type", null, 0, 1, TamMemoryUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamMemoryUnit_AccessDelay(), theTypesPackage.getString(), "accessDelay", null, 0, 1, TamMemoryUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamProcessingUnitEClass, TamProcessingUnit.class, "TamProcessingUnit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamProcessingUnit_NumCores(), theTypesPackage.getInteger(), "numCores", null, 0, 1, TamProcessingUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamProcessingUnit_CoreSyncOvhd(), theTypesPackage.getString(), "coreSyncOvhd", null, 0, 1, TamProcessingUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamPeripheryUnitEClass, TamPeripheryUnit.class, "TamPeripheryUnit", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamPeripheryUnit_Delay(), theTypesPackage.getString(), "delay", null, 0, 1, TamPeripheryUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamPeripheryUnit_Type(), theTAM_TaskPackage.getTamPeripheryType(), null, "type", null, 1, 1, TamPeripheryUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamSensorEClass, TamSensor.class, "TamSensor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tamIOEClass, TamIO.class, "TamIO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tamActuatorEClass, TamActuator.class, "TamActuator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tamECUEClass, TamECU.class, "TamECU", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamECU_ProcessingUnit(), this.getTamProcessingUnit(), null, "processingUnit", null, 0, 1, TamECU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamECU_Memory(), this.getTamMemoryUnit(), null, "memory", null, 0, -1, TamECU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamECU_NetworkInterfaces(), theTAM_CommunicationPackage.getTamComInterface(), null, "networkInterfaces", null, 0, -1, TamECU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamECU_Peripherals(), this.getTamPeripheryUnit(), null, "peripherals", null, 0, -1, TamECU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamECU_RTOS(), theTAM_OperatingSystemPackage.getTamRTOS(), null, "RTOS", null, 0, 1, TamECU.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TAM_ControlUnitPackageImpl
