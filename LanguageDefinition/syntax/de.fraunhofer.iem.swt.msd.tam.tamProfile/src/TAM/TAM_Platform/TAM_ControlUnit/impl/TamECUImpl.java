/**
 */
package TAM.TAM_Platform.TAM_ControlUnit.impl;

import TAM.TAM_Platform.TAM_Communication.TamComInterface;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;
import TAM.TAM_Platform.TAM_ControlUnit.TamECU;
import TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit;
import TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit;
import TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit;

import TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.ResourceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam ECU</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl#getProcessingUnit <em>Processing Unit</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl#getMemory <em>Memory</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl#getNetworkInterfaces <em>Network Interfaces</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl#getPeripherals <em>Peripherals</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl#getRTOS <em>RTOS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamECUImpl extends ResourceImpl implements TamECU {
	/**
	 * The cached value of the '{@link #getProcessingUnit() <em>Processing Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessingUnit()
	 * @generated
	 * @ordered
	 */
	protected TamProcessingUnit processingUnit;

	/**
	 * The cached value of the '{@link #getMemory() <em>Memory</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemory()
	 * @generated
	 * @ordered
	 */
	protected EList<TamMemoryUnit> memory;

	/**
	 * The cached value of the '{@link #getNetworkInterfaces() <em>Network Interfaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<TamComInterface> networkInterfaces;

	/**
	 * The cached value of the '{@link #getPeripherals() <em>Peripherals</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeripherals()
	 * @generated
	 * @ordered
	 */
	protected EList<TamPeripheryUnit> peripherals;

	/**
	 * The cached value of the '{@link #getRTOS() <em>RTOS</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRTOS()
	 * @generated
	 * @ordered
	 */
	protected TamRTOS rtos;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamECUImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_ControlUnitPackage.Literals.TAM_ECU;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamProcessingUnit getProcessingUnit() {
		if (processingUnit != null && processingUnit.eIsProxy()) {
			InternalEObject oldProcessingUnit = (InternalEObject)processingUnit;
			processingUnit = (TamProcessingUnit)eResolveProxy(oldProcessingUnit);
			if (processingUnit != oldProcessingUnit) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_ControlUnitPackage.TAM_ECU__PROCESSING_UNIT, oldProcessingUnit, processingUnit));
			}
		}
		return processingUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamProcessingUnit basicGetProcessingUnit() {
		return processingUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessingUnit(TamProcessingUnit newProcessingUnit) {
		TamProcessingUnit oldProcessingUnit = processingUnit;
		processingUnit = newProcessingUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_ECU__PROCESSING_UNIT, oldProcessingUnit, processingUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamMemoryUnit> getMemory() {
		if (memory == null) {
			memory = new EObjectResolvingEList<TamMemoryUnit>(TamMemoryUnit.class, this, TAM_ControlUnitPackage.TAM_ECU__MEMORY);
		}
		return memory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamComInterface> getNetworkInterfaces() {
		if (networkInterfaces == null) {
			networkInterfaces = new EObjectResolvingEList<TamComInterface>(TamComInterface.class, this, TAM_ControlUnitPackage.TAM_ECU__NETWORK_INTERFACES);
		}
		return networkInterfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamPeripheryUnit> getPeripherals() {
		if (peripherals == null) {
			peripherals = new EObjectResolvingEList<TamPeripheryUnit>(TamPeripheryUnit.class, this, TAM_ControlUnitPackage.TAM_ECU__PERIPHERALS);
		}
		return peripherals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamRTOS getRTOS() {
		if (rtos != null && rtos.eIsProxy()) {
			InternalEObject oldRTOS = (InternalEObject)rtos;
			rtos = (TamRTOS)eResolveProxy(oldRTOS);
			if (rtos != oldRTOS) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_ControlUnitPackage.TAM_ECU__RTOS, oldRTOS, rtos));
			}
		}
		return rtos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamRTOS basicGetRTOS() {
		return rtos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRTOS(TamRTOS newRTOS) {
		TamRTOS oldRTOS = rtos;
		rtos = newRTOS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_ECU__RTOS, oldRTOS, rtos));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_ECU__PROCESSING_UNIT:
				if (resolve) return getProcessingUnit();
				return basicGetProcessingUnit();
			case TAM_ControlUnitPackage.TAM_ECU__MEMORY:
				return getMemory();
			case TAM_ControlUnitPackage.TAM_ECU__NETWORK_INTERFACES:
				return getNetworkInterfaces();
			case TAM_ControlUnitPackage.TAM_ECU__PERIPHERALS:
				return getPeripherals();
			case TAM_ControlUnitPackage.TAM_ECU__RTOS:
				if (resolve) return getRTOS();
				return basicGetRTOS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_ECU__PROCESSING_UNIT:
				setProcessingUnit((TamProcessingUnit)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_ECU__MEMORY:
				getMemory().clear();
				getMemory().addAll((Collection<? extends TamMemoryUnit>)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_ECU__NETWORK_INTERFACES:
				getNetworkInterfaces().clear();
				getNetworkInterfaces().addAll((Collection<? extends TamComInterface>)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_ECU__PERIPHERALS:
				getPeripherals().clear();
				getPeripherals().addAll((Collection<? extends TamPeripheryUnit>)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_ECU__RTOS:
				setRTOS((TamRTOS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_ECU__PROCESSING_UNIT:
				setProcessingUnit((TamProcessingUnit)null);
				return;
			case TAM_ControlUnitPackage.TAM_ECU__MEMORY:
				getMemory().clear();
				return;
			case TAM_ControlUnitPackage.TAM_ECU__NETWORK_INTERFACES:
				getNetworkInterfaces().clear();
				return;
			case TAM_ControlUnitPackage.TAM_ECU__PERIPHERALS:
				getPeripherals().clear();
				return;
			case TAM_ControlUnitPackage.TAM_ECU__RTOS:
				setRTOS((TamRTOS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_ECU__PROCESSING_UNIT:
				return processingUnit != null;
			case TAM_ControlUnitPackage.TAM_ECU__MEMORY:
				return memory != null && !memory.isEmpty();
			case TAM_ControlUnitPackage.TAM_ECU__NETWORK_INTERFACES:
				return networkInterfaces != null && !networkInterfaces.isEmpty();
			case TAM_ControlUnitPackage.TAM_ECU__PERIPHERALS:
				return peripherals != null && !peripherals.isEmpty();
			case TAM_ControlUnitPackage.TAM_ECU__RTOS:
				return rtos != null;
		}
		return super.eIsSet(featureID);
	}

} //TamECUImpl
