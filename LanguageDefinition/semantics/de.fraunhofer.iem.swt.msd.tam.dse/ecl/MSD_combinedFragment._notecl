import _'platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore'

ECLimport "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib"
ECLimport "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib"
ECLimport "platform:/resource/de.fraunhofer.iem.swt.msd.tam.mocc/mocc/MSDLanguageComplete.moccml"
ECLimport "platform:/resource/de.fraunhofer.iem.swt.msd.tam.mocc/mocc/MSDfacilities.ccslLib"


package UML 

context Model
		def if(self.getAppliedStereotype('Modal::MSDSpecification') <> null) : globalTime: Event = self

	context Class 
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgCreateEvent: Event =self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgSendEvent: Event =self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgReceiveEvent: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgConsumeEvent: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : taskStartEvent: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : taskCompleteEvent: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : oneEvent:Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null and self.getValue(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'lowerPrio').oclAsType(Collection(Class))->size()>0) : notReady: Event = self

	context NamedElement
		def if((self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamSharedOSResource') <>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamPeripheryUnit') <>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamSensor')<>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamActuator')<>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamIO')<>null)
		): acquireRes: Event = self


		def if(((self.getAppliedStereotype('TAM::TAM_Platform::TAM_Communication::TamComConnection') <>null) 
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamOSComChannel') <> null)) and  
					((Abstraction).allInstances()->exists(a | (a).getAppliedStereotype('SysML::Allocations::Allocate') <> null and (a).supplier->includes(self)))
			) : acquireCon: Event = self

		def if((self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit') <>null) and  
					((Class).allInstances()->select(us |(us).getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null and (us).getValue((us).getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'TamPU').oclAsType(NamedElement) = self))->size()>0) : dispatch: Event = self
		def if((self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit') <>null) and  
					((Class).allInstances()->select(us |(us).getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null and (us).getValue((us).getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'TamPU').oclAsType(NamedElement) = self))->size()>0) 
					: numCores: Integer = self.getValue(self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit'), 'numCores').oclAsType(Integer)
		
		
		
		
	context Message
		def: msgCreateUnification:Event = self
		def: msgSendUnification:Event = self
		def: msgReceiveUnification:Event = self
		def: msgConsumeUnification:Event = self
		def: taskStartUnification:Event = self
		def: taskCompleteUnification:Event = self 


context CombinedFragment
		inv endToEndDelay_upperNotInitial:
				let expression : String = self.operand.guard.specification.stringValue()->first() in
				let clockName : String = (expression).substring(1, (expression).indexOf('=')-1).trim() in
				let ownIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(self) in
				let constraint : CombinedFragment = self.owner.oclAsType(Interaction).fragment->selectByType(CombinedFragment)->select(cb | ((cb).getAppliedStereotype('Modal::TimeCondition') <> null) and
																																			(self.owner.oclAsType(Interaction).fragment->indexOf(cb) > ownIndex) and(
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName).concat(' <='))	
																																				or
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName).concat('<='))
																																			)
																																																																		
				)->asSequence()->at(1) in
				let constrainedExpression : String = (constraint).operand.guard.specification.stringValue()->first() in
				
				
			(
				self.getAppliedStereotype('Modal::ClockReset') <> null 
				and (
					(constrainedExpression).startsWith((clockName).concat('<=')) or (constrainedExpression).startsWith((clockName).concat(' <='))
				) and
				ownIndex > 1
			)
			implies
			(
				let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
				
				let messageReceivesBeforeReset : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)				
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < ownIndex)->asSequence().message in
				
				let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message in
																																						
				let constrainedTaskCompletions_upperE : Event = Expression Sup((messageReceivesBeforeConstraint).taskCompleteUnification) in
				let activationTMsgReceives_upperE : Event = Expression Sup((messageReceivesBeforeReset).msgReceiveUnification) in
							
				let maxDelay_e2e_upperInitial: Integer = (constrainedExpression).substringAfter('<=').trim().toInteger() in
				
				let stimulusDelayedExpression_e2e_sE : Event = Expression DelayFor(activationTMsgReceives_upperE, self.getModel().globalTime, maxDelay_e2e_upperInitial) in
				
				Relation NonStrictAlternates(constrainedTaskCompletions_upperE,stimulusDelayedExpression_e2e_sE)
			)

			
			inv endToEndDelay_lowerNotInitial:
				let expression : String = self.operand.guard.specification.stringValue()->first() in
				let clockName : String = (expression).substring(1, (expression).indexOf('=')-1).trim() in
				let ownIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(self) in
				let constraint : CombinedFragment = self.owner.oclAsType(Interaction).fragment->selectByType(CombinedFragment)->select(cb | ((cb).getAppliedStereotype('Modal::TimeCondition') <> null) and
																																			(self.owner.oclAsType(Interaction).fragment->indexOf(cb) > ownIndex) and(
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName))	
																																				or
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName))
																																			)
																																																																		
				)->asSequence()->at(1) in
				let constrainedExpression : String = (constraint).operand.guard.specification.stringValue()->first() in
				
				
			(
				self.getAppliedStereotype('Modal::ClockReset') <> null 
				and (
					(constrainedExpression).startsWith((clockName).concat('>=')) or (constrainedExpression).startsWith((clockName).concat(' >='))
				) and
				ownIndex > 1
			)
			implies
			(
				let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
				
				let messageReceivesBeforeReset : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)				
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < ownIndex)->asSequence().message in
				
				let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message in
																																						
				let constrainedTaskCompletions_lowerNotInitial : Event = Expression Sup((messageReceivesBeforeConstraint).taskCompleteUnification) in
				let activationMsgReceives_lowerNotInitial : Event = Expression Sup((messageReceivesBeforeReset).msgReceiveUnification) in
							
				let maxDelay_e2e_lowerNotInitial: Integer = (constrainedExpression).substringAfter('>=').trim().toInteger() in
				
				let stimulusDelayedExpression_e2e_lowerNotInitial : Event = Expression DelayFor(activationMsgReceives_lowerNotInitial, self.getModel().globalTime, maxDelay_e2e_lowerNotInitial) in
				
				Relation NonStrictAlternates(stimulusDelayedExpression_e2e_lowerNotInitial, constrainedTaskCompletions_lowerNotInitial)
			)

			
			inv endToEndDelay_upperNotInitial_s:
				let expression : String = self.operand.guard.specification.stringValue()->first() in
				let clockName : String = (expression).substring(1, (expression).indexOf('=')-1).trim() in
				let ownIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(self) in
				let constraint : CombinedFragment = self.owner.oclAsType(Interaction).fragment->selectByType(CombinedFragment)->select(cb | ((cb).getAppliedStereotype('Modal::TimeCondition') <> null) and
																																			(self.owner.oclAsType(Interaction).fragment->indexOf(cb) > ownIndex) and(
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName).concat(' <'))	
																																				or
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName).concat('<'))
																																			)
																																																																		
				)->asSequence()->at(1) in
				let constrainedExpression : String = (constraint).operand.guard.specification.stringValue()->first() in
				
				
			(
				self.getAppliedStereotype('Modal::ClockReset') <> null 
				and ((constrainedExpression).startsWith((clockName).concat('<')) or (constrainedExpression).startsWith((clockName).concat(' <')))
				and ownIndex > 1
				and (constrainedExpression).indexOf('=') <=0
			)
			implies
			(
				let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
				
				let messageReceivesBeforeReset : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)				
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < ownIndex)->asSequence().message in
				
				let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message in
																																						
				let constrainedTaskCompletions_upperE_s : Event = Expression Sup((messageReceivesBeforeConstraint).taskCompleteUnification) in
				let activationmsgReceive_upperE_s : Event = Expression Sup((messageReceivesBeforeReset).msgReceiveUnification) in
							
				let maxDelay_e2e_upperNotInitial_s: Integer = (constrainedExpression).substringAfter('<').trim().toInteger() in
				
				let stimulusDelayedExpression_e2e_sE_s : Event = Expression DelayFor(activationmsgReceive_upperE_s, self.getModel().globalTime, maxDelay_e2e_upperNotInitial_s) in
				
				Relation Alternates(constrainedTaskCompletions_upperE_s,stimulusDelayedExpression_e2e_sE_s)
			)

			
			inv endToEndDelay_lowerNotInitial_s:
				let expression : String = self.operand.guard.specification.stringValue()->first() in
				let clockName : String = (expression).substring(1, (expression).indexOf('=')-1).trim() in
				let ownIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(self) in
				let constraint : CombinedFragment = self.owner.oclAsType(Interaction).fragment->selectByType(CombinedFragment)->select(cb | ((cb).getAppliedStereotype('Modal::TimeCondition') <> null) and
																																			(self.owner.oclAsType(Interaction).fragment->indexOf(cb) > ownIndex) and(
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName))	
																																				or
																																				(cb).operand.guard.specification.stringValue()->first().trim().startsWith((clockName))
																																			)
																																																																		
				)->asSequence()->at(1) in
				let constrainedExpression : String = (constraint).operand.guard.specification.stringValue()->first() in
				
				
			(
				self.getAppliedStereotype('Modal::ClockReset') <> null 
				and (
					(constrainedExpression).startsWith((clockName).concat('>')) or (constrainedExpression).startsWith((clockName).concat(' >'))
				) and (not((constrainedExpression).startsWith((clockName).concat('>=')) or (constrainedExpression).startsWith((clockName).concat(' >=')))) and
				ownIndex > 1
			)
			implies
			(
				let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
				
				let messageReceivesBeforeReset : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)				
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < ownIndex)->asSequence().message in
				
				let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | (mos).message._'receiveEvent' = (mos)
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message in
																																						
				let constrainedTaskCompletions_lowerNotInitial_s : Event = Expression Sup((messageReceivesBeforeConstraint).msgReceiveUnification) in
				let activationTaskCompletions_lowerNotInitial_s : Event = Expression Sup((messageReceivesBeforeReset).taskCompleteUnification) in
							
				let maxDelay_e2e_lowerNotInitial_s: Integer = (constrainedExpression).substringAfter('>').trim().toInteger() in
				
				let stimulusDelayedExpression_e2e_lowerNotInitial_s : Event = Expression DelayFor(activationTaskCompletions_lowerNotInitial_s, self.getModel().globalTime, maxDelay_e2e_lowerNotInitial_s) in
				
				Relation Alternates(stimulusDelayedExpression_e2e_lowerNotInitial_s, constrainedTaskCompletions_lowerNotInitial_s)
			)
endpackage
