/**
 */
package TAM.TAM_Platform.TAM_Communication;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaCommHost;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Com Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkOvhd <em>Network Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkBlockT <em>Network Block T</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getUsedProtocol <em>Used Protocol</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getUsedComServices <em>Used Com Services</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface1 <em>Interface1</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface2 <em>Interface2</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection()
 * @model
 * @generated
 */
public interface TamComConnection extends GaCommHost {
	/**
	 * Returns the value of the '<em><b>Network Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Network Ovhd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Network Ovhd</em>' attribute.
	 * @see #setNetworkOvhd(String)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection_NetworkOvhd()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getNetworkOvhd();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkOvhd <em>Network Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Network Ovhd</em>' attribute.
	 * @see #getNetworkOvhd()
	 * @generated
	 */
	void setNetworkOvhd(String value);

	/**
	 * Returns the value of the '<em><b>Network Block T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Network Block T</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Network Block T</em>' attribute.
	 * @see #setNetworkBlockT(String)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection_NetworkBlockT()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getNetworkBlockT();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getNetworkBlockT <em>Network Block T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Network Block T</em>' attribute.
	 * @see #getNetworkBlockT()
	 * @generated
	 */
	void setNetworkBlockT(String value);

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_Communication.TamComInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interfaces</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection_Interfaces()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamComInterface> getInterfaces();

	/**
	 * Returns the value of the '<em><b>Used Protocol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Protocol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used Protocol</em>' reference.
	 * @see #setUsedProtocol(TamProtocol)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection_UsedProtocol()
	 * @model ordered="false"
	 * @generated
	 */
	TamProtocol getUsedProtocol();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getUsedProtocol <em>Used Protocol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Used Protocol</em>' reference.
	 * @see #getUsedProtocol()
	 * @generated
	 */
	void setUsedProtocol(TamProtocol value);

	/**
	 * Returns the value of the '<em><b>Used Com Services</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_Communication.TamComService}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used Com Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used Com Services</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection_UsedComServices()
	 * @model
	 * @generated
	 */
	EList<TamComService> getUsedComServices();

	/**
	 * Returns the value of the '<em><b>Interface1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface1</em>' reference.
	 * @see #setInterface1(TamComInterface)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection_Interface1()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	TamComInterface getInterface1();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface1 <em>Interface1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface1</em>' reference.
	 * @see #getInterface1()
	 * @generated
	 */
	void setInterface1(TamComInterface value);

	/**
	 * Returns the value of the '<em><b>Interface2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface2</em>' reference.
	 * @see #setInterface2(TamComInterface)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComConnection_Interface2()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	TamComInterface getInterface2();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamComConnection#getInterface2 <em>Interface2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface2</em>' reference.
	 * @see #getInterface2()
	 * @generated
	 */
	void setInterface2(TamComInterface value);

} // TamComConnection
