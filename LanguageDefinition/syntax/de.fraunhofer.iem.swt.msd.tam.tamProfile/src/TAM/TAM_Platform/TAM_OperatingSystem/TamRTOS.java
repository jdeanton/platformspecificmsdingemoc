/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam RTOS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getScheduler <em>Scheduler</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getComChannels <em>Com Channels</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getSharedRes <em>Shared Res</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getOsServices <em>Os Services</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamRTOS()
 * @model
 * @generated
 */
public interface TamRTOS extends TamOSResource {
	/**
	 * Returns the value of the '<em><b>Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduler</em>' reference.
	 * @see #setScheduler(TamScheduler)
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamRTOS_Scheduler()
	 * @model ordered="false"
	 * @generated
	 */
	TamScheduler getScheduler();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getScheduler <em>Scheduler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduler</em>' reference.
	 * @see #getScheduler()
	 * @generated
	 */
	void setScheduler(TamScheduler value);

	/**
	 * Returns the value of the '<em><b>Com Channels</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSComChannel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Com Channels</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Com Channels</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamRTOS_ComChannels()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamOSComChannel> getComChannels();

	/**
	 * Returns the value of the '<em><b>Shared Res</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Res</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Res</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamRTOS_SharedRes()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamSharedOSResource> getSharedRes();

	/**
	 * Returns the value of the '<em><b>Os Services</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSService}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Os Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os Services</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#getTamRTOS_OsServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamOSService> getOsServices();

} // TamRTOS
