/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem.impl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSService;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam OS Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TamOSServiceImpl extends TamOSResourceImpl implements TamOSService {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamOSServiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_OperatingSystemPackage.Literals.TAM_OS_SERVICE;
	}

} //TamOSServiceImpl
