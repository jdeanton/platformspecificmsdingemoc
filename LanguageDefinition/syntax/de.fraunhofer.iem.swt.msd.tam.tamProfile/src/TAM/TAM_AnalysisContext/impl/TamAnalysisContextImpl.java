/**
 */
package TAM.TAM_AnalysisContext.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;
import TAM.TAM_AnalysisContext.TamAnalysisContext;

import TAM.TAM_AnalysisContext.util.TAM_AnalysisContextValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaAnalysisContextImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Analysis Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_AnalysisContext.impl.TamAnalysisContextImpl#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link TAM.TAM_AnalysisContext.impl.TamAnalysisContextImpl#getConstraintsSatisfied <em>Constraints Satisfied</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamAnalysisContextImpl extends GaAnalysisContextImpl implements TamAnalysisContext {
	/**
	 * The default value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected static final String EXECUTION_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected String executionTime = EXECUTION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getConstraintsSatisfied() <em>Constraints Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintsSatisfied()
	 * @generated
	 * @ordered
	 */
	protected static final String CONSTRAINTS_SATISFIED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConstraintsSatisfied() <em>Constraints Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintsSatisfied()
	 * @generated
	 * @ordered
	 */
	protected String constraintsSatisfied = CONSTRAINTS_SATISFIED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamAnalysisContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_AnalysisContextPackage.Literals.TAM_ANALYSIS_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExecutionTime() {
		return executionTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionTime(String newExecutionTime) {
		String oldExecutionTime = executionTime;
		executionTime = newExecutionTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__EXECUTION_TIME, oldExecutionTime, executionTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConstraintsSatisfied() {
		return constraintsSatisfied;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraintsSatisfied(String newConstraintsSatisfied) {
		String oldConstraintsSatisfied = constraintsSatisfied;
		constraintsSatisfied = newConstraintsSatisfied;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED, oldConstraintsSatisfied, constraintsSatisfied));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean Constraint1(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TAM_AnalysisContextValidator.DIAGNOSTIC_SOURCE,
						 TAM_AnalysisContextValidator.TAM_ANALYSIS_CONTEXT__CONSTRAINT1,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "Constraint1", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__EXECUTION_TIME:
				return getExecutionTime();
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED:
				return getConstraintsSatisfied();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__EXECUTION_TIME:
				setExecutionTime((String)newValue);
				return;
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED:
				setConstraintsSatisfied((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__EXECUTION_TIME:
				setExecutionTime(EXECUTION_TIME_EDEFAULT);
				return;
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED:
				setConstraintsSatisfied(CONSTRAINTS_SATISFIED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__EXECUTION_TIME:
				return EXECUTION_TIME_EDEFAULT == null ? executionTime != null : !EXECUTION_TIME_EDEFAULT.equals(executionTime);
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED:
				return CONSTRAINTS_SATISFIED_EDEFAULT == null ? constraintsSatisfied != null : !CONSTRAINTS_SATISFIED_EDEFAULT.equals(constraintsSatisfied);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT___CONSTRAINT1__DIAGNOSTICCHAIN_MAP:
				return Constraint1((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executionTime: ");
		result.append(executionTime);
		result.append(", constraintsSatisfied: ");
		result.append(constraintsSatisfied);
		result.append(')');
		return result.toString();
	}

} //TamAnalysisContextImpl
