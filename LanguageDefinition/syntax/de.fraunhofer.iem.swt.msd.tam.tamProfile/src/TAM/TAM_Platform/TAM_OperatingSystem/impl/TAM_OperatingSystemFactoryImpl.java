/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem.impl;

import TAM.TAM_Platform.TAM_OperatingSystem.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_OperatingSystemFactoryImpl extends EFactoryImpl implements TAM_OperatingSystemFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TAM_OperatingSystemFactory init() {
		try {
			TAM_OperatingSystemFactory theTAM_OperatingSystemFactory = (TAM_OperatingSystemFactory)EPackage.Registry.INSTANCE.getEFactory(TAM_OperatingSystemPackage.eNS_URI);
			if (theTAM_OperatingSystemFactory != null) {
				return theTAM_OperatingSystemFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TAM_OperatingSystemFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_OperatingSystemFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TAM_OperatingSystemPackage.TAM_OS_RESOURCE: return createTamOSResource();
			case TAM_OperatingSystemPackage.TAM_SCHEDULER: return createTamScheduler();
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL: return createTamOSComChannel();
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE: return createTamSharedOSResource();
			case TAM_OperatingSystemPackage.TAM_OS_SERVICE: return createTamOSService();
			case TAM_OperatingSystemPackage.TAM_RTOS: return createTamRTOS();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamOSResource createTamOSResource() {
		TamOSResourceImpl tamOSResource = new TamOSResourceImpl();
		return tamOSResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamScheduler createTamScheduler() {
		TamSchedulerImpl tamScheduler = new TamSchedulerImpl();
		return tamScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamOSComChannel createTamOSComChannel() {
		TamOSComChannelImpl tamOSComChannel = new TamOSComChannelImpl();
		return tamOSComChannel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamSharedOSResource createTamSharedOSResource() {
		TamSharedOSResourceImpl tamSharedOSResource = new TamSharedOSResourceImpl();
		return tamSharedOSResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamOSService createTamOSService() {
		TamOSServiceImpl tamOSService = new TamOSServiceImpl();
		return tamOSService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamRTOS createTamRTOS() {
		TamRTOSImpl tamRTOS = new TamRTOSImpl();
		return tamRTOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_OperatingSystemPackage getTAM_OperatingSystemPackage() {
		return (TAM_OperatingSystemPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TAM_OperatingSystemPackage getPackage() {
		return TAM_OperatingSystemPackage.eINSTANCE;
	}

} //TAM_OperatingSystemFactoryImpl
