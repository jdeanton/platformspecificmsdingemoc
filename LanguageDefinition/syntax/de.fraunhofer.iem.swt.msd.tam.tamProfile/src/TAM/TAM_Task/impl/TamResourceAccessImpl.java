/**
 */
package TAM.TAM_Task.impl;

import TAM.TAM_Task.TAM_TaskPackage;
import TAM.TAM_Task.TamPeripheryType;
import TAM.TAM_Task.TamResourceAccess;
import TAM.TAM_Task.TamSharedOSResourcType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.ResourceUsageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Resource Access</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.impl.TamResourceAccessImpl#getNumAccess <em>Num Access</em>}</li>
 *   <li>{@link TAM.TAM_Task.impl.TamResourceAccessImpl#getPerResource <em>Per Resource</em>}</li>
 *   <li>{@link TAM.TAM_Task.impl.TamResourceAccessImpl#getOsResource <em>Os Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamResourceAccessImpl extends ResourceUsageImpl implements TamResourceAccess {
	/**
	 * The default value of the '{@link #getNumAccess() <em>Num Access</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumAccess()
	 * @generated
	 * @ordered
	 */
	protected static final String NUM_ACCESS_EDEFAULT = "1";

	/**
	 * The cached value of the '{@link #getNumAccess() <em>Num Access</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumAccess()
	 * @generated
	 * @ordered
	 */
	protected String numAccess = NUM_ACCESS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPerResource() <em>Per Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerResource()
	 * @generated
	 * @ordered
	 */
	protected TamPeripheryType perResource;

	/**
	 * The cached value of the '{@link #getOsResource() <em>Os Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsResource()
	 * @generated
	 * @ordered
	 */
	protected TamSharedOSResourcType osResource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamResourceAccessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_TaskPackage.Literals.TAM_RESOURCE_ACCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNumAccess() {
		return numAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumAccess(String newNumAccess) {
		String oldNumAccess = numAccess;
		numAccess = newNumAccess;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_RESOURCE_ACCESS__NUM_ACCESS, oldNumAccess, numAccess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamPeripheryType getPerResource() {
		if (perResource != null && perResource.eIsProxy()) {
			InternalEObject oldPerResource = (InternalEObject)perResource;
			perResource = (TamPeripheryType)eResolveProxy(oldPerResource);
			if (perResource != oldPerResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_TaskPackage.TAM_RESOURCE_ACCESS__PER_RESOURCE, oldPerResource, perResource));
			}
		}
		return perResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamPeripheryType basicGetPerResource() {
		return perResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPerResource(TamPeripheryType newPerResource) {
		TamPeripheryType oldPerResource = perResource;
		perResource = newPerResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_RESOURCE_ACCESS__PER_RESOURCE, oldPerResource, perResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamSharedOSResourcType getOsResource() {
		if (osResource != null && osResource.eIsProxy()) {
			InternalEObject oldOsResource = (InternalEObject)osResource;
			osResource = (TamSharedOSResourcType)eResolveProxy(oldOsResource);
			if (osResource != oldOsResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_TaskPackage.TAM_RESOURCE_ACCESS__OS_RESOURCE, oldOsResource, osResource));
			}
		}
		return osResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamSharedOSResourcType basicGetOsResource() {
		return osResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOsResource(TamSharedOSResourcType newOsResource) {
		TamSharedOSResourcType oldOsResource = osResource;
		osResource = newOsResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_RESOURCE_ACCESS__OS_RESOURCE, oldOsResource, osResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__NUM_ACCESS:
				return getNumAccess();
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__PER_RESOURCE:
				if (resolve) return getPerResource();
				return basicGetPerResource();
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__OS_RESOURCE:
				if (resolve) return getOsResource();
				return basicGetOsResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__NUM_ACCESS:
				setNumAccess((String)newValue);
				return;
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__PER_RESOURCE:
				setPerResource((TamPeripheryType)newValue);
				return;
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__OS_RESOURCE:
				setOsResource((TamSharedOSResourcType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__NUM_ACCESS:
				setNumAccess(NUM_ACCESS_EDEFAULT);
				return;
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__PER_RESOURCE:
				setPerResource((TamPeripheryType)null);
				return;
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__OS_RESOURCE:
				setOsResource((TamSharedOSResourcType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__NUM_ACCESS:
				return NUM_ACCESS_EDEFAULT == null ? numAccess != null : !NUM_ACCESS_EDEFAULT.equals(numAccess);
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__PER_RESOURCE:
				return perResource != null;
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS__OS_RESOURCE:
				return osResource != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (numAccess: ");
		result.append(numAccess);
		result.append(')');
		return result.toString();
	}

} //TamResourceAccessImpl
