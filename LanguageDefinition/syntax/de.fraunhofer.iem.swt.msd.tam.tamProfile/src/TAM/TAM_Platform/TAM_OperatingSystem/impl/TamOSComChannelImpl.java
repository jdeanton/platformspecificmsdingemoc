/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem.impl;

import TAM.TAM_Platform.TAM_Communication.impl.TamComConnectionImpl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSComChannel;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam OS Com Channel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSComChannelImpl#getMemFootprint <em>Mem Footprint</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSComChannelImpl#getBackgroundUtilization <em>Background Utilization</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamOSComChannelImpl extends TamComConnectionImpl implements TamOSComChannel {
	/**
	 * The default value of the '{@link #getMemFootprint() <em>Mem Footprint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemFootprint()
	 * @generated
	 * @ordered
	 */
	protected static final String MEM_FOOTPRINT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMemFootprint() <em>Mem Footprint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemFootprint()
	 * @generated
	 * @ordered
	 */
	protected String memFootprint = MEM_FOOTPRINT_EDEFAULT;

	/**
	 * The default value of the '{@link #getBackgroundUtilization() <em>Background Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackgroundUtilization()
	 * @generated
	 * @ordered
	 */
	protected static final String BACKGROUND_UTILIZATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBackgroundUtilization() <em>Background Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackgroundUtilization()
	 * @generated
	 * @ordered
	 */
	protected String backgroundUtilization = BACKGROUND_UTILIZATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamOSComChannelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_OperatingSystemPackage.Literals.TAM_OS_COM_CHANNEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMemFootprint() {
		return memFootprint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemFootprint(String newMemFootprint) {
		String oldMemFootprint = memFootprint;
		memFootprint = newMemFootprint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__MEM_FOOTPRINT, oldMemFootprint, memFootprint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBackgroundUtilization() {
		return backgroundUtilization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBackgroundUtilization(String newBackgroundUtilization) {
		String oldBackgroundUtilization = backgroundUtilization;
		backgroundUtilization = newBackgroundUtilization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION, oldBackgroundUtilization, backgroundUtilization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__MEM_FOOTPRINT:
				return getMemFootprint();
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION:
				return getBackgroundUtilization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__MEM_FOOTPRINT:
				setMemFootprint((String)newValue);
				return;
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION:
				setBackgroundUtilization((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__MEM_FOOTPRINT:
				setMemFootprint(MEM_FOOTPRINT_EDEFAULT);
				return;
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION:
				setBackgroundUtilization(BACKGROUND_UTILIZATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__MEM_FOOTPRINT:
				return MEM_FOOTPRINT_EDEFAULT == null ? memFootprint != null : !MEM_FOOTPRINT_EDEFAULT.equals(memFootprint);
			case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION:
				return BACKGROUND_UTILIZATION_EDEFAULT == null ? backgroundUtilization != null : !BACKGROUND_UTILIZATION_EDEFAULT.equals(backgroundUtilization);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == TamOSResource.class) {
			switch (derivedFeatureID) {
				case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__MEM_FOOTPRINT: return TAM_OperatingSystemPackage.TAM_OS_RESOURCE__MEM_FOOTPRINT;
				case TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION: return TAM_OperatingSystemPackage.TAM_OS_RESOURCE__BACKGROUND_UTILIZATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == TamOSResource.class) {
			switch (baseFeatureID) {
				case TAM_OperatingSystemPackage.TAM_OS_RESOURCE__MEM_FOOTPRINT: return TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__MEM_FOOTPRINT;
				case TAM_OperatingSystemPackage.TAM_OS_RESOURCE__BACKGROUND_UTILIZATION: return TAM_OperatingSystemPackage.TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (memFootprint: ");
		result.append(memFootprint);
		result.append(", backgroundUtilization: ");
		result.append(backgroundUtilization);
		result.append(')');
		return result.toString();
	}

} //TamOSComChannelImpl
