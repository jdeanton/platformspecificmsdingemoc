/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage
 * @generated
 */
public interface TAM_ControlUnitFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_ControlUnitFactory eINSTANCE = TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Tam Memory Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Memory Unit</em>'.
	 * @generated
	 */
	TamMemoryUnit createTamMemoryUnit();

	/**
	 * Returns a new object of class '<em>Tam Processing Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Processing Unit</em>'.
	 * @generated
	 */
	TamProcessingUnit createTamProcessingUnit();

	/**
	 * Returns a new object of class '<em>Tam Sensor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Sensor</em>'.
	 * @generated
	 */
	TamSensor createTamSensor();

	/**
	 * Returns a new object of class '<em>Tam IO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam IO</em>'.
	 * @generated
	 */
	TamIO createTamIO();

	/**
	 * Returns a new object of class '<em>Tam Actuator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Actuator</em>'.
	 * @generated
	 */
	TamActuator createTamActuator();

	/**
	 * Returns a new object of class '<em>Tam ECU</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam ECU</em>'.
	 * @generated
	 */
	TamECU createTamECU();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TAM_ControlUnitPackage getTAM_ControlUnitPackage();

} //TAM_ControlUnitFactory
