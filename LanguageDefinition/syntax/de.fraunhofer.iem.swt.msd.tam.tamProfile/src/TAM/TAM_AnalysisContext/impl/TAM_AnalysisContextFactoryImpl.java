/**
 */
package TAM.TAM_AnalysisContext.impl;

import TAM.TAM_AnalysisContext.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_AnalysisContextFactoryImpl extends EFactoryImpl implements TAM_AnalysisContextFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TAM_AnalysisContextFactory init() {
		try {
			TAM_AnalysisContextFactory theTAM_AnalysisContextFactory = (TAM_AnalysisContextFactory)EPackage.Registry.INSTANCE.getEFactory(TAM_AnalysisContextPackage.eNS_URI);
			if (theTAM_AnalysisContextFactory != null) {
				return theTAM_AnalysisContextFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TAM_AnalysisContextFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_AnalysisContextFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TAM_AnalysisContextPackage.TAM_WORKLOAD_BEHAVIOR: return createTamWorkloadBehavior();
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT: return createTamAnalysisContext();
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM: return createTamResourcePlatform();
			case TAM_AnalysisContextPackage.TAM_ASSUMPTION_MSD: return createTamAssumptionMSD();
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD: return createTamRequirementMSD();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamWorkloadBehavior createTamWorkloadBehavior() {
		TamWorkloadBehaviorImpl tamWorkloadBehavior = new TamWorkloadBehaviorImpl();
		return tamWorkloadBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamAnalysisContext createTamAnalysisContext() {
		TamAnalysisContextImpl tamAnalysisContext = new TamAnalysisContextImpl();
		return tamAnalysisContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamResourcePlatform createTamResourcePlatform() {
		TamResourcePlatformImpl tamResourcePlatform = new TamResourcePlatformImpl();
		return tamResourcePlatform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamAssumptionMSD createTamAssumptionMSD() {
		TamAssumptionMSDImpl tamAssumptionMSD = new TamAssumptionMSDImpl();
		return tamAssumptionMSD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamRequirementMSD createTamRequirementMSD() {
		TamRequirementMSDImpl tamRequirementMSD = new TamRequirementMSDImpl();
		return tamRequirementMSD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_AnalysisContextPackage getTAM_AnalysisContextPackage() {
		return (TAM_AnalysisContextPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TAM_AnalysisContextPackage getPackage() {
		return TAM_AnalysisContextPackage.eINSTANCE;
	}

} //TAM_AnalysisContextFactoryImpl
