/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem.impl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource;
import TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource;

import TAM.TAM_Task.TamSharedOSResourcType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.MutualExclusionResourceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Shared OS Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl#getMemFootprint <em>Mem Footprint</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl#getBackgroundUtilization <em>Background Utilization</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl#getAccessDelay <em>Access Delay</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamSharedOSResourceImpl extends MutualExclusionResourceImpl implements TamSharedOSResource {
	/**
	 * The default value of the '{@link #getMemFootprint() <em>Mem Footprint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemFootprint()
	 * @generated
	 * @ordered
	 */
	protected static final String MEM_FOOTPRINT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMemFootprint() <em>Mem Footprint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMemFootprint()
	 * @generated
	 * @ordered
	 */
	protected String memFootprint = MEM_FOOTPRINT_EDEFAULT;

	/**
	 * The default value of the '{@link #getBackgroundUtilization() <em>Background Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackgroundUtilization()
	 * @generated
	 * @ordered
	 */
	protected static final String BACKGROUND_UTILIZATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBackgroundUtilization() <em>Background Utilization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackgroundUtilization()
	 * @generated
	 * @ordered
	 */
	protected String backgroundUtilization = BACKGROUND_UTILIZATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getAccessDelay() <em>Access Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessDelay()
	 * @generated
	 * @ordered
	 */
	protected static final String ACCESS_DELAY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAccessDelay() <em>Access Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessDelay()
	 * @generated
	 * @ordered
	 */
	protected String accessDelay = ACCESS_DELAY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected TamSharedOSResourcType type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamSharedOSResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_OperatingSystemPackage.Literals.TAM_SHARED_OS_RESOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMemFootprint() {
		return memFootprint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMemFootprint(String newMemFootprint) {
		String oldMemFootprint = memFootprint;
		memFootprint = newMemFootprint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT, oldMemFootprint, memFootprint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBackgroundUtilization() {
		return backgroundUtilization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBackgroundUtilization(String newBackgroundUtilization) {
		String oldBackgroundUtilization = backgroundUtilization;
		backgroundUtilization = newBackgroundUtilization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION, oldBackgroundUtilization, backgroundUtilization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAccessDelay() {
		return accessDelay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessDelay(String newAccessDelay) {
		String oldAccessDelay = accessDelay;
		accessDelay = newAccessDelay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__ACCESS_DELAY, oldAccessDelay, accessDelay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamSharedOSResourcType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (TamSharedOSResourcType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamSharedOSResourcType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(TamSharedOSResourcType newType) {
		TamSharedOSResourcType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT:
				return getMemFootprint();
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION:
				return getBackgroundUtilization();
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__ACCESS_DELAY:
				return getAccessDelay();
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__TYPE:
				if (resolve) return getType();
				return basicGetType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT:
				setMemFootprint((String)newValue);
				return;
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION:
				setBackgroundUtilization((String)newValue);
				return;
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__ACCESS_DELAY:
				setAccessDelay((String)newValue);
				return;
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__TYPE:
				setType((TamSharedOSResourcType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT:
				setMemFootprint(MEM_FOOTPRINT_EDEFAULT);
				return;
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION:
				setBackgroundUtilization(BACKGROUND_UTILIZATION_EDEFAULT);
				return;
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__ACCESS_DELAY:
				setAccessDelay(ACCESS_DELAY_EDEFAULT);
				return;
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__TYPE:
				setType((TamSharedOSResourcType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT:
				return MEM_FOOTPRINT_EDEFAULT == null ? memFootprint != null : !MEM_FOOTPRINT_EDEFAULT.equals(memFootprint);
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION:
				return BACKGROUND_UTILIZATION_EDEFAULT == null ? backgroundUtilization != null : !BACKGROUND_UTILIZATION_EDEFAULT.equals(backgroundUtilization);
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__ACCESS_DELAY:
				return ACCESS_DELAY_EDEFAULT == null ? accessDelay != null : !ACCESS_DELAY_EDEFAULT.equals(accessDelay);
			case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__TYPE:
				return type != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == TamOSResource.class) {
			switch (derivedFeatureID) {
				case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT: return TAM_OperatingSystemPackage.TAM_OS_RESOURCE__MEM_FOOTPRINT;
				case TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION: return TAM_OperatingSystemPackage.TAM_OS_RESOURCE__BACKGROUND_UTILIZATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == TamOSResource.class) {
			switch (baseFeatureID) {
				case TAM_OperatingSystemPackage.TAM_OS_RESOURCE__MEM_FOOTPRINT: return TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT;
				case TAM_OperatingSystemPackage.TAM_OS_RESOURCE__BACKGROUND_UTILIZATION: return TAM_OperatingSystemPackage.TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (memFootprint: ");
		result.append(memFootprint);
		result.append(", backgroundUtilization: ");
		result.append(backgroundUtilization);
		result.append(", accessDelay: ");
		result.append(accessDelay);
		result.append(')');
		return result.toString();
	}

} //TamSharedOSResourceImpl
