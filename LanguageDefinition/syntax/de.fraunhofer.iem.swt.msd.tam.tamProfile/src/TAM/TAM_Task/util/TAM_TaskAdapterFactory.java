/**
 */
package TAM.TAM_Task.util;

import TAM.TAM_Task.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see TAM.TAM_Task.TAM_TaskPackage
 * @generated
 */
public class TAM_TaskAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TAM_TaskPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_TaskAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TAM_TaskPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TAM_TaskSwitch<Adapter> modelSwitch =
		new TAM_TaskSwitch<Adapter>() {
			@Override
			public Adapter caseTamMemReq(TamMemReq object) {
				return createTamMemReqAdapter();
			}
			@Override
			public Adapter caseTamInterfaceImplementation(TamInterfaceImplementation object) {
				return createTamInterfaceImplementationAdapter();
			}
			@Override
			public Adapter caseTamPeripheryType(TamPeripheryType object) {
				return createTamPeripheryTypeAdapter();
			}
			@Override
			public Adapter caseTamSharedOSResourcType(TamSharedOSResourcType object) {
				return createTamSharedOSResourcTypeAdapter();
			}
			@Override
			public Adapter caseTamResourceAccess(TamResourceAccess object) {
				return createTamResourceAccessAdapter();
			}
			@Override
			public Adapter caseTamOperation(TamOperation object) {
				return createTamOperationAdapter();
			}
			@Override
			public Adapter caseTamMemType(TamMemType object) {
				return createTamMemTypeAdapter();
			}
			@Override
			public Adapter caseResourceUsage(ResourceUsage object) {
				return createResourceUsageAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link TAM.TAM_Task.TamMemReq <em>Tam Mem Req</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TAM.TAM_Task.TamMemReq
	 * @generated
	 */
	public Adapter createTamMemReqAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TAM.TAM_Task.TamInterfaceImplementation <em>Tam Interface Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TAM.TAM_Task.TamInterfaceImplementation
	 * @generated
	 */
	public Adapter createTamInterfaceImplementationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TAM.TAM_Task.TamPeripheryType <em>Tam Periphery Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TAM.TAM_Task.TamPeripheryType
	 * @generated
	 */
	public Adapter createTamPeripheryTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TAM.TAM_Task.TamSharedOSResourcType <em>Tam Shared OS Resourc Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TAM.TAM_Task.TamSharedOSResourcType
	 * @generated
	 */
	public Adapter createTamSharedOSResourcTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TAM.TAM_Task.TamResourceAccess <em>Tam Resource Access</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TAM.TAM_Task.TamResourceAccess
	 * @generated
	 */
	public Adapter createTamResourceAccessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TAM.TAM_Task.TamOperation <em>Tam Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TAM.TAM_Task.TamOperation
	 * @generated
	 */
	public Adapter createTamOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link TAM.TAM_Task.TamMemType <em>Tam Mem Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see TAM.TAM_Task.TamMemType
	 * @generated
	 */
	public Adapter createTamMemTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage <em>Resource Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage
	 * @generated
	 */
	public Adapter createResourceUsageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TAM_TaskAdapterFactory
