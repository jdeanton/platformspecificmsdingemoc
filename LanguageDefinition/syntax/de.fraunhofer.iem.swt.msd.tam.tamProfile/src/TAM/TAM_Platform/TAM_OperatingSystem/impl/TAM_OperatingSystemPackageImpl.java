/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;

import TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;

import TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;

import TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemFactory;
import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSComChannel;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSService;
import TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS;
import TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler;
import TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource;

import TAM.TAM_Task.TAM_TaskPackage;

import TAM.TAM_Task.impl.TAM_TaskPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.uml2.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_OperatingSystemPackageImpl extends EPackageImpl implements TAM_OperatingSystemPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamOSResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamSchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamOSComChannelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamSharedOSResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamOSServiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamRTOSEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TAM_OperatingSystemPackageImpl() {
		super(eNS_URI, TAM_OperatingSystemFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TAM_OperatingSystemPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TAM_OperatingSystemPackage init() {
		if (isInited) return (TAM_OperatingSystemPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI);

		// Obtain or create and register package
		TAM_OperatingSystemPackageImpl theTAM_OperatingSystemPackage = (TAM_OperatingSystemPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TAM_OperatingSystemPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TAM_OperatingSystemPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MARTEPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TAM_AnalysisContextPackageImpl theTAM_AnalysisContextPackage = (TAM_AnalysisContextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) instanceof TAM_AnalysisContextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) : TAM_AnalysisContextPackage.eINSTANCE);
		TAM_ControlUnitPackageImpl theTAM_ControlUnitPackage = (TAM_ControlUnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) instanceof TAM_ControlUnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) : TAM_ControlUnitPackage.eINSTANCE);
		TAM_CommunicationPackageImpl theTAM_CommunicationPackage = (TAM_CommunicationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) instanceof TAM_CommunicationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) : TAM_CommunicationPackage.eINSTANCE);
		TAM_TaskPackageImpl theTAM_TaskPackage = (TAM_TaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) instanceof TAM_TaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) : TAM_TaskPackage.eINSTANCE);

		// Create package meta-data objects
		theTAM_OperatingSystemPackage.createPackageContents();
		theTAM_AnalysisContextPackage.createPackageContents();
		theTAM_ControlUnitPackage.createPackageContents();
		theTAM_CommunicationPackage.createPackageContents();
		theTAM_TaskPackage.createPackageContents();

		// Initialize created meta-data
		theTAM_OperatingSystemPackage.initializePackageContents();
		theTAM_AnalysisContextPackage.initializePackageContents();
		theTAM_ControlUnitPackage.initializePackageContents();
		theTAM_CommunicationPackage.initializePackageContents();
		theTAM_TaskPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTAM_OperatingSystemPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TAM_OperatingSystemPackage.eNS_URI, theTAM_OperatingSystemPackage);
		return theTAM_OperatingSystemPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamOSResource() {
		return tamOSResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamOSResource_MemFootprint() {
		return (EAttribute)tamOSResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamOSResource_BackgroundUtilization() {
		return (EAttribute)tamOSResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamScheduler() {
		return tamSchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamScheduler_TaskSwitchT() {
		return (EAttribute)tamSchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamOSComChannel() {
		return tamOSComChannelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamSharedOSResource() {
		return tamSharedOSResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamSharedOSResource_AccessDelay() {
		return (EAttribute)tamSharedOSResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamSharedOSResource_Type() {
		return (EReference)tamSharedOSResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamOSService() {
		return tamOSServiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamRTOS() {
		return tamRTOSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamRTOS_Scheduler() {
		return (EReference)tamRTOSEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamRTOS_ComChannels() {
		return (EReference)tamRTOSEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamRTOS_SharedRes() {
		return (EReference)tamRTOSEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamRTOS_OsServices() {
		return (EReference)tamRTOSEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_OperatingSystemFactory getTAM_OperatingSystemFactory() {
		return (TAM_OperatingSystemFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tamOSResourceEClass = createEClass(TAM_OS_RESOURCE);
		createEAttribute(tamOSResourceEClass, TAM_OS_RESOURCE__MEM_FOOTPRINT);
		createEAttribute(tamOSResourceEClass, TAM_OS_RESOURCE__BACKGROUND_UTILIZATION);

		tamSchedulerEClass = createEClass(TAM_SCHEDULER);
		createEAttribute(tamSchedulerEClass, TAM_SCHEDULER__TASK_SWITCH_T);

		tamOSComChannelEClass = createEClass(TAM_OS_COM_CHANNEL);

		tamSharedOSResourceEClass = createEClass(TAM_SHARED_OS_RESOURCE);
		createEAttribute(tamSharedOSResourceEClass, TAM_SHARED_OS_RESOURCE__ACCESS_DELAY);
		createEReference(tamSharedOSResourceEClass, TAM_SHARED_OS_RESOURCE__TYPE);

		tamOSServiceEClass = createEClass(TAM_OS_SERVICE);

		tamRTOSEClass = createEClass(TAM_RTOS);
		createEReference(tamRTOSEClass, TAM_RTOS__SCHEDULER);
		createEReference(tamRTOSEClass, TAM_RTOS__COM_CHANNELS);
		createEReference(tamRTOSEClass, TAM_RTOS__SHARED_RES);
		createEReference(tamRTOSEClass, TAM_RTOS__OS_SERVICES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GRMPackage theGRMPackage = (GRMPackage)EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		TAM_CommunicationPackage theTAM_CommunicationPackage = (TAM_CommunicationPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI);
		TAM_TaskPackage theTAM_TaskPackage = (TAM_TaskPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tamOSResourceEClass.getESuperTypes().add(theGRMPackage.getResource());
		tamSchedulerEClass.getESuperTypes().add(theGRMPackage.getScheduler());
		tamSchedulerEClass.getESuperTypes().add(this.getTamOSResource());
		tamOSComChannelEClass.getESuperTypes().add(theTAM_CommunicationPackage.getTamComConnection());
		tamOSComChannelEClass.getESuperTypes().add(this.getTamOSResource());
		tamSharedOSResourceEClass.getESuperTypes().add(theGRMPackage.getMutualExclusionResource());
		tamSharedOSResourceEClass.getESuperTypes().add(this.getTamOSResource());
		tamOSServiceEClass.getESuperTypes().add(this.getTamOSResource());
		tamRTOSEClass.getESuperTypes().add(this.getTamOSResource());

		// Initialize classes, features, and operations; add parameters
		initEClass(tamOSResourceEClass, TamOSResource.class, "TamOSResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamOSResource_MemFootprint(), theTypesPackage.getString(), "memFootprint", null, 0, 1, TamOSResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamOSResource_BackgroundUtilization(), theTypesPackage.getString(), "backgroundUtilization", null, 0, 1, TamOSResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamSchedulerEClass, TamScheduler.class, "TamScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamScheduler_TaskSwitchT(), theTypesPackage.getString(), "taskSwitchT", null, 0, 1, TamScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamOSComChannelEClass, TamOSComChannel.class, "TamOSComChannel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tamSharedOSResourceEClass, TamSharedOSResource.class, "TamSharedOSResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamSharedOSResource_AccessDelay(), theTypesPackage.getString(), "accessDelay", null, 0, 1, TamSharedOSResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamSharedOSResource_Type(), theTAM_TaskPackage.getTamSharedOSResourcType(), null, "type", null, 1, 1, TamSharedOSResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamOSServiceEClass, TamOSService.class, "TamOSService", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tamRTOSEClass, TamRTOS.class, "TamRTOS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamRTOS_Scheduler(), this.getTamScheduler(), null, "scheduler", null, 0, 1, TamRTOS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamRTOS_ComChannels(), this.getTamOSComChannel(), null, "comChannels", null, 0, -1, TamRTOS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamRTOS_SharedRes(), this.getTamSharedOSResource(), null, "sharedRes", null, 0, -1, TamRTOS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamRTOS_OsServices(), this.getTamOSService(), null, "osServices", null, 0, -1, TamRTOS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TAM_OperatingSystemPackageImpl
