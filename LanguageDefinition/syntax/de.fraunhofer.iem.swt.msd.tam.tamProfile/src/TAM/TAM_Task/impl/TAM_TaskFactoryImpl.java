/**
 */
package TAM.TAM_Task.impl;

import TAM.TAM_Task.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_TaskFactoryImpl extends EFactoryImpl implements TAM_TaskFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TAM_TaskFactory init() {
		try {
			TAM_TaskFactory theTAM_TaskFactory = (TAM_TaskFactory)EPackage.Registry.INSTANCE.getEFactory(TAM_TaskPackage.eNS_URI);
			if (theTAM_TaskFactory != null) {
				return theTAM_TaskFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TAM_TaskFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_TaskFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TAM_TaskPackage.TAM_MEM_REQ: return createTamMemReq();
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION: return createTamInterfaceImplementation();
			case TAM_TaskPackage.TAM_PERIPHERY_TYPE: return createTamPeripheryType();
			case TAM_TaskPackage.TAM_SHARED_OS_RESOURC_TYPE: return createTamSharedOSResourcType();
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS: return createTamResourceAccess();
			case TAM_TaskPackage.TAM_OPERATION: return createTamOperation();
			case TAM_TaskPackage.TAM_MEM_TYPE: return createTamMemType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamMemReq createTamMemReq() {
		TamMemReqImpl tamMemReq = new TamMemReqImpl();
		return tamMemReq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamInterfaceImplementation createTamInterfaceImplementation() {
		TamInterfaceImplementationImpl tamInterfaceImplementation = new TamInterfaceImplementationImpl();
		return tamInterfaceImplementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamPeripheryType createTamPeripheryType() {
		TamPeripheryTypeImpl tamPeripheryType = new TamPeripheryTypeImpl();
		return tamPeripheryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamSharedOSResourcType createTamSharedOSResourcType() {
		TamSharedOSResourcTypeImpl tamSharedOSResourcType = new TamSharedOSResourcTypeImpl();
		return tamSharedOSResourcType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamResourceAccess createTamResourceAccess() {
		TamResourceAccessImpl tamResourceAccess = new TamResourceAccessImpl();
		return tamResourceAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamOperation createTamOperation() {
		TamOperationImpl tamOperation = new TamOperationImpl();
		return tamOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamMemType createTamMemType() {
		TamMemTypeImpl tamMemType = new TamMemTypeImpl();
		return tamMemType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_TaskPackage getTAM_TaskPackage() {
		return (TAM_TaskPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TAM_TaskPackage getPackage() {
		return TAM_TaskPackage.eINSTANCE;
	}

} //TAM_TaskFactoryImpl
