/**
 */
package TAM.TAM_Platform.TAM_ControlUnit.impl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;
import TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaExecHostImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Processing Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamProcessingUnitImpl#getNumCores <em>Num Cores</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamProcessingUnitImpl#getCoreSyncOvhd <em>Core Sync Ovhd</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamProcessingUnitImpl extends GaExecHostImpl implements TamProcessingUnit {
	/**
	 * The default value of the '{@link #getNumCores() <em>Num Cores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumCores()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_CORES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumCores() <em>Num Cores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumCores()
	 * @generated
	 * @ordered
	 */
	protected int numCores = NUM_CORES_EDEFAULT;

	/**
	 * The default value of the '{@link #getCoreSyncOvhd() <em>Core Sync Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoreSyncOvhd()
	 * @generated
	 * @ordered
	 */
	protected static final String CORE_SYNC_OVHD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCoreSyncOvhd() <em>Core Sync Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoreSyncOvhd()
	 * @generated
	 * @ordered
	 */
	protected String coreSyncOvhd = CORE_SYNC_OVHD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamProcessingUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_ControlUnitPackage.Literals.TAM_PROCESSING_UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumCores() {
		return numCores;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumCores(int newNumCores) {
		int oldNumCores = numCores;
		numCores = newNumCores;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__NUM_CORES, oldNumCores, numCores));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCoreSyncOvhd() {
		return coreSyncOvhd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoreSyncOvhd(String newCoreSyncOvhd) {
		String oldCoreSyncOvhd = coreSyncOvhd;
		coreSyncOvhd = newCoreSyncOvhd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__CORE_SYNC_OVHD, oldCoreSyncOvhd, coreSyncOvhd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__NUM_CORES:
				return getNumCores();
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__CORE_SYNC_OVHD:
				return getCoreSyncOvhd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__NUM_CORES:
				setNumCores((Integer)newValue);
				return;
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__CORE_SYNC_OVHD:
				setCoreSyncOvhd((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__NUM_CORES:
				setNumCores(NUM_CORES_EDEFAULT);
				return;
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__CORE_SYNC_OVHD:
				setCoreSyncOvhd(CORE_SYNC_OVHD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__NUM_CORES:
				return numCores != NUM_CORES_EDEFAULT;
			case TAM_ControlUnitPackage.TAM_PROCESSING_UNIT__CORE_SYNC_OVHD:
				return CORE_SYNC_OVHD_EDEFAULT == null ? coreSyncOvhd != null : !CORE_SYNC_OVHD_EDEFAULT.equals(coreSyncOvhd);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (numCores: ");
		result.append(numCores);
		result.append(", coreSyncOvhd: ");
		result.append(coreSyncOvhd);
		result.append(')');
		return result.toString();
	}

} //TamProcessingUnitImpl
