import 'http://www.eclipse.org/emf/2002/Ecore' 
import _'http://www.eclipse.org/uml2/5.0.0/UML'
--_'platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore'
 
ECLimport "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib"
ECLimport "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib"
ECLimport "platform:/resource/de.fraunhofer.iem.swt.msd.tam.mocc/mocc/MSDLanguageComplete.moccml"
ECLimport "platform:/resource/de.fraunhofer.iem.swt.msd.tam.mocc/mocc/MSDFacilities.moccml" 


package UML 
 
	context Model
		def if(self.getAppliedStereotype('Modal::MSDSpecification') <> null) : globalTime: Event = self
 
	context Class
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgCreateEvent	: Event =self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgSendEvent	: Event =self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgReceiveEvent	: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : msgConsumeEvent	: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : taskStartEvent	: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : taskCompleteEvent: Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null) : oneEvent		:Event = self
		def if(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null 
		    and self.getValue(self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'lowerPrio').oclAsType(Collection(Class))->size() > 0
			) : notReady: Event = self

	context NamedElement
		def if((self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamSharedOSResource') <>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamPeripheryUnit') <>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamSensor')<>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamActuator')<>null)
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamIO')<>null)
			): acquireRes: Event = self


		def if(((self.getAppliedStereotype('TAM::TAM_Platform::TAM_Communication::TamComConnection') <>null) 
			or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamOSComChannel') <> null)) and  
					(Abstraction.allInstances()->exists(a | a.getAppliedStereotype('SysML::Allocations::Allocate') <> null and a.supplier->includes(self)))
			) : acquireCon: Event = self 

		def if((self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit') <>null) and  
					((Class).allInstances()->select(us |(us).getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null and (us).getValue((us).getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'TamPU').oclAsType(NamedElement) = self))->size() > 0) : dispatch: Event = self
		def if((self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit') <>null) and  
					(Class.allInstances()->select(us |us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null and us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'TamPU').oclAsType(NamedElement) = self))->size() > 0) 
			: numCores: Integer = (self.getValue(self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit'), 'numCores').oclAsType(Integer)) 

	context Message
		def: msgCreateUnification	:Event = self
		def: msgSendUnification		:Event = self
		def: msgReceiveUnification	:Event = self
		def: msgConsumeUnification	:Event = self
		def: taskStartUnification	:Event = self 
		def: taskCompleteUnification:Event = self 
		
	context Model
		inv globalClockTicks:
			let allMessageEvents :Event = Expression Union(
								Class.allInstances()->select(us |us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null).oclAsType(Class).oneEvent)
								in
			Relation SubClock(allMessageEvents, self.globalTime)


 
/**
   SystemMessage BEHAVIORAL INVARIANTS
**/ 

	context Class
		def : systemMessageStereotype : Stereotype = self.getAppliedStereotype('TAM_SimulationProfile::SystemMessage')
		inv defineOneEvent:
		(
			systemMessageStereotype <> null
		) implies (
			let unionEvts0:Event = Expression Union(self.msgCreateEvent, self.msgSendEvent) in
			let unionEvts1:Event = Expression Union(unionEvts0, self.msgReceiveEvent) in
			let unionEvts2:Event = Expression Union(unionEvts1, self.msgConsumeEvent) in
			let unionEvts3:Event = Expression Union(unionEvts2, self.taskStartEvent) in
			let unionEvts4:Event = Expression Union(unionEvts3, self.taskCompleteEvent) in
			Relation Coincides(self.oneEvent, unionEvts4)
		)
-------
		inv noUnnecessaryCreate: 
		( systemMessageStereotype <> null )
		implies
		(
			let oneRelCreateClock : Event = Expression Union(self.getValue(systemMessageStereotype, 'messages').oclAsType(Message).msgCreateUnification) in
			Relation SubClock(self.msgCreateEvent, oneRelCreateClock)	
		) 	
-------			
		inv noUnnecessarySend:
		( systemMessageStereotype <> null )
		implies
		(
			let oneRelSendClock : Event = Expression Union(self.getValue(systemMessageStereotype, 'messages').oclAsType(Message).msgSendUnification) in
			Relation SubClock(self.msgSendEvent, oneRelSendClock)	
		) 	
-------			
		inv noUnnecessaryReceive:
		( systemMessageStereotype <> null )
		implies
		(				
			let oneRelReceiveClock : Event =  Expression Union((self.getValue(systemMessageStereotype, 'messages').oclAsType(Message)).msgReceiveUnification) in
			Relation SubClock(self.msgReceiveEvent, oneRelReceiveClock)	
		) 	
-------		
		inv noUnnecessaryConsume:
		( systemMessageStereotype <> null )
		implies 
		(				
			let oneRelConsumeClock : Event = Expression  Union((self.getValue(systemMessageStereotype, 'messages').oclAsType(Message)).msgConsumeUnification) in
			Relation SubClock(self.msgConsumeEvent, oneRelConsumeClock)	
		) 	
-------			
		inv noUnnecessaryTaskStart:
		( systemMessageStereotype <> null )
		implies
		(				
			let oneRelTaskStartClock : Event = Expression  Union((self.getValue(systemMessageStereotype, 'messages').oclAsType(Message)).taskStartUnification) in
			Relation SubClock(self.taskStartEvent, oneRelTaskStartClock)	
		) 	
-------			
		inv noUnnecessaryTaskComplete:
		( systemMessageStereotype <> null )
		implies
		(
			let oneRelTaskCompleteClock : Event =  Expression Union((self.getValue(systemMessageStereotype, 'messages').oclAsType(Message)).taskCompleteUnification) in
			Relation SubClock(self.taskCompleteEvent, oneRelTaskCompleteClock)	
		) 
-------			 
		inv minEnd2EndDelay:
		(
			systemMessageStereotype <> null 
			and self.getValue(systemMessageStereotype, 'minEndToEndDelay').toString().toReal().round() > 0
		)
		implies
		(
			let minEnd2EndDelay:Integer = self.getValue(systemMessageStereotype, 'minEndToEndDelay').toString().toReal().round()in
			let createDelayedByMin_e2e : Event = Expression DelayFor(self.msgCreateEvent,self.getModel().globalTime,minEnd2EndDelay	) in
			Relation NonStrictAlternatesFSM(createDelayedByMin_e2e , self.taskCompleteEvent)
		)
-------				
		inv maxEnd2EndDelay:
		(
			systemMessageStereotype <> null and
			self.getValue(systemMessageStereotype, 'maxEndToEndDelay').toString().toReal().round() > 0
		)
		implies
		(
			let maxEnd2EndDelay:Integer = self.getValue(systemMessageStereotype, 'maxEndToEndDelay').toString().toReal().round() in
			let createDelayedByMax_e2e : Event = Expression DelayFor(self.msgCreateEvent, self.getModel().globalTime, maxEnd2EndDelay) in
			Relation NonStrictAlternatesFSM(self.taskCompleteEvent, createDelayedByMax_e2e)
		)	
-------
		inv minCreateDelay: 
		(
			systemMessageStereotype <> null 
			and self.getValue(systemMessageStereotype, 'minCreateDelay').toString().toReal().round() > 0
		)
		implies
		(
			let minCDelay:Integer = self.getValue(systemMessageStereotype, 'minCreateDelay').toString().toReal().round()in
			let createDelayedByMin : Event = Expression DelayFor(self.msgCreateEvent, self.getModel().globalTime, minCDelay) in
			Relation NonStrictAlternatesFSM(createDelayedByMin , self.msgSendEvent)
		)
-------				
		inv maxCreateDelay:
		(
			systemMessageStereotype <> null and
			self.getValue(systemMessageStereotype, 'maxCreateDelay').toString().toReal().round() > 0
		)
		implies
		(
			let maxCDelay:Integer= self.getValue(systemMessageStereotype, 'maxCreateDelay').toString().toReal().round() in
			let createDelayedByMax : Event = Expression DelayFor(self.msgCreateEvent, self.getModel().globalTime,maxCDelay) in
			Relation NonStrictAlternatesFSM(self.msgSendEvent, createDelayedByMax)
		)
-------				
		inv minSendDelay: 
		(
			systemMessageStereotype <> null 
			and self.getValue(systemMessageStereotype, 'minSendDelay').toString().toReal().round() > 0
		)
		implies
		(
			let minSDelay:Integer = self.getValue(systemMessageStereotype, 'minSendDelay').toString().toReal().round()in
			let sendDelayedByMin : Event = Expression DelayFor(self.msgSendEvent, self.getModel().globalTime, minSDelay) in
			Relation NonStrictAlternatesFSM(sendDelayedByMin , self.msgReceiveEvent)
		)
-------				
		inv maxSendDelay:
		(
			systemMessageStereotype <> null and
			self.getValue(systemMessageStereotype, 'maxSendDelay').toString().toReal().round() > 0
		)
		implies
		(
			let maxSDelay:Integer= self.getValue(systemMessageStereotype, 'maxSendDelay').toString().toReal().round() in
			let sendDelayedByMax : Event = Expression DelayFor(self.msgSendEvent, self.getModel().globalTime,maxSDelay) in
			Relation NonStrictAlternatesFSM(self.msgReceiveEvent, sendDelayedByMax)
		)
-------
		inv minReceiveDelay:
		(
			systemMessageStereotype <> null and
			self.getValue(systemMessageStereotype, 'minReceiveDelay').toString().toReal().round() > 0
		)
		implies
		(
			let minRDelay:Integer = self.getValue(systemMessageStereotype, 'minReceiveDelay').toString().toReal().round() in
			let receiveDelayedByMin : Event = Expression DelayFor(self.msgReceiveEvent, self.getModel().globalTime,minRDelay) in
			Relation NonStrictAlternatesFSM(receiveDelayedByMin , self.msgConsumeEvent)
		)
-------
		inv maxReceiveDelay:
		(
			systemMessageStereotype <> null and
			self.getValue(systemMessageStereotype, 'maxReceiveDelay').toString().toReal().round() > 0
		)
		implies
		(
			let maxRDelay:Integer = self.getValue(systemMessageStereotype, 'maxReceiveDelay').toString().toReal().round() in
			let receiveDelayedByMax : Event = Expression DelayFor(self.msgReceiveEvent, self.getModel().globalTime, maxRDelay) in
			Relation NonStrictAlternatesFSM(self.msgConsumeEvent, receiveDelayedByMax)
		)
-------	
		inv minExecutionDelay:
		(
			systemMessageStereotype <> null and
			self.getValue(systemMessageStereotype, 'minExecutionDelay').toString().toReal().round() > 0
		)
		implies
		(
			let minTDelay:Integer = self.getValue(systemMessageStereotype, 'minExecutionDelay').toString().toReal().round() in
			let taskStartDelayedByMin : Event = Expression DelayFor(self.taskStartEvent, self.getModel().globalTime, minTDelay) in
			Relation NonStrictAlternatesFSM(taskStartDelayedByMin , self.taskCompleteEvent)
		)
-------				
		inv maxExecDelay:
		(
			systemMessageStereotype <> null and
			self.getValue(systemMessageStereotype, 'maxExecutionDelay').toString().toReal().round() > 0
		)
		implies
		(
			let maxTDelay:Integer = self.getValue(systemMessageStereotype, 'maxExecutionDelay').toString().toReal().round() in
			let taskStartDelayedByMax : Event = Expression DelayFor(self.taskStartEvent, self.getModel().globalTime, maxTDelay) in
			Relation NonStrictAlternatesFSM(self.taskCompleteEvent, taskStartDelayedByMax)
		)
-------	
--TODO: check if tamCon.acquireCon could be the union of all msgSent on it instead
		inv lockComChannelWhenSending:
			let con:Connector = self.getValue(systemMessageStereotype, 'connector').oclAsType(Connector) in
			let tamCon:NamedElement = Abstraction.allInstances()->select(a | a.getAppliedStereotype('SysML::Allocations::Allocate') <> null and a.client->includes(con)).supplier->selectOne(true) in
			(
				systemMessageStereotype <> null and
				not (tamCon.oclIsInvalid() or tamCon.oclIsUndefined())
			)
			implies (
				Relation SubClock(self.msgSendEvent, tamCon.acquireCon)
			)
-------			
--TODO: check if acquireAllResources could be the union of all msgSent on it instead
--		also instead of Intersection, a Sup seams more appropriate
		inv acquireResourcesWhenStart:
			let resources :Collection(NamedElement) = self.getValue(systemMessageStereotype, 'requiredResources').oclAsType(Collection(NamedElement)) in
			(
				systemMessageStereotype <> null and
				resources->size() > 0
			)
			implies (
				let acquireAllResources: Event = Expression Intersection(resources.acquireRes) in
				Relation SubClock(self.taskStartEvent, acquireAllResources)
			)
-------			
--TODO: check if pu.dispatch could be the union of all taskStart on it instead	
		inv claimProcessingUnitWhenStart:
			let pu :NamedElement = self.getValue(systemMessageStereotype, 'TamPU').oclAsType(NamedElement) in
			(
				systemMessageStereotype <> null and
				not (pu.oclIsInvalid() or pu.oclIsUndefined())
			)
			implies (
				Relation SubClock(self.taskStartEvent, pu.dispatch)
			)
-------	
--TODO: check this notReady even, which is strange... it may be the union of all lowerPrio... and is forbidden in the next inv (syncWithLowerPrioUS)	
--		also it seems redondant with the union of lower in the next invariant. 
--TO BE REMOVED ? 
		inv NoInterferenceWithHigherPrio:
			let higherPrioUs: Collection(Class)  = self.getValue(systemMessageStereotype, 'higherPrio').oclAsType(Collection(Class)) in
			(
				systemMessageStereotype <> null and
				higherPrioUs->size() > 0
			) implies(
				let noHigherPrioTasksReady:Event = Expression Intersection(higherPrioUs.notReady) in
				Relation SubClock(self.taskStartEvent, noHigherPrioTasksReady)
			)
-------
		inv syncWithLowerPrioUS:
			let lowerPrioUS:Collection(Class) = self.getValue(systemMessageStereotype, 'lowerPrio').oclAsType(Collection(Class)) in
			(
				systemMessageStereotype <> null and
				lowerPrioUS->size() > 0
			) implies(
				let oneLowerPrioTask:Event = Expression Union(lowerPrioUS.taskStartEvent) in
				
				Relation fixedPrioritySchedTask(self.notReady, oneLowerPrioTask, self.msgConsumeEvent, self.taskStartEvent)
			)



/**
   NamedElement BEHAVIORAL INVARIANTS
**/

	context NamedElement
		def: allUS:Set(Class) = Class.allInstances()->select(us |us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null)
-------		
		inv os_resource_lock_unlock:			
			let relevantUS = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'exclusiveResource').oclAsType(Collection(Element))->includes(self)) in
			(
				(self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamSharedOSResource') <>null)
				or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamPeripheryUnit')<>null)
				or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamSensor')<>null)
				or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamActuator')<>null)
				or (self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamIO')<>null)
			) and
				(relevantUS->size() >0 )
			implies(
				let startTaskEvts:Event = Expression Union(relevantUS.taskStartEvent) in
				let taskCompleteEvts:Event = Expression Union(relevantUS.taskCompleteEvent) in
				Relation resourceAccess(self.acquireRes, startTaskEvts, taskCompleteEvts)
			)
-------			
		inv os_exclusiveResourceAccess:
			let relevantUS = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'requiredResources').oclAsType(Collection(Element))->includes(self)) in
			(
				(
					self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamSharedOSResource') <>null
				or 	self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamPeripheryUnit') <>null
				or 	self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamSensor')<>null
				or 	self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamActuator')<>null
				or 	self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamIO')<>null
				)
				and	relevantUS->size() >0
			)
			implies(
				Relation Exclusion(relevantUS.taskStartEvent)	
			)
-------
		inv connection_lock_unlock:
			let relevantAllocations:Set(Abstraction) = Abstraction.allInstances()->select(a | a.getAppliedStereotype('SysML::Allocations::Allocate') <> null and a.supplier->includes(self)) in
			let relevantUS:Set(Class) = allUS->select(us | relevantAllocations->exists(a | a.client->includes(us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'connector').oclAsType(Connector)))) in
			(
				(
						self.getAppliedStereotype('TAM::TAM_Platform::TAM_Communication::TamComConnection') <> null
					or	self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamOSComChannel') <> null
				)
				and	relevantUS->size() >0
			)
			implies(
				let lu_sendEvts:Event = Expression Union(relevantUS.msgSendEvent) in
				let receiveEvts:Event = Expression Union(relevantUS.msgReceiveEvent) in
				Relation resourceAccess(self.acquireCon, lu_sendEvts, receiveEvts)
			)
-------
		inv exclusiveAccessToComChannel:
			let relevantAllocations:Set(Abstraction) = Abstraction.allInstances()->select(a | a.getAppliedStereotype('SysML::Allocations::Allocate') <> null and a.supplier->includes(self)) in
			let relevantUS:Set(Class) = allUS->select(us | relevantAllocations->exists(a | a.client->includes(us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'connector').oclAsType(Connector)))) in
			(
				(
						self.getAppliedStereotype('TAM::TAM_Platform::TAM_Communication::TamComConnection') <> null 
					or 	self.getAppliedStereotype('TAM::TAM_Platform::TAM_OperatingSystem::TamOSComChannel') <> null
				)
				and	relevantUS->size() >0
			)
			implies(
					Relation Exclusion(relevantUS.msgSendEvent)
			)
-------	
		inv occupyCoreWhenTaskStart:
			let relevantUS:Set(Class) = Class.allInstances()->select(us |us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null and us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'TamPU').oclAsType(NamedElement) = self) in
			(
					self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit') <>null
				and relevantUS->size() >0
			)
			implies(
				let relTaskStarts:Event = Expression Union(relevantUS.taskStartEvent) in
				let relTaskComplete:Event = Expression Union(relevantUS.taskCompleteEvent) in
				Relation fixedPrioritySchedCPU(self.dispatch, relTaskStarts, relTaskComplete, numCores)
			)
-------			
		inv onlyOneTaskDispatchAt:
			let relevantUS:Set(Class) = Class.allInstances()->select(us |us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null and us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'TamPU').oclAsType(NamedElement) = self) in
			(
					self.getAppliedStereotype('TAM::TAM_Platform::TAM_ControlUnit::TamProcessingUnit') <>null
				and relevantUS->size() >0
			)
			implies(
				Relation Exclusion(relevantUS.taskStartEvent)	
			)
 
 
 
/**
   Message BEHAVIORAL INVARIANTS
**/

	context Message 
		def: allUS:Set(Class) = Class.allInstances()->select(us |us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null)
		def : syncPoint : Class = Class.allInstances()->selectOne(s | s.getAppliedStereotype('TAM_SimulationProfile::SystemMessage') <> null 
																	and s.getValue(s.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->includes(self)
																)
		def : otherMsgs:Set(Message) = self.owner.oclAsType(Interaction).message->reject(m | m = self)

		inv UnificateWithSameEndPoints:
			let msgsInAssumptionUE : Collection(Message) = Message.allInstances()->select(m:Message | m.signature = self.signature 
																								and m.interaction.getAppliedStereotype('TAM::TAM_AnalysisContext::TamRequirementMSD')<> null
																								and m._'sendEvent'.oclAsType(MessageOccurrenceSpecification).covered.represents = self._'sendEvent'.oclAsType(MessageOccurrenceSpecification).covered.represents
																								and m._'receiveEvent'.oclAsType(MessageOccurrenceSpecification).covered.represents = self._'receiveEvent'.oclAsType(MessageOccurrenceSpecification).covered.represents) 
														in
			(
				self.interaction.getAppliedStereotype('TAM::TAM_AnalysisContext::TamRequirementMSD') <> null
				and	msgsInAssumptionUE->size() >0
			) implies(
				let oneAssumptionMsgCreateUE : Event = Expression Union(msgsInAssumptionUE.msgCreateUnification) in
				Relation SubClock(self.msgCreateUnification, oneAssumptionMsgCreateUE)
			)
-------
		inv UnificateNotWithFirstReqMsg:
			let msgsInAssumptionUF : Collection(Message) = Message.allInstances()->select(m:Message | m.signature = self.signature 
																								and m.interaction.getAppliedStereotype('TAM::TAM_AnalysisContext::TamRequirementMSD')<> null 
																								and m.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)->indexOf(m._'sendEvent'.oclAsType(MessageOccurrenceSpecification))>1)
														in
			(
				self.interaction.getAppliedStereotype('TAM::TAM_AnalysisContext::TamRequirementMSD') <> null
				and	msgsInAssumptionUF->size() >0
			) implies(
				let oneAssumptionMsgCreateUF : Event = Expression Union(msgsInAssumptionUF.msgCreateUnification) in
				Relation SubClock(self.msgCreateUnification, oneAssumptionMsgCreateUF)
			)
-------
		inv activateFromEnviromentAssumption:
			let msgsInAssumption2 : Collection(Message) = (Message).allInstances()->select(m:Message | (m).signature = self.signature 
																									and (m).interaction.getAppliedStereotype('Modal::EnvironmentAssumption')<> null) 
															in
			(
				self.interaction.getAppliedStereotype('TAM::TAM_AnalysisContext::TamRequirementMSD') <> null
				and	msgsInAssumption2->size() >0
			) implies(
				let oneAssumptionMsgCreate2 : Event = Expression Union(msgsInAssumption2.msgCreateUnification) in
				Relation Coincides(self.msgCreateUnification, oneAssumptionMsgCreate2)
			)
-------		
		inv messageStatusHot:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Hot')
				or	otherMsgs->size() = 0
			) implies (
				Relation messageSatusHot(msgCreateUnification, msgSendUnification, msgReceiveUnification, msgConsumeUnification, taskStartUnification, taskCompleteUnification)
			)
-------	
		inv messageStatusCold:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Cold') 
				and	otherMsgs->size() > 0
			) implies (
				let relevantUS:Set(Class) = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->exists(m | otherMsgs->includes(m))) in
		    	let deactivate:Event = Expression Union(relevantUS.oneEvent) in
				Relation messageSatusCold(msgCreateUnification, msgSendUnification, msgReceiveUnification, msgConsumeUnification, taskStartUnification, taskCompleteUnification, deactivate) 
			)
		
		inv messageRelation:
			let mos_index : Integer = self.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)->indexOf(self._'sendEvent'.oclAsType(MessageOccurrenceSpecification)) in
			(
				mos_index > 1
			) implies
			(
				let precedingMessage : Message = self.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)
										->at((mos_index)-1).message in
				Relation NonStrictAlternatesFSM(precedingMessage.taskCompleteUnification, self.msgCreateUnification)
			)	
-------			
		inv createUnification_notInitialHot:
			let mos_index : Integer = self.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)->indexOf(self._'sendEvent'.oclAsType(MessageOccurrenceSpecification)) in
				(
					mos_index > 1
					and (not (syncPoint.oclIsUndefined())) 
					and	(
						self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Hot')
						or	otherMsgs->size() = 0
						)
				) 
				implies(
					let precedingMessage : Message = self.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)->at(mos_index-1).message in
					Relation messageUnificationHot(precedingMessage.taskCompleteUnification, syncPoint.msgCreateEvent, self.msgCreateUnification)
				)
-------
	 	inv createUnification_initial:
	 		(
	 			self.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)->indexOf(self._'sendEvent'.oclAsType(MessageOccurrenceSpecification)) = 1 
	 			and not syncPoint.oclIsUndefined()
	 		) 
	 		implies(
	 			Relation Coincides(syncPoint.msgCreateEvent, self.msgCreateUnification)
	 		)
-------	
	 	inv sendUnificationHot:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Hot')
				or otherMsgs->size() = 0
			) implies (
				Relation messageUnificationHot(self.msgCreateUnification, syncPoint.msgSendEvent, self.msgSendUnification)
			)
-------
	 	inv receiveUnificationHot:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Hot')
				or otherMsgs->size() = 0
			) implies (
				Relation messageUnificationHot(self.msgSendUnification, syncPoint.msgReceiveEvent, self.msgReceiveUnification)
			)
-------	 		
	 	inv consumeUnificationHot:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Hot')
				or otherMsgs->size() = 0
			) implies (
				Relation messageUnificationHot(self.msgReceiveUnification, (syncPoint).msgConsumeEvent, self.msgConsumeUnification)
			)
-------			
		inv taskStartUnificationHot:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Hot')
				or otherMsgs->size() = 0
			) implies (
				Relation messageUnificationHot(self.msgConsumeUnification, syncPoint.taskStartEvent, self.taskStartUnification)
			)
-------
		inv taskCompleteUnificationHot:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Hot')
				or otherMsgs->size() = 0
			) implies (
				Relation messageUnificationHot(self.taskStartUnification, syncPoint.taskCompleteEvent, self.taskCompleteUnification)
			)
-------		
		 inv createUnification_notInitialCold:
	 		let mos_index : Integer = self.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)->indexOf(self._'sendEvent'.oclAsType(MessageOccurrenceSpecification)) in
	 		(
	 			mos_index > 1 
				and not ((syncPoint).oclIsUndefined()) 
				and self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Cold') 
				and otherMsgs->size() > 0
	 		) 
	 		implies(
				let relevantUS:Set(Class) = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->exists(m | otherMsgs->includes(m))) in
				let deactivate_ui:Event = Expression Union(relevantUS.oneEvent) in
		 		let precedingMessage : Message = self.owner.oclAsType(Interaction).fragment->asSequence()->selectByType(MessageOccurrenceSpecification)->at((mos_index)-1).message in
		 		Relation messageUnificationCold(precedingMessage.taskCompleteUnification, syncPoint.msgCreateEvent, self.msgCreateUnification, deactivate_ui)
	 		)
-------	 		
	 	inv sendUnificationCold:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Cold')
				and otherMsgs->size() > 0
			) implies (
				let relevantUS:Set(Class) = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->exists(m | otherMsgs->includes(m))) in
				let deactivate0:Event = Expression Union(relevantUS.oneEvent) in
				Relation messageUnificationCold(self.msgCreateUnification, syncPoint.msgSendEvent, self.msgSendUnification, deactivate0)
			)
-------
		inv receiveUnificationCold:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Cold') 
				and	otherMsgs->size() > 0
			) implies (
				let relevantUS:Set(Class) = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->exists(m | otherMsgs->includes(m))) in
				let deactivate2:Event = Expression Union(relevantUS.oneEvent) in
				Relation messageUnificationCold(self.msgSendUnification, syncPoint.msgReceiveEvent, self.msgReceiveUnification, deactivate2)
			)
-------	 		
	 	inv consumeUnificationCold:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Cold')
				and	otherMsgs->size() > 0
			) implies (
				let relevantUS:Set(Class) = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->exists(m | otherMsgs->includes(m))) in
				let deactivate3:Event = Expression Union(relevantUS.oneEvent) in
				Relation messageUnificationCold(self.msgReceiveUnification, syncPoint.msgConsumeEvent, self.msgConsumeUnification, deactivate3)
			)
-------
		inv taskStartUnificationCold:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Cold')
				and otherMsgs->size() > 0
			) implies (
				let relevantUS:Set(Class) = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->exists(m | otherMsgs->includes(m))) in
				let deactivate4:Event = Expression Union(relevantUS.oneEvent) in
				Relation messageUnificationCold(self.msgConsumeUnification, syncPoint.taskStartEvent, self.taskStartUnification, deactivate4)
			)
-------		 
		inv taskCompleteUnificationCold:
			(
				self.getValue(self.getAppliedStereotype('Modal::ModalMessage'), 'temperature').oclAsType(NamedElement).name.startsWith('Cold')
				and otherMsgs->size() > 0
			) implies (
				let relevantUS:Set(Class) = allUS->select(us |us.getValue(us.getAppliedStereotype('TAM_SimulationProfile::SystemMessage'), 'messages').oclAsType(Collection(Message))->exists(m | otherMsgs->includes(m))) in
				let deactivate5:Event = Expression Union(relevantUS.oneEvent) in
				Relation messageUnificationCold(self.taskStartUnification, syncPoint.taskCompleteEvent, self.taskCompleteUnification, deactivate5)
			)
 
/**
   CombinedFragment BEHAVIORAL INVARIANTS
**/


	context CombinedFragment
		def : expression : String = self.operand.guard.specification.stringValue()->first()
		def : clockName : String = expression.substring(1, expression.indexOf('=')-1).trim()
		def : ownIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(self)
		def : followingCombinedFragment : Collection(CombinedFragment) = self.owner.oclAsType(Interaction).fragment->selectByType(CombinedFragment)->select(cb | (cb.getAppliedStereotype('Modal::TimeCondition') <> null) 
																																	and self.owner.oclAsType(Interaction).fragment->indexOf(cb) > ownIndex)
		def : messageReceivesBeforeReset : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | mos.message._'receiveEvent' = mos				
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < ownIndex)->asSequence().message
		inv endToEndDelay_upperNotInitial:
			let constraint : CombinedFragment = followingCombinedFragment->select (cb | cb.operand.guard.specification.stringValue()->first().trim().startsWith(clockName.concat(' <='))	
																					 or cb.operand.guard.specification.stringValue()->first().trim().startsWith(clockName.concat('<='))
																			)->asSequence()->at(1) in
			let constrainedExpression : String = constraint.operand.guard.specification.stringValue()->first() in
				(
					self.getAppliedStereotype('Modal::ClockReset') <> null 
					and (
						constrainedExpression.startsWith(clockName.concat('<='))
						or constrainedExpression.startsWith(clockName.concat(' <='))
						)
					and	ownIndex > 1
				)
				implies
				(
					let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
					let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | mos.message._'receiveEvent' = mos
																																							and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																							and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message
																		in
					let constrainedTaskCompletions_upperE : Event = Expression Sup(messageReceivesBeforeConstraint.taskCompleteUnification) in
					let activationTMsgReceives_upperE : Event = Expression Sup(messageReceivesBeforeReset.msgReceiveUnification) in
					let maxDelay_e2e_upperInitial: Integer = constrainedExpression.substringAfter('<=').trim().toInteger() in
					let stimulusDelayedExpression_e2e_sE : Event = Expression DelayFor(activationTMsgReceives_upperE, self.getModel().globalTime, maxDelay_e2e_upperInitial) in
					Relation NonStrictAlternatesFSM(constrainedTaskCompletions_upperE,stimulusDelayedExpression_e2e_sE)
				)
-------
			inv endToEndDelay_lowerNotInitial:
				let constraint : CombinedFragment = followingCombinedFragment->select (cb | cb.operand.guard.specification.stringValue()->first().trim().startsWith((clockName)))->asSequence()->at(1) in
				let constrainedExpression : String = (constraint).operand.guard.specification.stringValue()->first() in
				(
					self.getAppliedStereotype('Modal::ClockReset') <> null 
					and (
						constrainedExpression.startsWith(clockName.concat('>=')) or constrainedExpression.startsWith(clockName.concat(' >='))
						) 
					and ownIndex > 1
				)
				implies
				(
					let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
					let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | mos.message._'receiveEvent' = mos
																																							and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																							and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message in
					let constrainedTaskCompletions_lowerNotInitial : Event = Expression Sup((messageReceivesBeforeConstraint).taskCompleteUnification) in
					let activationMsgReceives_lowerNotInitial : Event = Expression Sup((messageReceivesBeforeReset).msgReceiveUnification) in
					let maxDelay_e2e_lowerNotInitial: Integer = (constrainedExpression).substringAfter('>=').trim().toInteger() in
					let stimulusDelayedExpression_e2e_lowerNotInitial : Event = Expression DelayFor(activationMsgReceives_lowerNotInitial, self.getModel().globalTime, maxDelay_e2e_lowerNotInitial) in
					Relation NonStrictAlternatesFSM(stimulusDelayedExpression_e2e_lowerNotInitial, constrainedTaskCompletions_lowerNotInitial)
				)
-------			
			inv endToEndDelay_upperNotInitial_s:
				let constraint : CombinedFragment = followingCombinedFragment->select (cb | cb.operand.guard.specification.stringValue()->first().trim().startsWith((clockName).concat(' <'))	
																						or  cb.operand.guard.specification.stringValue()->first().trim().startsWith((clockName).concat('<'))
																				)->asSequence()->at(1) in
				let constrainedExpression : String = constraint.operand.guard.specification.stringValue()->first() in
				(
					self.getAppliedStereotype('Modal::ClockReset') <> null 
					and (
						constrainedExpression.startsWith(clockName.concat('<'))
						or constrainedExpression.startsWith(clockName.concat(' <'))
						)
					and ownIndex > 1
					and constrainedExpression.indexOf('=') <= 0
				)
				implies
				(
					let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
					let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | mos.message._'receiveEvent' = mos
																																							and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																							and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message in
					let constrainedTaskCompletions_upperE_s : Event = Expression Sup(messageReceivesBeforeConstraint.taskCompleteUnification) in
					let activationmsgReceive_upperE_s : Event = Expression Sup(messageReceivesBeforeReset.msgReceiveUnification) in
					let maxDelay_e2e_upperNotInitial_s: Integer = constrainedExpression.substringAfter('<').trim().toInteger() in
					let stimulusDelayedExpression_e2e_sE_s : Event = Expression DelayFor(activationmsgReceive_upperE_s, self.getModel().globalTime, maxDelay_e2e_upperNotInitial_s) in
					Relation NonStrictAlternatesFSM(constrainedTaskCompletions_upperE_s,stimulusDelayedExpression_e2e_sE_s)
				)
-------
			inv endToEndDelay_lowerNotInitial_s:
				let constraint : CombinedFragment = followingCombinedFragment->select (cb | cb.operand.guard.specification.stringValue()->first().trim().startsWith((clockName)))->asSequence()->at(1) in
				let constrainedExpression : String = (constraint).operand.guard.specification.stringValue()->first() in
			(
				self.getAppliedStereotype('Modal::ClockReset') <> null 
				and (
					constrainedExpression.startsWith((clockName).concat('>'))
					or (constrainedExpression).startsWith((clockName).concat(' >'))
					)
				and (not((constrainedExpression).startsWith((clockName).concat('>=')) or (constrainedExpression).startsWith((clockName).concat(' >='))))
				and	ownIndex > 1
			)
			implies
			(
				let constraintIndex : Integer = self.owner.oclAsType(Interaction).fragment->indexOf(constraint) in
				let messageReceivesBeforeConstraint : Sequence(Message) = self.owner.oclAsType(Interaction).fragment->selectByType(MessageOccurrenceSpecification)->select(mos | mos.message._'receiveEvent' = mos
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) > ownIndex
																																						and self.owner.oclAsType(Interaction).fragment->indexOf(mos) < constraintIndex)->asSequence().message in
				let constrainedTaskCompletions_lowerNotInitial_s : Event = Expression Sup(messageReceivesBeforeConstraint.msgReceiveUnification) in
				let activationTaskCompletions_lowerNotInitial_s : Event = Expression Sup(messageReceivesBeforeReset.taskCompleteUnification) in
				let maxDelay_e2e_lowerNotInitial_s: Integer = constrainedExpression.substringAfter('>').trim().toInteger() in
				let stimulusDelayedExpression_e2e_lowerNotInitial_s : Event = Expression DelayFor(activationTaskCompletions_lowerNotInitial_s, self.getModel().globalTime, maxDelay_e2e_lowerNotInitial_s) in
				Relation NonStrictAlternatesFSM(stimulusDelayedExpression_e2e_lowerNotInitial_s, constrainedTaskCompletions_lowerNotInitial_s)
			)

/**
   Interaction BEHAVIORAL INVARIANTS
**/
		

	context Interaction

		def : firstMessage : Message = self.fragment->selectByType(MessageOccurrenceSpecification)->first().message
		def : tamAssumptionStereotype : Stereotype = self.getAppliedStereotype('TAM::TAM_AnalysisContext::TamAssumptionMSD')
		def : tamAssumptionPattern : Element = self.getValue(tamAssumptionStereotype, 'tamPattern').oclAsType(Element)
-------
		inv makeItBounded:          
			let lastMessage : Message = self.fragment->selectByType(MessageOccurrenceSpecification)->last().message in          
			Relation NonStrictAlternatesFSM(firstMessage.taskStartUnification,lastMessage.taskCompleteUnification)
-------	   		
		inv periodicPattern:
			(
				tamAssumptionStereotype <> null
				and tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamPeriodicPattern') <> null
			) implies (
				let period:Integer = tamAssumptionPattern.getValue(tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamPeriodicPattern'), 'period').toString().toInteger() in
				let offset:Integer = tamAssumptionPattern.getValue(tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamPeriodicPattern'), 'offset').toString().toInteger() in
				let periodicActivation:Event = Expression PeriodicOffsetP(self.getModel().globalTime, period) in
				Relation Coincides(firstMessage.msgCreateUnification, periodicActivation)
			)
-------			
		inv sporadicMin:
			(
				tamAssumptionStereotype <> null
				and tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamSporadicPattern') <> null
				and tamAssumptionPattern.getValue(tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamSporadicPattern'), 'minMeanInterarrivalTime').toString().toInteger()> 0
			) implies (
				let period2:Integer = tamAssumptionPattern.getValue(tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamSporadicPattern'), 'minMeanInterarrivalTime').toString().toInteger() in
				let periodicActivation_sMin:Event = Expression PeriodicOffsetP(self.getModel().globalTime, period2) in
				Relation NonStrictAlternatesFSM(periodicActivation_sMin, firstMessage.msgCreateUnification)
			)
-------			
		inv sporadicMax:
			(
				tamAssumptionStereotype <> null
				and tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamSporadicPattern') <> null
				and tamAssumptionPattern.getValue(tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamSporadicPattern'), 'maxMeanInterarrivalTime').toString().toInteger()> 0
			) implies (
				let period3:Integer = tamAssumptionPattern.getValue(tamAssumptionPattern.getAppliedStereotype('TAM::TAM_AnalysisContext::TamSporadicPattern'), 'maxMeanInterarrivalTime').toString().toInteger() in
				let periodicActivation_sMax:Event = Expression PeriodicOffsetP(self.getModel().globalTime, period3) in
				Relation NonStrictAlternatesFSM(firstMessage.msgCreateUnification, periodicActivation_sMax)
			)
			

endpackage