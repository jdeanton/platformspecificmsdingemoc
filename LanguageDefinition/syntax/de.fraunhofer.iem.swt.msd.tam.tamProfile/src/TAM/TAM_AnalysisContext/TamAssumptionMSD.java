/**
 */
package TAM.TAM_AnalysisContext;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaWorkloadEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Assumption MSD</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamAssumptionMSD()
 * @model
 * @generated
 */
public interface TamAssumptionMSD extends GaWorkloadEvent {
} // TamAssumptionMSD
