/**
 */
package TAM.TAM_AnalysisContext;

import TAM.TAM_Platform.TAM_Communication.TamComConnection;

import TAM.TAM_Platform.TAM_ControlUnit.TamECU;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaResourcesPlatform;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Resource Platform</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_AnalysisContext.TamResourcePlatform#getConnections <em>Connections</em>}</li>
 *   <li>{@link TAM.TAM_AnalysisContext.TamResourcePlatform#getECUs <em>EC Us</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamResourcePlatform()
 * @model
 * @generated
 */
public interface TamResourcePlatform extends GaResourcesPlatform {
	/**
	 * Returns the value of the '<em><b>Connections</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_Communication.TamComConnection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connections</em>' reference list.
	 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamResourcePlatform_Connections()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamComConnection> getConnections();

	/**
	 * Returns the value of the '<em><b>EC Us</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_ControlUnit.TamECU}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Us</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EC Us</em>' reference list.
	 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamResourcePlatform_ECUs()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamECU> getECUs();

} // TamResourcePlatform
