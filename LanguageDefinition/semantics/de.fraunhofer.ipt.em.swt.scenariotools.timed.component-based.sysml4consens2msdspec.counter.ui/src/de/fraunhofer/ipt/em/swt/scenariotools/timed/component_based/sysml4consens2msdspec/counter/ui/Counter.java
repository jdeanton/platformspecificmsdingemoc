package de.fraunhofer.ipt.em.swt.scenariotools.timed.component_based.sysml4consens2msdspec.counter.ui;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;




public class Counter {
	
	public static void main(String[] args){
		
		ResourceSet rs = new ResourceSetImpl();
		 rs.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		 Map extensionToFactoryMap = rs.getResourceFactoryRegistry()
		 .getExtensionToFactoryMap();
		 extensionToFactoryMap.put(UMLResource.FILE_EXTENSION,
		 UMLResource.Factory.INSTANCE);
		 extensionToFactoryMap.put("ecore", new EcoreResourceFactoryImpl());
		 extensionToFactoryMap.put(Resource.Factory.Registry.DEFAULT_EXTENSION,
		 new XMIResourceFactoryImpl());

		
		//Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		//Map<String, Object> m = reg.getExtensionToFactoryMap();
		//m.put("uml", new XMIResourceFactoryImpl());
	
		 //System.out.println("model name?");
		 //String modelName = System.out
		 

		  
		 URI uri = URI.createURI(args[0]);
		 
		 Resource inResource = rs.createResource(uri);
		 try {
			inResource.load(extensionToFactoryMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 URI transformationURI = null;
		 if(args.length<=2 || args[2].equals("de.fraunhofer.ipt.em.swt.scenariotools.timed.component-based.sysml4consens2msdspec.counter.ui.t_ek_notSet"))
			 transformationURI = URI.createURI("platform:/plugin/de.fraunhofer.ipt.em.swt.scenariotools.timed.component-based.sysml4consens2msdspec.counter.ui/transforms/CountTransformation.qvto");
		 else if(args[2].equals("de.fraunhofer.ipt.em.swt.scenariotools.timed.component-based.sysml4consens2msdspec.counter.ui.t_ek_set")) {
			 transformationURI = URI.createURI("platform:/plugin/de.fraunhofer.ipt.em.swt.scenariotools.timed.component-based.sysml4consens2msdspec.counter.ui/transforms/CountTransformation_t_ek.qvto");
		 }
		 //URI transformationURI = URI.createPlatformPluginURI(ResourcesPlugin.getWorkspace().getRoot().getLocation().toString()+ "transforms/Transformation.qvto", true);
		 TransformationExecutor executor = new TransformationExecutor(transformationURI);
		  EList<EObject> inObjects1 = inResource.getContents();
		   ModelExtent input1 = new BasicModelExtent(inObjects1);	
		   ModelExtent output = new BasicModelExtent();
		   ExecutionContextImpl context = new ExecutionContextImpl();
	          context.setConfigProperty("keepModeling", false);
	          ExecutionDiagnostic result = executor.execute(context, input1, output);
	          	// the output objects got captured in the output extent
	          	List<EObject> outObjects = output.getContents();
		   
		   
		   
		 count(outObjects);
		 
		 //countResource(inResource);
	}
	
	public static int count(List<EObject> list){
		//Model inModel = (Model) res.getContents().get(0);
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		
		for(EObject e: list){
			if(e instanceof NamedElement){
				NamedElement ne = (NamedElement)e;
				if(map.containsKey(ne.getName())){
					map.put(ne.getName(), map.get(ne.getName()) +1 );
				} else {
					map.put(ne.getName(), 1);
				}
			}
		}
		
		int total = 0;
		
		for(Entry<String, Integer> entry : map.entrySet()){
			System.out.println(entry.getKey()+": " +entry.getValue());
			total += entry.getValue();
		}
		
		System.out.println("Total: " + total);
		
		int totalNew = total;
		if(map.containsKey("Comment")){
			totalNew -= map.get("Comment").intValue();
		}
		if(map.containsKey("ConnectorEnd_end")){
			totalNew -=  map.get("ConnectorEnd_end").intValue();;
		}
				
		
		System.out.println("Total without comments and connector ends: " + totalNew);
		
		return 0;
	}
	
//	public static int countResource(Resource res) {
//		int elementsTotal = 0;
//		Model inModel = (Model) res.getContents().get(0);
//
//		// count Models
//		// Assumption Resource always contains only one model
//		elementsTotal += 1;
//
//		// count profile applications and references to other models
//		elementsTotal += countProfileApplications(inModel);
//
//		// count use cases (collaborations, application scenarios)
//		elementsTotal += countUseCase(inModel);
//
//		// count blocks (types)
//		elementsTotal += countBlocks(inModel);
//
//		System.out.println("elements (total): " + elementsTotal);
//		return elementsTotal;
//	}
//
//	public static int countBlocks(Model m) {
//		int blocks = 0;
//
//		for (Element e : m.allOwnedElements()) {
//			if (e instanceof org.eclipse.uml2.uml.Class) {
//				org.eclipse.uml2.uml.Class c = (org.eclipse.uml2.uml.Class) e;
//				if (c.getAppliedStereotype("SysML::Blocks::Block") != null) {
//					blocks++;
//				}
//			}
//		}
//
//		System.out.println("blocks: " + blocks);
//		return blocks;
//	}
//
//	public static int countUseCase(Model m) {
//		int useCases = 0;
//
//		for (Element e : m.allOwnedElements()) {
//			if (e instanceof UseCase) {
//				useCases++;
//			}
//		}
//
//		System.out.println("use cases: " + useCases);
//		return useCases;
//	}
//
//	public static int countProfileApplications(Model m) {
//		int profileApplication = m.getAllProfileApplications().size();
//		System.out.println("profile applications: " + profileApplication);
//		return profileApplication;
//	}


}
