/**
 */
package TAM.TAM_Task.impl;

import TAM.TAM_Task.TAM_TaskPackage;
import TAM.TAM_Task.TamMemReq;
import TAM.TAM_Task.TamMemType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.ResourceUsageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Mem Req</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.impl.TamMemReqImpl#getAccessFreq <em>Access Freq</em>}</li>
 *   <li>{@link TAM.TAM_Task.impl.TamMemReqImpl#getTransfMem <em>Transf Mem</em>}</li>
 *   <li>{@link TAM.TAM_Task.impl.TamMemReqImpl#getMem <em>Mem</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamMemReqImpl extends ResourceUsageImpl implements TamMemReq {
	/**
	 * The default value of the '{@link #getAccessFreq() <em>Access Freq</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessFreq()
	 * @generated
	 * @ordered
	 */
	protected static final String ACCESS_FREQ_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAccessFreq() <em>Access Freq</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessFreq()
	 * @generated
	 * @ordered
	 */
	protected String accessFreq = ACCESS_FREQ_EDEFAULT;

	/**
	 * The default value of the '{@link #getTransfMem() <em>Transf Mem</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransfMem()
	 * @generated
	 * @ordered
	 */
	protected static final String TRANSF_MEM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTransfMem() <em>Transf Mem</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransfMem()
	 * @generated
	 * @ordered
	 */
	protected String transfMem = TRANSF_MEM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMem() <em>Mem</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMem()
	 * @generated
	 * @ordered
	 */
	protected TamMemType mem;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamMemReqImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_TaskPackage.Literals.TAM_MEM_REQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAccessFreq() {
		return accessFreq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessFreq(String newAccessFreq) {
		String oldAccessFreq = accessFreq;
		accessFreq = newAccessFreq;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_MEM_REQ__ACCESS_FREQ, oldAccessFreq, accessFreq));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTransfMem() {
		return transfMem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransfMem(String newTransfMem) {
		String oldTransfMem = transfMem;
		transfMem = newTransfMem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_MEM_REQ__TRANSF_MEM, oldTransfMem, transfMem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamMemType getMem() {
		if (mem != null && mem.eIsProxy()) {
			InternalEObject oldMem = (InternalEObject)mem;
			mem = (TamMemType)eResolveProxy(oldMem);
			if (mem != oldMem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_TaskPackage.TAM_MEM_REQ__MEM, oldMem, mem));
			}
		}
		return mem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamMemType basicGetMem() {
		return mem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMem(TamMemType newMem) {
		TamMemType oldMem = mem;
		mem = newMem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_MEM_REQ__MEM, oldMem, mem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_MEM_REQ__ACCESS_FREQ:
				return getAccessFreq();
			case TAM_TaskPackage.TAM_MEM_REQ__TRANSF_MEM:
				return getTransfMem();
			case TAM_TaskPackage.TAM_MEM_REQ__MEM:
				if (resolve) return getMem();
				return basicGetMem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_MEM_REQ__ACCESS_FREQ:
				setAccessFreq((String)newValue);
				return;
			case TAM_TaskPackage.TAM_MEM_REQ__TRANSF_MEM:
				setTransfMem((String)newValue);
				return;
			case TAM_TaskPackage.TAM_MEM_REQ__MEM:
				setMem((TamMemType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_MEM_REQ__ACCESS_FREQ:
				setAccessFreq(ACCESS_FREQ_EDEFAULT);
				return;
			case TAM_TaskPackage.TAM_MEM_REQ__TRANSF_MEM:
				setTransfMem(TRANSF_MEM_EDEFAULT);
				return;
			case TAM_TaskPackage.TAM_MEM_REQ__MEM:
				setMem((TamMemType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_MEM_REQ__ACCESS_FREQ:
				return ACCESS_FREQ_EDEFAULT == null ? accessFreq != null : !ACCESS_FREQ_EDEFAULT.equals(accessFreq);
			case TAM_TaskPackage.TAM_MEM_REQ__TRANSF_MEM:
				return TRANSF_MEM_EDEFAULT == null ? transfMem != null : !TRANSF_MEM_EDEFAULT.equals(transfMem);
			case TAM_TaskPackage.TAM_MEM_REQ__MEM:
				return mem != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (accessFreq: ");
		result.append(accessFreq);
		result.append(", transfMem: ");
		result.append(transfMem);
		result.append(')');
		return result.toString();
	}

} //TamMemReqImpl
