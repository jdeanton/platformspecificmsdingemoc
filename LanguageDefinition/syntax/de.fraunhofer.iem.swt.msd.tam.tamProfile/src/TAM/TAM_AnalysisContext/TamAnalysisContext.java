/**
 */
package TAM.TAM_AnalysisContext;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaAnalysisContext;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Analysis Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_AnalysisContext.TamAnalysisContext#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link TAM.TAM_AnalysisContext.TamAnalysisContext#getConstraintsSatisfied <em>Constraints Satisfied</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamAnalysisContext()
 * @model
 * @generated
 */
public interface TamAnalysisContext extends GaAnalysisContext {
	/**
	 * Returns the value of the '<em><b>Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Time</em>' attribute.
	 * @see #setExecutionTime(String)
	 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamAnalysisContext_ExecutionTime()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getExecutionTime();

	/**
	 * Sets the value of the '{@link TAM.TAM_AnalysisContext.TamAnalysisContext#getExecutionTime <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Time</em>' attribute.
	 * @see #getExecutionTime()
	 * @generated
	 */
	void setExecutionTime(String value);

	/**
	 * Returns the value of the '<em><b>Constraints Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints Satisfied</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints Satisfied</em>' attribute.
	 * @see #setConstraintsSatisfied(String)
	 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamAnalysisContext_ConstraintsSatisfied()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getConstraintsSatisfied();

	/**
	 * Sets the value of the '{@link TAM.TAM_AnalysisContext.TamAnalysisContext#getConstraintsSatisfied <em>Constraints Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraints Satisfied</em>' attribute.
	 * @see #getConstraintsSatisfied()
	 * @generated
	 */
	void setConstraintsSatisfied(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * platform.size() = 1 
	 * and platform->forAll(p | p.isStereotypeApplied(
	 * TAM::TAM_AnalysisContext::TamResourcePlatform))
	 * and workload.size() = 1
	 * and workload->forAll(w | w.isStereotypeApplied(
	 * TAM::TAM_AnalysisContext::TamWorkloadBehavior))
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean Constraint1(DiagnosticChain diagnostics, Map<Object, Object> context);

} // TamAnalysisContext
