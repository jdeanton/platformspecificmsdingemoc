/**
 */
package TAM.TAM_Task;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see TAM.TAM_Task.TAM_TaskPackage
 * @generated
 */
public interface TAM_TaskFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_TaskFactory eINSTANCE = TAM.TAM_Task.impl.TAM_TaskFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Tam Mem Req</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Mem Req</em>'.
	 * @generated
	 */
	TamMemReq createTamMemReq();

	/**
	 * Returns a new object of class '<em>Tam Interface Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Interface Implementation</em>'.
	 * @generated
	 */
	TamInterfaceImplementation createTamInterfaceImplementation();

	/**
	 * Returns a new object of class '<em>Tam Periphery Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Periphery Type</em>'.
	 * @generated
	 */
	TamPeripheryType createTamPeripheryType();

	/**
	 * Returns a new object of class '<em>Tam Shared OS Resourc Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Shared OS Resourc Type</em>'.
	 * @generated
	 */
	TamSharedOSResourcType createTamSharedOSResourcType();

	/**
	 * Returns a new object of class '<em>Tam Resource Access</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Resource Access</em>'.
	 * @generated
	 */
	TamResourceAccess createTamResourceAccess();

	/**
	 * Returns a new object of class '<em>Tam Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Operation</em>'.
	 * @generated
	 */
	TamOperation createTamOperation();

	/**
	 * Returns a new object of class '<em>Tam Mem Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Mem Type</em>'.
	 * @generated
	 */
	TamMemType createTamMemType();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TAM_TaskPackage getTAM_TaskPackage();

} //TAM_TaskFactory
