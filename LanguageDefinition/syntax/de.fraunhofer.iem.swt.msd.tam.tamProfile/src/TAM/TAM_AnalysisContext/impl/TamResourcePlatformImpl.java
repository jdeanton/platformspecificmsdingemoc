/**
 */
package TAM.TAM_AnalysisContext.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;
import TAM.TAM_AnalysisContext.TamResourcePlatform;

import TAM.TAM_Platform.TAM_Communication.TamComConnection;

import TAM.TAM_Platform.TAM_ControlUnit.TamECU;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaResourcesPlatformImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Resource Platform</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_AnalysisContext.impl.TamResourcePlatformImpl#getConnections <em>Connections</em>}</li>
 *   <li>{@link TAM.TAM_AnalysisContext.impl.TamResourcePlatformImpl#getECUs <em>EC Us</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamResourcePlatformImpl extends GaResourcesPlatformImpl implements TamResourcePlatform {
	/**
	 * The cached value of the '{@link #getConnections() <em>Connections</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnections()
	 * @generated
	 * @ordered
	 */
	protected EList<TamComConnection> connections;

	/**
	 * The cached value of the '{@link #getECUs() <em>EC Us</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUs()
	 * @generated
	 * @ordered
	 */
	protected EList<TamECU> ecUs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamResourcePlatformImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_AnalysisContextPackage.Literals.TAM_RESOURCE_PLATFORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamComConnection> getConnections() {
		if (connections == null) {
			connections = new EObjectResolvingEList<TamComConnection>(TamComConnection.class, this, TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__CONNECTIONS);
		}
		return connections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamECU> getECUs() {
		if (ecUs == null) {
			ecUs = new EObjectResolvingEList<TamECU>(TamECU.class, this, TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__EC_US);
		}
		return ecUs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__CONNECTIONS:
				return getConnections();
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__EC_US:
				return getECUs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__CONNECTIONS:
				getConnections().clear();
				getConnections().addAll((Collection<? extends TamComConnection>)newValue);
				return;
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__EC_US:
				getECUs().clear();
				getECUs().addAll((Collection<? extends TamECU>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__CONNECTIONS:
				getConnections().clear();
				return;
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__EC_US:
				getECUs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__CONNECTIONS:
				return connections != null && !connections.isEmpty();
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM__EC_US:
				return ecUs != null && !ecUs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TamResourcePlatformImpl
