/**
 */
package TAM.TAM_AnalysisContext.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;
import TAM.TAM_AnalysisContext.TamAssumptionMSD;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaWorkloadEventImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Assumption MSD</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TamAssumptionMSDImpl extends GaWorkloadEventImpl implements TamAssumptionMSD {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamAssumptionMSDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_AnalysisContextPackage.Literals.TAM_ASSUMPTION_MSD;
	}

} //TamAssumptionMSDImpl
