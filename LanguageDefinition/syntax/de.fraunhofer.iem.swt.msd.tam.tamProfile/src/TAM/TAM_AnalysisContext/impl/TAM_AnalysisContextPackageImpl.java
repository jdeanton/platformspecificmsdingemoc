/**
 */
package TAM.TAM_AnalysisContext.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextFactory;
import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;
import TAM.TAM_AnalysisContext.TamAnalysisContext;
import TAM.TAM_AnalysisContext.TamAssumptionMSD;
import TAM.TAM_AnalysisContext.TamRequirementMSD;
import TAM.TAM_AnalysisContext.TamResourcePlatform;
import TAM.TAM_AnalysisContext.TamWorkloadBehavior;

import TAM.TAM_AnalysisContext.util.TAM_AnalysisContextValidator;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;

import TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;

import TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;

import TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl;

import TAM.TAM_Task.TAM_TaskPackage;

import TAM.TAM_Task.impl.TAM_TaskPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.uml2.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_AnalysisContextPackageImpl extends EPackageImpl implements TAM_AnalysisContextPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamWorkloadBehaviorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamAnalysisContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamResourcePlatformEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamAssumptionMSDEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamRequirementMSDEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TAM_AnalysisContextPackageImpl() {
		super(eNS_URI, TAM_AnalysisContextFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TAM_AnalysisContextPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TAM_AnalysisContextPackage init() {
		if (isInited) return (TAM_AnalysisContextPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI);

		// Obtain or create and register package
		TAM_AnalysisContextPackageImpl theTAM_AnalysisContextPackage = (TAM_AnalysisContextPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TAM_AnalysisContextPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TAM_AnalysisContextPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MARTEPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TAM_ControlUnitPackageImpl theTAM_ControlUnitPackage = (TAM_ControlUnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) instanceof TAM_ControlUnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) : TAM_ControlUnitPackage.eINSTANCE);
		TAM_OperatingSystemPackageImpl theTAM_OperatingSystemPackage = (TAM_OperatingSystemPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) instanceof TAM_OperatingSystemPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) : TAM_OperatingSystemPackage.eINSTANCE);
		TAM_CommunicationPackageImpl theTAM_CommunicationPackage = (TAM_CommunicationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) instanceof TAM_CommunicationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) : TAM_CommunicationPackage.eINSTANCE);
		TAM_TaskPackageImpl theTAM_TaskPackage = (TAM_TaskPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) instanceof TAM_TaskPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI) : TAM_TaskPackage.eINSTANCE);

		// Create package meta-data objects
		theTAM_AnalysisContextPackage.createPackageContents();
		theTAM_ControlUnitPackage.createPackageContents();
		theTAM_OperatingSystemPackage.createPackageContents();
		theTAM_CommunicationPackage.createPackageContents();
		theTAM_TaskPackage.createPackageContents();

		// Initialize created meta-data
		theTAM_AnalysisContextPackage.initializePackageContents();
		theTAM_ControlUnitPackage.initializePackageContents();
		theTAM_OperatingSystemPackage.initializePackageContents();
		theTAM_CommunicationPackage.initializePackageContents();
		theTAM_TaskPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theTAM_AnalysisContextPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return TAM_AnalysisContextValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theTAM_AnalysisContextPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TAM_AnalysisContextPackage.eNS_URI, theTAM_AnalysisContextPackage);
		return theTAM_AnalysisContextPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamWorkloadBehavior() {
		return tamWorkloadBehaviorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTamWorkloadBehavior__Constraint1__DiagnosticChain_Map() {
		return tamWorkloadBehaviorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamAnalysisContext() {
		return tamAnalysisContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamAnalysisContext_ExecutionTime() {
		return (EAttribute)tamAnalysisContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamAnalysisContext_ConstraintsSatisfied() {
		return (EAttribute)tamAnalysisContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTamAnalysisContext__Constraint1__DiagnosticChain_Map() {
		return tamAnalysisContextEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamResourcePlatform() {
		return tamResourcePlatformEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamResourcePlatform_Connections() {
		return (EReference)tamResourcePlatformEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamResourcePlatform_ECUs() {
		return (EReference)tamResourcePlatformEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamAssumptionMSD() {
		return tamAssumptionMSDEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamRequirementMSD() {
		return tamRequirementMSDEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamRequirementMSD_ActivationCount() {
		return (EAttribute)tamRequirementMSDEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamRequirementMSD_ConstraintsSatisfied() {
		return (EAttribute)tamRequirementMSDEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_AnalysisContextFactory getTAM_AnalysisContextFactory() {
		return (TAM_AnalysisContextFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tamWorkloadBehaviorEClass = createEClass(TAM_WORKLOAD_BEHAVIOR);
		createEOperation(tamWorkloadBehaviorEClass, TAM_WORKLOAD_BEHAVIOR___CONSTRAINT1__DIAGNOSTICCHAIN_MAP);

		tamAnalysisContextEClass = createEClass(TAM_ANALYSIS_CONTEXT);
		createEAttribute(tamAnalysisContextEClass, TAM_ANALYSIS_CONTEXT__EXECUTION_TIME);
		createEAttribute(tamAnalysisContextEClass, TAM_ANALYSIS_CONTEXT__CONSTRAINTS_SATISFIED);
		createEOperation(tamAnalysisContextEClass, TAM_ANALYSIS_CONTEXT___CONSTRAINT1__DIAGNOSTICCHAIN_MAP);

		tamResourcePlatformEClass = createEClass(TAM_RESOURCE_PLATFORM);
		createEReference(tamResourcePlatformEClass, TAM_RESOURCE_PLATFORM__CONNECTIONS);
		createEReference(tamResourcePlatformEClass, TAM_RESOURCE_PLATFORM__EC_US);

		tamAssumptionMSDEClass = createEClass(TAM_ASSUMPTION_MSD);

		tamRequirementMSDEClass = createEClass(TAM_REQUIREMENT_MSD);
		createEAttribute(tamRequirementMSDEClass, TAM_REQUIREMENT_MSD__ACTIVATION_COUNT);
		createEAttribute(tamRequirementMSDEClass, TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GQAMPackage theGQAMPackage = (GQAMPackage)EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		TAM_CommunicationPackage theTAM_CommunicationPackage = (TAM_CommunicationPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI);
		TAM_ControlUnitPackage theTAM_ControlUnitPackage = (TAM_ControlUnitPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tamWorkloadBehaviorEClass.getESuperTypes().add(theGQAMPackage.getGaWorkloadBehavior());
		tamAnalysisContextEClass.getESuperTypes().add(theGQAMPackage.getGaAnalysisContext());
		tamResourcePlatformEClass.getESuperTypes().add(theGQAMPackage.getGaResourcesPlatform());
		tamAssumptionMSDEClass.getESuperTypes().add(theGQAMPackage.getGaWorkloadEvent());
		tamRequirementMSDEClass.getESuperTypes().add(theGQAMPackage.getGaScenario());

		// Initialize classes, features, and operations; add parameters
		initEClass(tamWorkloadBehaviorEClass, TamWorkloadBehavior.class, "TamWorkloadBehavior", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getTamWorkloadBehavior__Constraint1__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "Constraint1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(tamAnalysisContextEClass, TamAnalysisContext.class, "TamAnalysisContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamAnalysisContext_ExecutionTime(), theTypesPackage.getString(), "executionTime", null, 0, 1, TamAnalysisContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamAnalysisContext_ConstraintsSatisfied(), theTypesPackage.getString(), "constraintsSatisfied", null, 0, 1, TamAnalysisContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getTamAnalysisContext__Constraint1__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "Constraint1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(tamResourcePlatformEClass, TamResourcePlatform.class, "TamResourcePlatform", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamResourcePlatform_Connections(), theTAM_CommunicationPackage.getTamComConnection(), null, "connections", null, 0, -1, TamResourcePlatform.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamResourcePlatform_ECUs(), theTAM_ControlUnitPackage.getTamECU(), null, "ECUs", null, 0, -1, TamResourcePlatform.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamAssumptionMSDEClass, TamAssumptionMSD.class, "TamAssumptionMSD", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tamRequirementMSDEClass, TamRequirementMSD.class, "TamRequirementMSD", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamRequirementMSD_ActivationCount(), theTypesPackage.getString(), "activationCount", null, 0, 1, TamRequirementMSD.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamRequirementMSD_ConstraintsSatisfied(), theTypesPackage.getString(), "constraintsSatisfied", null, 0, 1, TamRequirementMSD.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TAM_AnalysisContextPackageImpl
