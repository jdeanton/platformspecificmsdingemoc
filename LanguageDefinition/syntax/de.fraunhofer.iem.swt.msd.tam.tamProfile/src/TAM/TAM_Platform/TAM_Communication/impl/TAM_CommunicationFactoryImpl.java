/**
 */
package TAM.TAM_Platform.TAM_Communication.impl;

import TAM.TAM_Platform.TAM_Communication.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_CommunicationFactoryImpl extends EFactoryImpl implements TAM_CommunicationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TAM_CommunicationFactory init() {
		try {
			TAM_CommunicationFactory theTAM_CommunicationFactory = (TAM_CommunicationFactory)EPackage.Registry.INSTANCE.getEFactory(TAM_CommunicationPackage.eNS_URI);
			if (theTAM_CommunicationFactory != null) {
				return theTAM_CommunicationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TAM_CommunicationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_CommunicationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TAM_CommunicationPackage.TAM_COM_INTERFACE: return createTamComInterface();
			case TAM_CommunicationPackage.TAM_PROTOCOL: return createTamProtocol();
			case TAM_CommunicationPackage.TAM_COM_SERVICE: return createTamComService();
			case TAM_CommunicationPackage.TAM_COM_CONNECTION: return createTamComConnection();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComInterface createTamComInterface() {
		TamComInterfaceImpl tamComInterface = new TamComInterfaceImpl();
		return tamComInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamProtocol createTamProtocol() {
		TamProtocolImpl tamProtocol = new TamProtocolImpl();
		return tamProtocol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComService createTamComService() {
		TamComServiceImpl tamComService = new TamComServiceImpl();
		return tamComService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamComConnection createTamComConnection() {
		TamComConnectionImpl tamComConnection = new TamComConnectionImpl();
		return tamComConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_CommunicationPackage getTAM_CommunicationPackage() {
		return (TAM_CommunicationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TAM_CommunicationPackage getPackage() {
		return TAM_CommunicationPackage.eINSTANCE;
	}

} //TAM_CommunicationFactoryImpl
