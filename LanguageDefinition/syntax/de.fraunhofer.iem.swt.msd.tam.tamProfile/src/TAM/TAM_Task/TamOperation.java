/**
 */
package TAM.TAM_Task;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.TamOperation#getMemReq <em>Mem Req</em>}</li>
 *   <li>{@link TAM.TAM_Task.TamOperation#getResAccess <em>Res Access</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Task.TAM_TaskPackage#getTamOperation()
 * @model
 * @generated
 */
public interface TamOperation extends ResourceUsage {
	/**
	 * Returns the value of the '<em><b>Mem Req</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Task.TamMemReq}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mem Req</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mem Req</em>' reference list.
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamOperation_MemReq()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamMemReq> getMemReq();

	/**
	 * Returns the value of the '<em><b>Res Access</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Task.TamResourceAccess}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Res Access</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Res Access</em>' reference list.
	 * @see TAM.TAM_Task.TAM_TaskPackage#getTamOperation_ResAccess()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamResourceAccess> getResAccess();

} // TamOperation
