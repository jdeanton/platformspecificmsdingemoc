/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam IO</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamIO()
 * @model
 * @generated
 */
public interface TamIO extends TamPeripheryUnit {
} // TamIO
