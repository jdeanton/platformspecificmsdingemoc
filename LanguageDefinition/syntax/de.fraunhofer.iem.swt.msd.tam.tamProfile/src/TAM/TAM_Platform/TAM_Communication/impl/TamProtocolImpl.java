/**
 */
package TAM.TAM_Platform.TAM_Communication.impl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;
import TAM.TAM_Platform.TAM_Communication.TamProtocol;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaCommChannelImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Protocol</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl#getTransmOvhd <em>Transm Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl#getMsgCtrlOvhd <em>Msg Ctrl Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl#getCommTxT <em>Comm Tx T</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.impl.TamProtocolImpl#getCommRcvT <em>Comm Rcv T</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamProtocolImpl extends GaCommChannelImpl implements TamProtocol {
	/**
	 * The default value of the '{@link #getTransmOvhd() <em>Transm Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransmOvhd()
	 * @generated
	 * @ordered
	 */
	protected static final String TRANSM_OVHD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTransmOvhd() <em>Transm Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransmOvhd()
	 * @generated
	 * @ordered
	 */
	protected String transmOvhd = TRANSM_OVHD_EDEFAULT;

	/**
	 * The default value of the '{@link #getMsgCtrlOvhd() <em>Msg Ctrl Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMsgCtrlOvhd()
	 * @generated
	 * @ordered
	 */
	protected static final String MSG_CTRL_OVHD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMsgCtrlOvhd() <em>Msg Ctrl Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMsgCtrlOvhd()
	 * @generated
	 * @ordered
	 */
	protected String msgCtrlOvhd = MSG_CTRL_OVHD_EDEFAULT;

	/**
	 * The default value of the '{@link #getCommTxT() <em>Comm Tx T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommTxT()
	 * @generated
	 * @ordered
	 */
	protected static final String COMM_TX_T_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCommTxT() <em>Comm Tx T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommTxT()
	 * @generated
	 * @ordered
	 */
	protected String commTxT = COMM_TX_T_EDEFAULT;

	/**
	 * The default value of the '{@link #getCommRcvT() <em>Comm Rcv T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommRcvT()
	 * @generated
	 * @ordered
	 */
	protected static final String COMM_RCV_T_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCommRcvT() <em>Comm Rcv T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommRcvT()
	 * @generated
	 * @ordered
	 */
	protected String commRcvT = COMM_RCV_T_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamProtocolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_CommunicationPackage.Literals.TAM_PROTOCOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTransmOvhd() {
		return transmOvhd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransmOvhd(String newTransmOvhd) {
		String oldTransmOvhd = transmOvhd;
		transmOvhd = newTransmOvhd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_PROTOCOL__TRANSM_OVHD, oldTransmOvhd, transmOvhd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMsgCtrlOvhd() {
		return msgCtrlOvhd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMsgCtrlOvhd(String newMsgCtrlOvhd) {
		String oldMsgCtrlOvhd = msgCtrlOvhd;
		msgCtrlOvhd = newMsgCtrlOvhd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_PROTOCOL__MSG_CTRL_OVHD, oldMsgCtrlOvhd, msgCtrlOvhd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCommTxT() {
		return commTxT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommTxT(String newCommTxT) {
		String oldCommTxT = commTxT;
		commTxT = newCommTxT;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_PROTOCOL__COMM_TX_T, oldCommTxT, commTxT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCommRcvT() {
		return commRcvT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommRcvT(String newCommRcvT) {
		String oldCommRcvT = commRcvT;
		commRcvT = newCommRcvT;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_CommunicationPackage.TAM_PROTOCOL__COMM_RCV_T, oldCommRcvT, commRcvT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_PROTOCOL__TRANSM_OVHD:
				return getTransmOvhd();
			case TAM_CommunicationPackage.TAM_PROTOCOL__MSG_CTRL_OVHD:
				return getMsgCtrlOvhd();
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_TX_T:
				return getCommTxT();
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_RCV_T:
				return getCommRcvT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_PROTOCOL__TRANSM_OVHD:
				setTransmOvhd((String)newValue);
				return;
			case TAM_CommunicationPackage.TAM_PROTOCOL__MSG_CTRL_OVHD:
				setMsgCtrlOvhd((String)newValue);
				return;
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_TX_T:
				setCommTxT((String)newValue);
				return;
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_RCV_T:
				setCommRcvT((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_PROTOCOL__TRANSM_OVHD:
				setTransmOvhd(TRANSM_OVHD_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_PROTOCOL__MSG_CTRL_OVHD:
				setMsgCtrlOvhd(MSG_CTRL_OVHD_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_TX_T:
				setCommTxT(COMM_TX_T_EDEFAULT);
				return;
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_RCV_T:
				setCommRcvT(COMM_RCV_T_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_CommunicationPackage.TAM_PROTOCOL__TRANSM_OVHD:
				return TRANSM_OVHD_EDEFAULT == null ? transmOvhd != null : !TRANSM_OVHD_EDEFAULT.equals(transmOvhd);
			case TAM_CommunicationPackage.TAM_PROTOCOL__MSG_CTRL_OVHD:
				return MSG_CTRL_OVHD_EDEFAULT == null ? msgCtrlOvhd != null : !MSG_CTRL_OVHD_EDEFAULT.equals(msgCtrlOvhd);
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_TX_T:
				return COMM_TX_T_EDEFAULT == null ? commTxT != null : !COMM_TX_T_EDEFAULT.equals(commTxT);
			case TAM_CommunicationPackage.TAM_PROTOCOL__COMM_RCV_T:
				return COMM_RCV_T_EDEFAULT == null ? commRcvT != null : !COMM_RCV_T_EDEFAULT.equals(commRcvT);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (transmOvhd: ");
		result.append(transmOvhd);
		result.append(", msgCtrlOvhd: ");
		result.append(msgCtrlOvhd);
		result.append(", commTxT: ");
		result.append(commTxT);
		result.append(", commRcvT: ");
		result.append(commRcvT);
		result.append(')');
		return result.toString();
	}

} //TamProtocolImpl
