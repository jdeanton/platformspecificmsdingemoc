/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem.impl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSComChannel;
import TAM.TAM_Platform.TAM_OperatingSystem.TamOSService;
import TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS;
import TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler;
import TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam RTOS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl#getScheduler <em>Scheduler</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl#getComChannels <em>Com Channels</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl#getSharedRes <em>Shared Res</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl#getOsServices <em>Os Services</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamRTOSImpl extends TamOSResourceImpl implements TamRTOS {
	/**
	 * The cached value of the '{@link #getScheduler() <em>Scheduler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduler()
	 * @generated
	 * @ordered
	 */
	protected TamScheduler scheduler;

	/**
	 * The cached value of the '{@link #getComChannels() <em>Com Channels</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComChannels()
	 * @generated
	 * @ordered
	 */
	protected EList<TamOSComChannel> comChannels;

	/**
	 * The cached value of the '{@link #getSharedRes() <em>Shared Res</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharedRes()
	 * @generated
	 * @ordered
	 */
	protected EList<TamSharedOSResource> sharedRes;

	/**
	 * The cached value of the '{@link #getOsServices() <em>Os Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsServices()
	 * @generated
	 * @ordered
	 */
	protected EList<TamOSService> osServices;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamRTOSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_OperatingSystemPackage.Literals.TAM_RTOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamScheduler getScheduler() {
		if (scheduler != null && scheduler.eIsProxy()) {
			InternalEObject oldScheduler = (InternalEObject)scheduler;
			scheduler = (TamScheduler)eResolveProxy(oldScheduler);
			if (scheduler != oldScheduler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_OperatingSystemPackage.TAM_RTOS__SCHEDULER, oldScheduler, scheduler));
			}
		}
		return scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TamScheduler basicGetScheduler() {
		return scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScheduler(TamScheduler newScheduler) {
		TamScheduler oldScheduler = scheduler;
		scheduler = newScheduler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_OperatingSystemPackage.TAM_RTOS__SCHEDULER, oldScheduler, scheduler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamOSComChannel> getComChannels() {
		if (comChannels == null) {
			comChannels = new EObjectResolvingEList<TamOSComChannel>(TamOSComChannel.class, this, TAM_OperatingSystemPackage.TAM_RTOS__COM_CHANNELS);
		}
		return comChannels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamSharedOSResource> getSharedRes() {
		if (sharedRes == null) {
			sharedRes = new EObjectResolvingEList<TamSharedOSResource>(TamSharedOSResource.class, this, TAM_OperatingSystemPackage.TAM_RTOS__SHARED_RES);
		}
		return sharedRes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TamOSService> getOsServices() {
		if (osServices == null) {
			osServices = new EObjectResolvingEList<TamOSService>(TamOSService.class, this, TAM_OperatingSystemPackage.TAM_RTOS__OS_SERVICES);
		}
		return osServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_RTOS__SCHEDULER:
				if (resolve) return getScheduler();
				return basicGetScheduler();
			case TAM_OperatingSystemPackage.TAM_RTOS__COM_CHANNELS:
				return getComChannels();
			case TAM_OperatingSystemPackage.TAM_RTOS__SHARED_RES:
				return getSharedRes();
			case TAM_OperatingSystemPackage.TAM_RTOS__OS_SERVICES:
				return getOsServices();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_RTOS__SCHEDULER:
				setScheduler((TamScheduler)newValue);
				return;
			case TAM_OperatingSystemPackage.TAM_RTOS__COM_CHANNELS:
				getComChannels().clear();
				getComChannels().addAll((Collection<? extends TamOSComChannel>)newValue);
				return;
			case TAM_OperatingSystemPackage.TAM_RTOS__SHARED_RES:
				getSharedRes().clear();
				getSharedRes().addAll((Collection<? extends TamSharedOSResource>)newValue);
				return;
			case TAM_OperatingSystemPackage.TAM_RTOS__OS_SERVICES:
				getOsServices().clear();
				getOsServices().addAll((Collection<? extends TamOSService>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_RTOS__SCHEDULER:
				setScheduler((TamScheduler)null);
				return;
			case TAM_OperatingSystemPackage.TAM_RTOS__COM_CHANNELS:
				getComChannels().clear();
				return;
			case TAM_OperatingSystemPackage.TAM_RTOS__SHARED_RES:
				getSharedRes().clear();
				return;
			case TAM_OperatingSystemPackage.TAM_RTOS__OS_SERVICES:
				getOsServices().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_OperatingSystemPackage.TAM_RTOS__SCHEDULER:
				return scheduler != null;
			case TAM_OperatingSystemPackage.TAM_RTOS__COM_CHANNELS:
				return comChannels != null && !comChannels.isEmpty();
			case TAM_OperatingSystemPackage.TAM_RTOS__SHARED_RES:
				return sharedRes != null && !sharedRes.isEmpty();
			case TAM_OperatingSystemPackage.TAM_RTOS__OS_SERVICES:
				return osServices != null && !osServices.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TamRTOSImpl
