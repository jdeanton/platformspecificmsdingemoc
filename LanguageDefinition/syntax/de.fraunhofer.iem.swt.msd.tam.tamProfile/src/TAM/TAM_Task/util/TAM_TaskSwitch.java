/**
 */
package TAM.TAM_Task.util;

import TAM.TAM_Task.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see TAM.TAM_Task.TAM_TaskPackage
 * @generated
 */
public class TAM_TaskSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TAM_TaskPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_TaskSwitch() {
		if (modelPackage == null) {
			modelPackage = TAM_TaskPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TAM_TaskPackage.TAM_MEM_REQ: {
				TamMemReq tamMemReq = (TamMemReq)theEObject;
				T result = caseTamMemReq(tamMemReq);
				if (result == null) result = caseResourceUsage(tamMemReq);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION: {
				TamInterfaceImplementation tamInterfaceImplementation = (TamInterfaceImplementation)theEObject;
				T result = caseTamInterfaceImplementation(tamInterfaceImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TAM_TaskPackage.TAM_PERIPHERY_TYPE: {
				TamPeripheryType tamPeripheryType = (TamPeripheryType)theEObject;
				T result = caseTamPeripheryType(tamPeripheryType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TAM_TaskPackage.TAM_SHARED_OS_RESOURC_TYPE: {
				TamSharedOSResourcType tamSharedOSResourcType = (TamSharedOSResourcType)theEObject;
				T result = caseTamSharedOSResourcType(tamSharedOSResourcType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TAM_TaskPackage.TAM_RESOURCE_ACCESS: {
				TamResourceAccess tamResourceAccess = (TamResourceAccess)theEObject;
				T result = caseTamResourceAccess(tamResourceAccess);
				if (result == null) result = caseResourceUsage(tamResourceAccess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TAM_TaskPackage.TAM_OPERATION: {
				TamOperation tamOperation = (TamOperation)theEObject;
				T result = caseTamOperation(tamOperation);
				if (result == null) result = caseResourceUsage(tamOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TAM_TaskPackage.TAM_MEM_TYPE: {
				TamMemType tamMemType = (TamMemType)theEObject;
				T result = caseTamMemType(tamMemType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tam Mem Req</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tam Mem Req</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTamMemReq(TamMemReq object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tam Interface Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tam Interface Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTamInterfaceImplementation(TamInterfaceImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tam Periphery Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tam Periphery Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTamPeripheryType(TamPeripheryType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tam Shared OS Resourc Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tam Shared OS Resourc Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTamSharedOSResourcType(TamSharedOSResourcType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tam Resource Access</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tam Resource Access</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTamResourceAccess(TamResourceAccess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tam Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tam Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTamOperation(TamOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tam Mem Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tam Mem Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTamMemType(TamMemType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceUsage(ResourceUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TAM_TaskSwitch
