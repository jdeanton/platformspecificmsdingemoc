/**
 */
package TAM.TAM_Task.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;

import TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextPackageImpl;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;

import TAM.TAM_Platform.TAM_Communication.impl.TAM_CommunicationPackageImpl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;

import TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl;

import TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemPackage;

import TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl;

import TAM.TAM_Task.TAM_TaskFactory;
import TAM.TAM_Task.TAM_TaskPackage;
import TAM.TAM_Task.TamInterfaceImplementation;
import TAM.TAM_Task.TamMemReq;
import TAM.TAM_Task.TamMemType;
import TAM.TAM_Task.TamOperation;
import TAM.TAM_Task.TamPeripheryType;
import TAM.TAM_Task.TamResourceAccess;
import TAM.TAM_Task.TamSharedOSResourcType;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TAM_TaskPackageImpl extends EPackageImpl implements TAM_TaskPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamMemReqEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamInterfaceImplementationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamPeripheryTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamSharedOSResourcTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamResourceAccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tamMemTypeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see TAM.TAM_Task.TAM_TaskPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TAM_TaskPackageImpl() {
		super(eNS_URI, TAM_TaskFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TAM_TaskPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TAM_TaskPackage init() {
		if (isInited) return (TAM_TaskPackage)EPackage.Registry.INSTANCE.getEPackage(TAM_TaskPackage.eNS_URI);

		// Obtain or create and register package
		TAM_TaskPackageImpl theTAM_TaskPackage = (TAM_TaskPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TAM_TaskPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TAM_TaskPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		MARTEPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TAM_AnalysisContextPackageImpl theTAM_AnalysisContextPackage = (TAM_AnalysisContextPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) instanceof TAM_AnalysisContextPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_AnalysisContextPackage.eNS_URI) : TAM_AnalysisContextPackage.eINSTANCE);
		TAM_ControlUnitPackageImpl theTAM_ControlUnitPackage = (TAM_ControlUnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) instanceof TAM_ControlUnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_ControlUnitPackage.eNS_URI) : TAM_ControlUnitPackage.eINSTANCE);
		TAM_OperatingSystemPackageImpl theTAM_OperatingSystemPackage = (TAM_OperatingSystemPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) instanceof TAM_OperatingSystemPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_OperatingSystemPackage.eNS_URI) : TAM_OperatingSystemPackage.eINSTANCE);
		TAM_CommunicationPackageImpl theTAM_CommunicationPackage = (TAM_CommunicationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) instanceof TAM_CommunicationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TAM_CommunicationPackage.eNS_URI) : TAM_CommunicationPackage.eINSTANCE);

		// Create package meta-data objects
		theTAM_TaskPackage.createPackageContents();
		theTAM_AnalysisContextPackage.createPackageContents();
		theTAM_ControlUnitPackage.createPackageContents();
		theTAM_OperatingSystemPackage.createPackageContents();
		theTAM_CommunicationPackage.createPackageContents();

		// Initialize created meta-data
		theTAM_TaskPackage.initializePackageContents();
		theTAM_AnalysisContextPackage.initializePackageContents();
		theTAM_ControlUnitPackage.initializePackageContents();
		theTAM_OperatingSystemPackage.initializePackageContents();
		theTAM_CommunicationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTAM_TaskPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TAM_TaskPackage.eNS_URI, theTAM_TaskPackage);
		return theTAM_TaskPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamMemReq() {
		return tamMemReqEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamMemReq_AccessFreq() {
		return (EAttribute)tamMemReqEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamMemReq_TransfMem() {
		return (EAttribute)tamMemReqEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamMemReq_Mem() {
		return (EReference)tamMemReqEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamInterfaceImplementation() {
		return tamInterfaceImplementationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamInterfaceImplementation_Base_Port() {
		return (EReference)tamInterfaceImplementationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamInterfaceImplementation_ImplExecOvhd() {
		return (EAttribute)tamInterfaceImplementationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamInterfaceImplementation_ImplMemOvhd() {
		return (EAttribute)tamInterfaceImplementationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamPeripheryType() {
		return tamPeripheryTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamPeripheryType_Base_Classifier() {
		return (EReference)tamPeripheryTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamSharedOSResourcType() {
		return tamSharedOSResourcTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamSharedOSResourcType_Base_Classifier() {
		return (EReference)tamSharedOSResourcTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamResourceAccess() {
		return tamResourceAccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTamResourceAccess_NumAccess() {
		return (EAttribute)tamResourceAccessEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamResourceAccess_PerResource() {
		return (EReference)tamResourceAccessEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamResourceAccess_OsResource() {
		return (EReference)tamResourceAccessEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamOperation() {
		return tamOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamOperation_MemReq() {
		return (EReference)tamOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamOperation_ResAccess() {
		return (EReference)tamOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTamMemType() {
		return tamMemTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTamMemType_Base_Classifier() {
		return (EReference)tamMemTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_TaskFactory getTAM_TaskFactory() {
		return (TAM_TaskFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tamMemReqEClass = createEClass(TAM_MEM_REQ);
		createEAttribute(tamMemReqEClass, TAM_MEM_REQ__ACCESS_FREQ);
		createEAttribute(tamMemReqEClass, TAM_MEM_REQ__TRANSF_MEM);
		createEReference(tamMemReqEClass, TAM_MEM_REQ__MEM);

		tamInterfaceImplementationEClass = createEClass(TAM_INTERFACE_IMPLEMENTATION);
		createEReference(tamInterfaceImplementationEClass, TAM_INTERFACE_IMPLEMENTATION__BASE_PORT);
		createEAttribute(tamInterfaceImplementationEClass, TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD);
		createEAttribute(tamInterfaceImplementationEClass, TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD);

		tamPeripheryTypeEClass = createEClass(TAM_PERIPHERY_TYPE);
		createEReference(tamPeripheryTypeEClass, TAM_PERIPHERY_TYPE__BASE_CLASSIFIER);

		tamSharedOSResourcTypeEClass = createEClass(TAM_SHARED_OS_RESOURC_TYPE);
		createEReference(tamSharedOSResourcTypeEClass, TAM_SHARED_OS_RESOURC_TYPE__BASE_CLASSIFIER);

		tamResourceAccessEClass = createEClass(TAM_RESOURCE_ACCESS);
		createEAttribute(tamResourceAccessEClass, TAM_RESOURCE_ACCESS__NUM_ACCESS);
		createEReference(tamResourceAccessEClass, TAM_RESOURCE_ACCESS__PER_RESOURCE);
		createEReference(tamResourceAccessEClass, TAM_RESOURCE_ACCESS__OS_RESOURCE);

		tamOperationEClass = createEClass(TAM_OPERATION);
		createEReference(tamOperationEClass, TAM_OPERATION__MEM_REQ);
		createEReference(tamOperationEClass, TAM_OPERATION__RES_ACCESS);

		tamMemTypeEClass = createEClass(TAM_MEM_TYPE);
		createEReference(tamMemTypeEClass, TAM_MEM_TYPE__BASE_CLASSIFIER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GRMPackage theGRMPackage = (GRMPackage)EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tamMemReqEClass.getESuperTypes().add(theGRMPackage.getResourceUsage());
		tamResourceAccessEClass.getESuperTypes().add(theGRMPackage.getResourceUsage());
		tamOperationEClass.getESuperTypes().add(theGRMPackage.getResourceUsage());

		// Initialize classes, features, and operations; add parameters
		initEClass(tamMemReqEClass, TamMemReq.class, "TamMemReq", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamMemReq_AccessFreq(), theTypesPackage.getString(), "accessFreq", null, 0, 1, TamMemReq.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamMemReq_TransfMem(), theTypesPackage.getString(), "transfMem", null, 0, 1, TamMemReq.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamMemReq_Mem(), this.getTamMemType(), null, "mem", null, 0, 1, TamMemReq.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamInterfaceImplementationEClass, TamInterfaceImplementation.class, "TamInterfaceImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamInterfaceImplementation_Base_Port(), theUMLPackage.getPort(), null, "base_Port", null, 1, 1, TamInterfaceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamInterfaceImplementation_ImplExecOvhd(), theTypesPackage.getString(), "implExecOvhd", null, 0, 1, TamInterfaceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTamInterfaceImplementation_ImplMemOvhd(), theTypesPackage.getString(), "implMemOvhd", null, 0, 1, TamInterfaceImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamPeripheryTypeEClass, TamPeripheryType.class, "TamPeripheryType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamPeripheryType_Base_Classifier(), theUMLPackage.getClassifier(), null, "base_Classifier", null, 1, 1, TamPeripheryType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamSharedOSResourcTypeEClass, TamSharedOSResourcType.class, "TamSharedOSResourcType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamSharedOSResourcType_Base_Classifier(), theUMLPackage.getClassifier(), null, "base_Classifier", null, 1, 1, TamSharedOSResourcType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamResourceAccessEClass, TamResourceAccess.class, "TamResourceAccess", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTamResourceAccess_NumAccess(), theTypesPackage.getString(), "numAccess", "1", 0, 1, TamResourceAccess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamResourceAccess_PerResource(), this.getTamPeripheryType(), null, "perResource", null, 0, 1, TamResourceAccess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamResourceAccess_OsResource(), this.getTamSharedOSResourcType(), null, "osResource", null, 0, 1, TamResourceAccess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamOperationEClass, TamOperation.class, "TamOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamOperation_MemReq(), this.getTamMemReq(), null, "memReq", null, 0, -1, TamOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTamOperation_ResAccess(), this.getTamResourceAccess(), null, "resAccess", null, 0, -1, TamOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tamMemTypeEClass, TamMemType.class, "TamMemType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTamMemType_Base_Classifier(), theUMLPackage.getClassifier(), null, "base_Classifier", null, 1, 1, TamMemType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TAM_TaskPackageImpl
