/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaExecHost;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Processing Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getNumCores <em>Num Cores</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getCoreSyncOvhd <em>Core Sync Ovhd</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamProcessingUnit()
 * @model
 * @generated
 */
public interface TamProcessingUnit extends GaExecHost {
	/**
	 * Returns the value of the '<em><b>Num Cores</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Cores</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Cores</em>' attribute.
	 * @see #setNumCores(int)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamProcessingUnit_NumCores()
	 * @model dataType="org.eclipse.uml2.types.Integer" ordered="false"
	 * @generated
	 */
	int getNumCores();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getNumCores <em>Num Cores</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Cores</em>' attribute.
	 * @see #getNumCores()
	 * @generated
	 */
	void setNumCores(int value);

	/**
	 * Returns the value of the '<em><b>Core Sync Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Core Sync Ovhd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Core Sync Ovhd</em>' attribute.
	 * @see #setCoreSyncOvhd(String)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamProcessingUnit_CoreSyncOvhd()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getCoreSyncOvhd();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getCoreSyncOvhd <em>Core Sync Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Core Sync Ovhd</em>' attribute.
	 * @see #getCoreSyncOvhd()
	 * @generated
	 */
	void setCoreSyncOvhd(String value);

} // TamProcessingUnit
