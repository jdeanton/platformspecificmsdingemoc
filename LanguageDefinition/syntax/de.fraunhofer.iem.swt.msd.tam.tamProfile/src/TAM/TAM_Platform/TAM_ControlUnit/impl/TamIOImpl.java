/**
 */
package TAM.TAM_Platform.TAM_ControlUnit.impl;

import TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage;
import TAM.TAM_Platform.TAM_ControlUnit.TamIO;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam IO</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TamIOImpl extends TamPeripheryUnitImpl implements TamIO {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamIOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_ControlUnitPackage.Literals.TAM_IO;
	}

} //TamIOImpl
