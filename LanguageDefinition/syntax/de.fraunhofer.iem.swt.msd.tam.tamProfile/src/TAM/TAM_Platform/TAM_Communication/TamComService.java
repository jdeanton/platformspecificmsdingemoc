/**
 */
package TAM.TAM_Platform.TAM_Communication;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Com Service</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComService()
 * @model
 * @generated
 */
public interface TamComService extends TamProtocol {
} // TamComService
