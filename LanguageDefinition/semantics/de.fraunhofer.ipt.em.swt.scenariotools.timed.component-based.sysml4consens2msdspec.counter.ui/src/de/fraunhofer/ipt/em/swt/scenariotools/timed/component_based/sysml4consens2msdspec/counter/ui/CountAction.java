package de.fraunhofer.ipt.em.swt.scenariotools.timed.component_based.sysml4consens2msdspec.counter.ui;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Model;


public class CountAction implements IObjectActionDelegate {

	@Override
	public void run(IAction action) {
		// TODO Auto-generated method stub
		

		IWorkbenchPage iwPage=PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	    ISelection selection=iwPage.getSelection();
	    
	    if(selection!=null && selection instanceof IStructuredSelection)
	        {
	          IStructuredSelection selectedFileSelection = (IStructuredSelection) selection;
	          IFile modelFile = ((IFile) selectedFileSelection.getFirstElement());
	          
	          String filePath = modelFile.getFullPath().toString();
	          String resultPath = filePath.replace(".uml", "_CountModel.uml");
	          
	          Counter.main(new String[]{filePath, resultPath, action.getId()});
	        }


	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub

	}

}
