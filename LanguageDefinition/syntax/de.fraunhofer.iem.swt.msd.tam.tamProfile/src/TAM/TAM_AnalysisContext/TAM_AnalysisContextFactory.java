/**
 */
package TAM.TAM_AnalysisContext;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage
 * @generated
 */
public interface TAM_AnalysisContextFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_AnalysisContextFactory eINSTANCE = TAM.TAM_AnalysisContext.impl.TAM_AnalysisContextFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Tam Workload Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Workload Behavior</em>'.
	 * @generated
	 */
	TamWorkloadBehavior createTamWorkloadBehavior();

	/**
	 * Returns a new object of class '<em>Tam Analysis Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Analysis Context</em>'.
	 * @generated
	 */
	TamAnalysisContext createTamAnalysisContext();

	/**
	 * Returns a new object of class '<em>Tam Resource Platform</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Resource Platform</em>'.
	 * @generated
	 */
	TamResourcePlatform createTamResourcePlatform();

	/**
	 * Returns a new object of class '<em>Tam Assumption MSD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Assumption MSD</em>'.
	 * @generated
	 */
	TamAssumptionMSD createTamAssumptionMSD();

	/**
	 * Returns a new object of class '<em>Tam Requirement MSD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tam Requirement MSD</em>'.
	 * @generated
	 */
	TamRequirementMSD createTamRequirementMSD();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TAM_AnalysisContextPackage getTAM_AnalysisContextPackage();

} //TAM_AnalysisContextFactory
