/**
 */
package TAM.TAM_AnalysisContext;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaWorkloadBehavior;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Workload Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamWorkloadBehavior()
 * @model
 * @generated
 */
public interface TamWorkloadBehavior extends GaWorkloadBehavior {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * demand->forAll(d | d.isStereotypeApplied(
	 * TAM::TAM_AnalysisContext::TamAssumptionMSD))
	 * and behavior->forAll(b | b.isStereotypeApplied(
	 * TAM::TAM_AnalysisContext::TamRequirementMSD))
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	boolean Constraint1(DiagnosticChain diagnostics, Map<Object, Object> context);

} // TamWorkloadBehavior
