/**
 */
package TAM.TAM_Platform.TAM_OperatingSystem;

import TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see TAM.TAM_Platform.TAM_OperatingSystem.TAM_OperatingSystemFactory
 * @model kind="package"
 * @generated
 */
public interface TAM_OperatingSystemPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TAM_OperatingSystem";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///TAM/TAM_Platform/TAM_OperatingSystem.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "TAM.TAM_Platform.TAM_OperatingSystem";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_OperatingSystemPackage eINSTANCE = TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl.init();

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSResourceImpl <em>Tam OS Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSResourceImpl
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamOSResource()
	 * @generated
	 */
	int TAM_OS_RESOURCE = 0;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__RES_MULT = GRMPackage.RESOURCE__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__IS_PROTECTED = GRMPackage.RESOURCE__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__IS_ACTIVE = GRMPackage.RESOURCE__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__BASE_PROPERTY = GRMPackage.RESOURCE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__BASE_INSTANCE_SPECIFICATION = GRMPackage.RESOURCE__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__BASE_CLASSIFIER = GRMPackage.RESOURCE__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__BASE_LIFELINE = GRMPackage.RESOURCE__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__BASE_CONNECTABLE_ELEMENT = GRMPackage.RESOURCE__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Mem Footprint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__MEM_FOOTPRINT = GRMPackage.RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Background Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE__BACKGROUND_UTILIZATION = GRMPackage.RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam OS Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE_FEATURE_COUNT = GRMPackage.RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tam OS Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_RESOURCE_OPERATION_COUNT = GRMPackage.RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSchedulerImpl <em>Tam Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSchedulerImpl
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamScheduler()
	 * @generated
	 */
	int TAM_SCHEDULER = 1;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__RES_MULT = GRMPackage.SCHEDULER__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__IS_PROTECTED = GRMPackage.SCHEDULER__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__IS_ACTIVE = GRMPackage.SCHEDULER__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__BASE_PROPERTY = GRMPackage.SCHEDULER__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__BASE_INSTANCE_SPECIFICATION = GRMPackage.SCHEDULER__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__BASE_CLASSIFIER = GRMPackage.SCHEDULER__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__BASE_LIFELINE = GRMPackage.SCHEDULER__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__BASE_CONNECTABLE_ELEMENT = GRMPackage.SCHEDULER__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Preemptible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__IS_PREEMPTIBLE = GRMPackage.SCHEDULER__IS_PREEMPTIBLE;

	/**
	 * The feature id for the '<em><b>Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__SCHED_POLICY = GRMPackage.SCHEDULER__SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Other Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__OTHER_SCHED_POLICY = GRMPackage.SCHEDULER__OTHER_SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__SCHEDULE = GRMPackage.SCHEDULER__SCHEDULE;

	/**
	 * The feature id for the '<em><b>Processing Units</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__PROCESSING_UNITS = GRMPackage.SCHEDULER__PROCESSING_UNITS;

	/**
	 * The feature id for the '<em><b>Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__HOST = GRMPackage.SCHEDULER__HOST;

	/**
	 * The feature id for the '<em><b>Protected Shared Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__PROTECTED_SHARED_RESOURCES = GRMPackage.SCHEDULER__PROTECTED_SHARED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Schedulable Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__SCHEDULABLE_RESOURCES = GRMPackage.SCHEDULER__SCHEDULABLE_RESOURCES;

	/**
	 * The feature id for the '<em><b>Mem Footprint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__MEM_FOOTPRINT = GRMPackage.SCHEDULER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Background Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__BACKGROUND_UTILIZATION = GRMPackage.SCHEDULER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Task Switch T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER__TASK_SWITCH_T = GRMPackage.SCHEDULER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Tam Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER_FEATURE_COUNT = GRMPackage.SCHEDULER_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Tam Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SCHEDULER_OPERATION_COUNT = GRMPackage.SCHEDULER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSComChannelImpl <em>Tam OS Com Channel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSComChannelImpl
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamOSComChannel()
	 * @generated
	 */
	int TAM_OS_COM_CHANNEL = 2;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__RES_MULT = TAM_CommunicationPackage.TAM_COM_CONNECTION__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__IS_PROTECTED = TAM_CommunicationPackage.TAM_COM_CONNECTION__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__IS_ACTIVE = TAM_CommunicationPackage.TAM_COM_CONNECTION__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BASE_PROPERTY = TAM_CommunicationPackage.TAM_COM_CONNECTION__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BASE_INSTANCE_SPECIFICATION = TAM_CommunicationPackage.TAM_COM_CONNECTION__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BASE_CLASSIFIER = TAM_CommunicationPackage.TAM_COM_CONNECTION__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BASE_LIFELINE = TAM_CommunicationPackage.TAM_COM_CONNECTION__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BASE_CONNECTABLE_ELEMENT = TAM_CommunicationPackage.TAM_COM_CONNECTION__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__SPEED_FACTOR = TAM_CommunicationPackage.TAM_COM_CONNECTION__SPEED_FACTOR;

	/**
	 * The feature id for the '<em><b>Main Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__MAIN_SCHEDULER = TAM_CommunicationPackage.TAM_COM_CONNECTION__MAIN_SCHEDULER;

	/**
	 * The feature id for the '<em><b>Element Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__ELEMENT_SIZE = TAM_CommunicationPackage.TAM_COM_CONNECTION__ELEMENT_SIZE;

	/**
	 * The feature id for the '<em><b>Base Connector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BASE_CONNECTOR = TAM_CommunicationPackage.TAM_COM_CONNECTION__BASE_CONNECTOR;

	/**
	 * The feature id for the '<em><b>Transm Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__TRANSM_MODE = TAM_CommunicationPackage.TAM_COM_CONNECTION__TRANSM_MODE;

	/**
	 * The feature id for the '<em><b>Block T</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BLOCK_T = TAM_CommunicationPackage.TAM_COM_CONNECTION__BLOCK_T;

	/**
	 * The feature id for the '<em><b>Packet T</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__PACKET_T = TAM_CommunicationPackage.TAM_COM_CONNECTION__PACKET_T;

	/**
	 * The feature id for the '<em><b>Capacity</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__CAPACITY = TAM_CommunicationPackage.TAM_COM_CONNECTION__CAPACITY;

	/**
	 * The feature id for the '<em><b>Is Preemptible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__IS_PREEMPTIBLE = TAM_CommunicationPackage.TAM_COM_CONNECTION__IS_PREEMPTIBLE;

	/**
	 * The feature id for the '<em><b>Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__SCHED_POLICY = TAM_CommunicationPackage.TAM_COM_CONNECTION__SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Other Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__OTHER_SCHED_POLICY = TAM_CommunicationPackage.TAM_COM_CONNECTION__OTHER_SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__SCHEDULE = TAM_CommunicationPackage.TAM_COM_CONNECTION__SCHEDULE;

	/**
	 * The feature id for the '<em><b>Processing Units</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__PROCESSING_UNITS = TAM_CommunicationPackage.TAM_COM_CONNECTION__PROCESSING_UNITS;

	/**
	 * The feature id for the '<em><b>Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__HOST = TAM_CommunicationPackage.TAM_COM_CONNECTION__HOST;

	/**
	 * The feature id for the '<em><b>Protected Shared Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__PROTECTED_SHARED_RESOURCES = TAM_CommunicationPackage.TAM_COM_CONNECTION__PROTECTED_SHARED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Schedulable Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__SCHEDULABLE_RESOURCES = TAM_CommunicationPackage.TAM_COM_CONNECTION__SCHEDULABLE_RESOURCES;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__THROUGHPUT = TAM_CommunicationPackage.TAM_COM_CONNECTION__THROUGHPUT;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__UTILIZATION = TAM_CommunicationPackage.TAM_COM_CONNECTION__UTILIZATION;

	/**
	 * The feature id for the '<em><b>Network Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__NETWORK_OVHD = TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_OVHD;

	/**
	 * The feature id for the '<em><b>Network Block T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__NETWORK_BLOCK_T = TAM_CommunicationPackage.TAM_COM_CONNECTION__NETWORK_BLOCK_T;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__INTERFACES = TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACES;

	/**
	 * The feature id for the '<em><b>Used Protocol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__USED_PROTOCOL = TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_PROTOCOL;

	/**
	 * The feature id for the '<em><b>Used Com Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__USED_COM_SERVICES = TAM_CommunicationPackage.TAM_COM_CONNECTION__USED_COM_SERVICES;

	/**
	 * The feature id for the '<em><b>Interface1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__INTERFACE1 = TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE1;

	/**
	 * The feature id for the '<em><b>Interface2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__INTERFACE2 = TAM_CommunicationPackage.TAM_COM_CONNECTION__INTERFACE2;

	/**
	 * The feature id for the '<em><b>Mem Footprint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__MEM_FOOTPRINT = TAM_CommunicationPackage.TAM_COM_CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Background Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL__BACKGROUND_UTILIZATION = TAM_CommunicationPackage.TAM_COM_CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam OS Com Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL_FEATURE_COUNT = TAM_CommunicationPackage.TAM_COM_CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tam OS Com Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_COM_CHANNEL_OPERATION_COUNT = TAM_CommunicationPackage.TAM_COM_CONNECTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl <em>Tam Shared OS Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamSharedOSResource()
	 * @generated
	 */
	int TAM_SHARED_OS_RESOURCE = 3;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__RES_MULT = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__IS_PROTECTED = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__IS_ACTIVE = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__BASE_PROPERTY = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__BASE_INSTANCE_SPECIFICATION = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__BASE_CLASSIFIER = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__BASE_LIFELINE = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__BASE_CONNECTABLE_ELEMENT = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Protect Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__PROTECT_KIND = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__PROTECT_KIND;

	/**
	 * The feature id for the '<em><b>Ceiling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__CEILING = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__CEILING;

	/**
	 * The feature id for the '<em><b>Other Protect Protocol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__OTHER_PROTECT_PROTOCOL = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__OTHER_PROTECT_PROTOCOL;

	/**
	 * The feature id for the '<em><b>Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__SCHEDULER = GRMPackage.MUTUAL_EXCLUSION_RESOURCE__SCHEDULER;

	/**
	 * The feature id for the '<em><b>Mem Footprint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__MEM_FOOTPRINT = GRMPackage.MUTUAL_EXCLUSION_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Background Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__BACKGROUND_UTILIZATION = GRMPackage.MUTUAL_EXCLUSION_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Access Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__ACCESS_DELAY = GRMPackage.MUTUAL_EXCLUSION_RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE__TYPE = GRMPackage.MUTUAL_EXCLUSION_RESOURCE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Tam Shared OS Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE_FEATURE_COUNT = GRMPackage.MUTUAL_EXCLUSION_RESOURCE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Tam Shared OS Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURCE_OPERATION_COUNT = GRMPackage.MUTUAL_EXCLUSION_RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSServiceImpl <em>Tam OS Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSServiceImpl
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamOSService()
	 * @generated
	 */
	int TAM_OS_SERVICE = 4;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__RES_MULT = TAM_OS_RESOURCE__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__IS_PROTECTED = TAM_OS_RESOURCE__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__IS_ACTIVE = TAM_OS_RESOURCE__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__BASE_PROPERTY = TAM_OS_RESOURCE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__BASE_INSTANCE_SPECIFICATION = TAM_OS_RESOURCE__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__BASE_CLASSIFIER = TAM_OS_RESOURCE__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__BASE_LIFELINE = TAM_OS_RESOURCE__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__BASE_CONNECTABLE_ELEMENT = TAM_OS_RESOURCE__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Mem Footprint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__MEM_FOOTPRINT = TAM_OS_RESOURCE__MEM_FOOTPRINT;

	/**
	 * The feature id for the '<em><b>Background Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE__BACKGROUND_UTILIZATION = TAM_OS_RESOURCE__BACKGROUND_UTILIZATION;

	/**
	 * The number of structural features of the '<em>Tam OS Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE_FEATURE_COUNT = TAM_OS_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam OS Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OS_SERVICE_OPERATION_COUNT = TAM_OS_RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl <em>Tam RTOS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamRTOS()
	 * @generated
	 */
	int TAM_RTOS = 5;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__RES_MULT = TAM_OS_RESOURCE__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__IS_PROTECTED = TAM_OS_RESOURCE__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__IS_ACTIVE = TAM_OS_RESOURCE__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__BASE_PROPERTY = TAM_OS_RESOURCE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__BASE_INSTANCE_SPECIFICATION = TAM_OS_RESOURCE__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__BASE_CLASSIFIER = TAM_OS_RESOURCE__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__BASE_LIFELINE = TAM_OS_RESOURCE__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__BASE_CONNECTABLE_ELEMENT = TAM_OS_RESOURCE__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Mem Footprint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__MEM_FOOTPRINT = TAM_OS_RESOURCE__MEM_FOOTPRINT;

	/**
	 * The feature id for the '<em><b>Background Utilization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__BACKGROUND_UTILIZATION = TAM_OS_RESOURCE__BACKGROUND_UTILIZATION;

	/**
	 * The feature id for the '<em><b>Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__SCHEDULER = TAM_OS_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Com Channels</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__COM_CHANNELS = TAM_OS_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Shared Res</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__SHARED_RES = TAM_OS_RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Os Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS__OS_SERVICES = TAM_OS_RESOURCE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Tam RTOS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS_FEATURE_COUNT = TAM_OS_RESOURCE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Tam RTOS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RTOS_OPERATION_COUNT = TAM_OS_RESOURCE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource <em>Tam OS Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam OS Resource</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource
	 * @generated
	 */
	EClass getTamOSResource();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getMemFootprint <em>Mem Footprint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mem Footprint</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getMemFootprint()
	 * @see #getTamOSResource()
	 * @generated
	 */
	EAttribute getTamOSResource_MemFootprint();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getBackgroundUtilization <em>Background Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Background Utilization</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamOSResource#getBackgroundUtilization()
	 * @see #getTamOSResource()
	 * @generated
	 */
	EAttribute getTamOSResource_BackgroundUtilization();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler <em>Tam Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Scheduler</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler
	 * @generated
	 */
	EClass getTamScheduler();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler#getTaskSwitchT <em>Task Switch T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Task Switch T</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamScheduler#getTaskSwitchT()
	 * @see #getTamScheduler()
	 * @generated
	 */
	EAttribute getTamScheduler_TaskSwitchT();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSComChannel <em>Tam OS Com Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam OS Com Channel</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamOSComChannel
	 * @generated
	 */
	EClass getTamOSComChannel();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource <em>Tam Shared OS Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Shared OS Resource</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource
	 * @generated
	 */
	EClass getTamSharedOSResource();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getAccessDelay <em>Access Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Access Delay</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getAccessDelay()
	 * @see #getTamSharedOSResource()
	 * @generated
	 */
	EAttribute getTamSharedOSResource_AccessDelay();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamSharedOSResource#getType()
	 * @see #getTamSharedOSResource()
	 * @generated
	 */
	EReference getTamSharedOSResource_Type();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamOSService <em>Tam OS Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam OS Service</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamOSService
	 * @generated
	 */
	EClass getTamOSService();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS <em>Tam RTOS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam RTOS</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS
	 * @generated
	 */
	EClass getTamRTOS();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getScheduler <em>Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scheduler</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getScheduler()
	 * @see #getTamRTOS()
	 * @generated
	 */
	EReference getTamRTOS_Scheduler();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getComChannels <em>Com Channels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Com Channels</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getComChannels()
	 * @see #getTamRTOS()
	 * @generated
	 */
	EReference getTamRTOS_ComChannels();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getSharedRes <em>Shared Res</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Shared Res</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getSharedRes()
	 * @see #getTamRTOS()
	 * @generated
	 */
	EReference getTamRTOS_SharedRes();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getOsServices <em>Os Services</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Os Services</em>'.
	 * @see TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS#getOsServices()
	 * @see #getTamRTOS()
	 * @generated
	 */
	EReference getTamRTOS_OsServices();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TAM_OperatingSystemFactory getTAM_OperatingSystemFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSResourceImpl <em>Tam OS Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSResourceImpl
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamOSResource()
		 * @generated
		 */
		EClass TAM_OS_RESOURCE = eINSTANCE.getTamOSResource();

		/**
		 * The meta object literal for the '<em><b>Mem Footprint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_OS_RESOURCE__MEM_FOOTPRINT = eINSTANCE.getTamOSResource_MemFootprint();

		/**
		 * The meta object literal for the '<em><b>Background Utilization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_OS_RESOURCE__BACKGROUND_UTILIZATION = eINSTANCE.getTamOSResource_BackgroundUtilization();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSchedulerImpl <em>Tam Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSchedulerImpl
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamScheduler()
		 * @generated
		 */
		EClass TAM_SCHEDULER = eINSTANCE.getTamScheduler();

		/**
		 * The meta object literal for the '<em><b>Task Switch T</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_SCHEDULER__TASK_SWITCH_T = eINSTANCE.getTamScheduler_TaskSwitchT();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSComChannelImpl <em>Tam OS Com Channel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSComChannelImpl
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamOSComChannel()
		 * @generated
		 */
		EClass TAM_OS_COM_CHANNEL = eINSTANCE.getTamOSComChannel();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl <em>Tam Shared OS Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamSharedOSResourceImpl
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamSharedOSResource()
		 * @generated
		 */
		EClass TAM_SHARED_OS_RESOURCE = eINSTANCE.getTamSharedOSResource();

		/**
		 * The meta object literal for the '<em><b>Access Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_SHARED_OS_RESOURCE__ACCESS_DELAY = eINSTANCE.getTamSharedOSResource_AccessDelay();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_SHARED_OS_RESOURCE__TYPE = eINSTANCE.getTamSharedOSResource_Type();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSServiceImpl <em>Tam OS Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamOSServiceImpl
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamOSService()
		 * @generated
		 */
		EClass TAM_OS_SERVICE = eINSTANCE.getTamOSService();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl <em>Tam RTOS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TamRTOSImpl
		 * @see TAM.TAM_Platform.TAM_OperatingSystem.impl.TAM_OperatingSystemPackageImpl#getTamRTOS()
		 * @generated
		 */
		EClass TAM_RTOS = eINSTANCE.getTamRTOS();

		/**
		 * The meta object literal for the '<em><b>Scheduler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RTOS__SCHEDULER = eINSTANCE.getTamRTOS_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Com Channels</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RTOS__COM_CHANNELS = eINSTANCE.getTamRTOS_ComChannels();

		/**
		 * The meta object literal for the '<em><b>Shared Res</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RTOS__SHARED_RES = eINSTANCE.getTamRTOS_SharedRes();

		/**
		 * The meta object literal for the '<em><b>Os Services</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RTOS__OS_SERVICES = eINSTANCE.getTamRTOS_OsServices();

	}

} //TAM_OperatingSystemPackage
