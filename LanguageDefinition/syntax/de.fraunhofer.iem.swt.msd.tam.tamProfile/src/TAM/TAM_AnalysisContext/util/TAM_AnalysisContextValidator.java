/**
 */
package TAM.TAM_AnalysisContext.util;

import TAM.TAM_AnalysisContext.*;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage
 * @generated
 */
public class TAM_AnalysisContextValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final TAM_AnalysisContextValidator INSTANCE = new TAM_AnalysisContextValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "TAM.TAM_AnalysisContext";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Constraint1' of 'Tam Workload Behavior'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TAM_WORKLOAD_BEHAVIOR__CONSTRAINT1 = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Constraint1' of 'Tam Analysis Context'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TAM_ANALYSIS_CONTEXT__CONSTRAINT1 = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TAM_AnalysisContextValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return TAM_AnalysisContextPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case TAM_AnalysisContextPackage.TAM_WORKLOAD_BEHAVIOR:
				return validateTamWorkloadBehavior((TamWorkloadBehavior)value, diagnostics, context);
			case TAM_AnalysisContextPackage.TAM_ANALYSIS_CONTEXT:
				return validateTamAnalysisContext((TamAnalysisContext)value, diagnostics, context);
			case TAM_AnalysisContextPackage.TAM_RESOURCE_PLATFORM:
				return validateTamResourcePlatform((TamResourcePlatform)value, diagnostics, context);
			case TAM_AnalysisContextPackage.TAM_ASSUMPTION_MSD:
				return validateTamAssumptionMSD((TamAssumptionMSD)value, diagnostics, context);
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD:
				return validateTamRequirementMSD((TamRequirementMSD)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTamWorkloadBehavior(TamWorkloadBehavior tamWorkloadBehavior, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tamWorkloadBehavior, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tamWorkloadBehavior, diagnostics, context);
		if (result || diagnostics != null) result &= validateTamWorkloadBehavior_Constraint1(tamWorkloadBehavior, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Constraint1 constraint of '<em>Tam Workload Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTamWorkloadBehavior_Constraint1(TamWorkloadBehavior tamWorkloadBehavior, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tamWorkloadBehavior.Constraint1(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTamAnalysisContext(TamAnalysisContext tamAnalysisContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tamAnalysisContext, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tamAnalysisContext, diagnostics, context);
		if (result || diagnostics != null) result &= validateTamAnalysisContext_Constraint1(tamAnalysisContext, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Constraint1 constraint of '<em>Tam Analysis Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTamAnalysisContext_Constraint1(TamAnalysisContext tamAnalysisContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tamAnalysisContext.Constraint1(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTamResourcePlatform(TamResourcePlatform tamResourcePlatform, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(tamResourcePlatform, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTamAssumptionMSD(TamAssumptionMSD tamAssumptionMSD, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(tamAssumptionMSD, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTamRequirementMSD(TamRequirementMSD tamRequirementMSD, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(tamRequirementMSD, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //TAM_AnalysisContextValidator
