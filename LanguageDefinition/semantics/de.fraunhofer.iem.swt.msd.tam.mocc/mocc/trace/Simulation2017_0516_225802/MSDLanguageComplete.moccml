AutomataConstraintLibrary MSDLanguage{ 
		import "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib" as kernel; 
		import "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib" as ccslLib;

		RelationLibrary MSDMessages_status{
			AutomataRelationDefinition  messageStatusDef[messageSatus]{
				init: ms_initialState

			from ms_initialState to ms_notSent : ms_initialState2notSent
					-> ( )
				
				from ms_notSent to ms_msgSent : ms_notSent2msgSent
					-> ( when ms_msgSend )
				
				from ms_taskStarted to ms_notSent : ms_taskStarted2notSent
					-> ( when ms_taskComplete )
					
				from ms_msgSent to ms_msgReceived : ms_msgSent2msgReceived
					-> ( when ms_msgReceive )
					
				from ms_msgReceived to ms_msgConsumed : ms_msgReceived2msgConsumed
					-> ( when ms_msgConsume )
					
				from ms_msgConsumed to ms_taskStarted : ms_msgConsumed2taskStarted
					-> ( when ms_taskStart )

			State ms_initialState(
					out: ms_initialState2notSent
				)
				
				State ms_notSent (
					in: ms_initialState2notSent, ms_taskStarted2notSent
					out: ms_notSent2msgSent
				)
				
				State ms_msgSent (
					in: ms_notSent2msgSent
					out: ms_msgSent2msgReceived
				)
				
				State ms_msgReceived (
					in: ms_msgSent2msgReceived
					out: ms_msgReceived2msgConsumed
				)
				
				State ms_msgConsumed (
					in: ms_msgReceived2msgConsumed
					out: ms_msgConsumed2taskStarted
				)
				
				State ms_taskStarted (
					in: ms_msgConsumed2taskStarted
					out: ms_taskStarted2notSent
				)
		}

		AutomataRelationDefinition  messageRelationDef[messageRelation]{
				init: mr_InitialState

			from mr_InitialState to MsgNotActive : mr_initialize
					-> ()
				
				from MsgNotActive to MsgActive : activate
					-> ( when mr_preceedingTaskFinish )
				
				from MsgActive to MsgNotActive : deactivate
					-> ( when mr_msgSend )

			State mr_InitialState(
					out:mr_initialize
				)
				
				State MsgNotActive(
					in: mr_initialize, deactivate
					out: activate
				)
				
				State MsgActive (
					in: activate
					out: deactivate
				)
		}

		AutomataRelationDefinition messageUnificationDef[messageUnification]{
				
				init: mu_initialState
				
				
				from mu_initialState to mu_not_active : mu_initialStateToidle
				-> ( )
				
	
				from mu_not_active to mu_active : mu_not_activeToactive
				-> ( when mu_activate )
	
				from mu_active to mu_not_active : mu_activeTonot_active
				-> (when mu_signatureActivate, mu_sendClock)
				
				from mu_not_active to mu_not_active : mu_not_active2not_active
				-> (when mu_signatureActivate)
				
	
				State mu_initialState( out : mu_initialStateToidle )
				
				State mu_not_active( in : mu_initialStateToidle, mu_activeTonot_active, mu_not_active2not_active out : mu_not_activeToactive, mu_not_active2not_active )
				
				State mu_active( in : mu_not_activeToactive out : mu_activeTonot_active )
	
				
				
				
			}

		AutomataRelationDefinition resourceAccessDef[resourceAccess]{
				init: ra_Init
				
				from ra_Init to ra_Free : ra_initialize
				-> ()
				
				from ra_Free to ra_Locked : ra_lockEvt
				-> (when ra_lock, ra_exclusiveAccess)
				
				from ra_Locked to ra_Free : ra_unlockEvt
				-> (when ra_unlock)
				
				State ra_Init(
					out: ra_initialize
				)	
				
				State ra_Free(
					in: ra_initialize, ra_unlockEvt
					out: ra_lockEvt
				)
				
				State ra_Locked(
					in: ra_lockEvt
					out: ra_unlockEvt
				)
			}

		AutomataRelationDefinition fixedPrioritySchedCPUDef[fixedPrioritySchedCPU]{
				variables {
					Integer runningTasks = 0
					Integer runningTasksPlusOne = 1
				}
					
				init: schd_Init

			from schd_Init to schd_CoreAvailabe : schd_initialize
				-> ()
				
				from schd_CoreAvailabe to schd_CoreAvailabe : schd_newTaskNotLast
				-> (when schd_newTask 
					if(runningTasksPlusOne < schd_numCores.value) 
					do runningTasks=runningTasks+=one
					do runningTasksPlusOne=runningTasksPlusOne+=one
				)
				
				from schd_CoreAvailabe to schd_NoCoresAvailable : schd_newTaskLast
				-> (when schd_newTask, schd_dispatch
					if(runningTasksPlusOne == schd_numCores.value)
				)
				
				from schd_NoCoresAvailable to schd_CoreAvailabe : schd_taskFinishLast
				-> (when schd_taskFinish)
				
				from schd_CoreAvailabe to schd_CoreAvailabe : schd_taskFinishNotLast
				-> (when schd_newTask , schd_dispatch
					do runningTasks=runningTasks-=one
					do runningTasksPlusOne=runningTasksPlusOne-=one
				)

			State schd_Init(
					out: schd_initialize
				)
				
				State schd_CoreAvailabe(
					in: schd_initialize,schd_taskFinishLast, schd_newTaskNotLast, schd_taskFinishNotLast
					out: schd_newTaskNotLast, schd_newTaskLast, schd_taskFinishNotLast
				)
				
				State schd_NoCoresAvailable(
					in: schd_newTaskLast
					out: schd_taskFinishLast
				)
		}

		AutomataRelationDefinition fixedPrioritySchedTaskDef[fixedPrioritySchedTask]{
				init: schdt_Init
				
				from schdt_Init to schdt_Idle : schdt_initialize
				->()
				
				State schdt_Init(
					out: schdt_initialize
				)
				
				State schdt_Idle(
					in:schdt_initialize
				)
				
			}
		RelationDeclaration messageSatus(ms_msgSend:clock, ms_msgReceive:clock, ms_msgConsume:clock, ms_taskStart:clock, ms_taskComplete:clock)
		RelationDeclaration messageRelation(mr_preceedingTaskFinish:clock, mr_msgSend:clock)
		RelationDeclaration messageUnification(mu_activate:clock, mu_signatureActivate:clock, mu_sendClock:clock )
		RelationDeclaration resourceAccess(ra_exclusiveAccess:clock, ra_lock:clock, ra_unlock:clock)
		RelationDeclaration fixedPrioritySchedCPU(schd_dispatch:clock, schd_newTask:clock, schd_taskFinish:clock, schd_numCores:int)
		RelationDeclaration fixedPrioritySchedTask(schd_idle:clock, schd_msgConsume:clock)
			
		}

}