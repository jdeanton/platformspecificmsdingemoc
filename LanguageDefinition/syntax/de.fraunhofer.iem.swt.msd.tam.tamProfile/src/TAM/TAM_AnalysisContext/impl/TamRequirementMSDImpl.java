/**
 */
package TAM.TAM_AnalysisContext.impl;

import TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage;
import TAM.TAM_AnalysisContext.TamRequirementMSD;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaScenarioImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Requirement MSD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_AnalysisContext.impl.TamRequirementMSDImpl#getActivationCount <em>Activation Count</em>}</li>
 *   <li>{@link TAM.TAM_AnalysisContext.impl.TamRequirementMSDImpl#getConstraintsSatisfied <em>Constraints Satisfied</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamRequirementMSDImpl extends GaScenarioImpl implements TamRequirementMSD {
	/**
	 * The default value of the '{@link #getActivationCount() <em>Activation Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivationCount()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTIVATION_COUNT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActivationCount() <em>Activation Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivationCount()
	 * @generated
	 * @ordered
	 */
	protected String activationCount = ACTIVATION_COUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getConstraintsSatisfied() <em>Constraints Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintsSatisfied()
	 * @generated
	 * @ordered
	 */
	protected static final String CONSTRAINTS_SATISFIED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConstraintsSatisfied() <em>Constraints Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintsSatisfied()
	 * @generated
	 * @ordered
	 */
	protected String constraintsSatisfied = CONSTRAINTS_SATISFIED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamRequirementMSDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_AnalysisContextPackage.Literals.TAM_REQUIREMENT_MSD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getActivationCount() {
		return activationCount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivationCount(String newActivationCount) {
		String oldActivationCount = activationCount;
		activationCount = newActivationCount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__ACTIVATION_COUNT, oldActivationCount, activationCount));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConstraintsSatisfied() {
		return constraintsSatisfied;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraintsSatisfied(String newConstraintsSatisfied) {
		String oldConstraintsSatisfied = constraintsSatisfied;
		constraintsSatisfied = newConstraintsSatisfied;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED, oldConstraintsSatisfied, constraintsSatisfied));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__ACTIVATION_COUNT:
				return getActivationCount();
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED:
				return getConstraintsSatisfied();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__ACTIVATION_COUNT:
				setActivationCount((String)newValue);
				return;
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED:
				setConstraintsSatisfied((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__ACTIVATION_COUNT:
				setActivationCount(ACTIVATION_COUNT_EDEFAULT);
				return;
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED:
				setConstraintsSatisfied(CONSTRAINTS_SATISFIED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__ACTIVATION_COUNT:
				return ACTIVATION_COUNT_EDEFAULT == null ? activationCount != null : !ACTIVATION_COUNT_EDEFAULT.equals(activationCount);
			case TAM_AnalysisContextPackage.TAM_REQUIREMENT_MSD__CONSTRAINTS_SATISFIED:
				return CONSTRAINTS_SATISFIED_EDEFAULT == null ? constraintsSatisfied != null : !CONSTRAINTS_SATISFIED_EDEFAULT.equals(constraintsSatisfied);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (activationCount: ");
		result.append(activationCount);
		result.append(", constraintsSatisfied: ");
		result.append(constraintsSatisfied);
		result.append(')');
		return result.toString();
	}

} //TamRequirementMSDImpl
