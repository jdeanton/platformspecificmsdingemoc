/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;

import TAM.TAM_Platform.TAM_Communication.TamComInterface;

import TAM.TAM_Platform.TAM_OperatingSystem.TamRTOS;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam ECU</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getProcessingUnit <em>Processing Unit</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getMemory <em>Memory</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getNetworkInterfaces <em>Network Interfaces</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getPeripherals <em>Peripherals</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getRTOS <em>RTOS</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamECU()
 * @model
 * @generated
 */
public interface TamECU extends Resource {
	/**
	 * Returns the value of the '<em><b>Processing Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processing Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processing Unit</em>' reference.
	 * @see #setProcessingUnit(TamProcessingUnit)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamECU_ProcessingUnit()
	 * @model ordered="false"
	 * @generated
	 */
	TamProcessingUnit getProcessingUnit();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getProcessingUnit <em>Processing Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processing Unit</em>' reference.
	 * @see #getProcessingUnit()
	 * @generated
	 */
	void setProcessingUnit(TamProcessingUnit value);

	/**
	 * Returns the value of the '<em><b>Memory</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamECU_Memory()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamMemoryUnit> getMemory();

	/**
	 * Returns the value of the '<em><b>Network Interfaces</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_Communication.TamComInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Network Interfaces</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Network Interfaces</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamECU_NetworkInterfaces()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamComInterface> getNetworkInterfaces();

	/**
	 * Returns the value of the '<em><b>Peripherals</b></em>' reference list.
	 * The list contents are of type {@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripherals</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Peripherals</em>' reference list.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamECU_Peripherals()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TamPeripheryUnit> getPeripherals();

	/**
	 * Returns the value of the '<em><b>RTOS</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>RTOS</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RTOS</em>' reference.
	 * @see #setRTOS(TamRTOS)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamECU_RTOS()
	 * @model ordered="false"
	 * @generated
	 */
	TamRTOS getRTOS();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getRTOS <em>RTOS</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RTOS</em>' reference.
	 * @see #getRTOS()
	 * @generated
	 */
	void setRTOS(TamRTOS value);

} // TamECU
