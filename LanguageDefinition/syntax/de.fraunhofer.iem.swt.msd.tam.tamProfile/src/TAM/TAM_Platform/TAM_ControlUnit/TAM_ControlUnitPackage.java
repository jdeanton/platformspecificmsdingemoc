/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitFactory
 * @model kind="package"
 * @generated
 */
public interface TAM_ControlUnitPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TAM_ControlUnit";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///TAM/TAM_Platform/TAM_ControlUnit.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "TAM.TAM_Platform.TAM_ControlUnit";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_ControlUnitPackage eINSTANCE = TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl.init();

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl <em>Tam Memory Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamMemoryUnit()
	 * @generated
	 */
	int TAM_MEMORY_UNIT = 0;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__RES_MULT = GRMPackage.STORAGE_RESOURCE__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__IS_PROTECTED = GRMPackage.STORAGE_RESOURCE__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__IS_ACTIVE = GRMPackage.STORAGE_RESOURCE__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__BASE_PROPERTY = GRMPackage.STORAGE_RESOURCE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__BASE_INSTANCE_SPECIFICATION = GRMPackage.STORAGE_RESOURCE__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__BASE_CLASSIFIER = GRMPackage.STORAGE_RESOURCE__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__BASE_LIFELINE = GRMPackage.STORAGE_RESOURCE__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__BASE_CONNECTABLE_ELEMENT = GRMPackage.STORAGE_RESOURCE__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Element Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__ELEMENT_SIZE = GRMPackage.STORAGE_RESOURCE__ELEMENT_SIZE;

	/**
	 * The feature id for the '<em><b>Memory Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__MEMORY_SIZE = GRMPackage.STORAGE_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__THROUGHPUT = GRMPackage.STORAGE_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__TYPE = GRMPackage.STORAGE_RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Access Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT__ACCESS_DELAY = GRMPackage.STORAGE_RESOURCE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Tam Memory Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT_FEATURE_COUNT = GRMPackage.STORAGE_RESOURCE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Tam Memory Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEMORY_UNIT_OPERATION_COUNT = GRMPackage.STORAGE_RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamProcessingUnitImpl <em>Tam Processing Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamProcessingUnitImpl
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamProcessingUnit()
	 * @generated
	 */
	int TAM_PROCESSING_UNIT = 1;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__RES_MULT = GQAMPackage.GA_EXEC_HOST__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__IS_PROTECTED = GQAMPackage.GA_EXEC_HOST__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__IS_ACTIVE = GQAMPackage.GA_EXEC_HOST__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__BASE_PROPERTY = GQAMPackage.GA_EXEC_HOST__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__BASE_INSTANCE_SPECIFICATION = GQAMPackage.GA_EXEC_HOST__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__BASE_CLASSIFIER = GQAMPackage.GA_EXEC_HOST__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__BASE_LIFELINE = GQAMPackage.GA_EXEC_HOST__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__BASE_CONNECTABLE_ELEMENT = GQAMPackage.GA_EXEC_HOST__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Is Preemptible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__IS_PREEMPTIBLE = GQAMPackage.GA_EXEC_HOST__IS_PREEMPTIBLE;

	/**
	 * The feature id for the '<em><b>Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__SCHED_POLICY = GQAMPackage.GA_EXEC_HOST__SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Other Sched Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__OTHER_SCHED_POLICY = GQAMPackage.GA_EXEC_HOST__OTHER_SCHED_POLICY;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__SCHEDULE = GQAMPackage.GA_EXEC_HOST__SCHEDULE;

	/**
	 * The feature id for the '<em><b>Processing Units</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__PROCESSING_UNITS = GQAMPackage.GA_EXEC_HOST__PROCESSING_UNITS;

	/**
	 * The feature id for the '<em><b>Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__HOST = GQAMPackage.GA_EXEC_HOST__HOST;

	/**
	 * The feature id for the '<em><b>Protected Shared Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__PROTECTED_SHARED_RESOURCES = GQAMPackage.GA_EXEC_HOST__PROTECTED_SHARED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Schedulable Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__SCHEDULABLE_RESOURCES = GQAMPackage.GA_EXEC_HOST__SCHEDULABLE_RESOURCES;

	/**
	 * The feature id for the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__SPEED_FACTOR = GQAMPackage.GA_EXEC_HOST__SPEED_FACTOR;

	/**
	 * The feature id for the '<em><b>Main Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__MAIN_SCHEDULER = GQAMPackage.GA_EXEC_HOST__MAIN_SCHEDULER;

	/**
	 * The feature id for the '<em><b>Comm Tx Ovh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__COMM_TX_OVH = GQAMPackage.GA_EXEC_HOST__COMM_TX_OVH;

	/**
	 * The feature id for the '<em><b>Comm Rcv Ovh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__COMM_RCV_OVH = GQAMPackage.GA_EXEC_HOST__COMM_RCV_OVH;

	/**
	 * The feature id for the '<em><b>Cntxt Sw T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__CNTXT_SW_T = GQAMPackage.GA_EXEC_HOST__CNTXT_SW_T;

	/**
	 * The feature id for the '<em><b>Clock Ovh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__CLOCK_OVH = GQAMPackage.GA_EXEC_HOST__CLOCK_OVH;

	/**
	 * The feature id for the '<em><b>Sched Pri Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__SCHED_PRI_RANGE = GQAMPackage.GA_EXEC_HOST__SCHED_PRI_RANGE;

	/**
	 * The feature id for the '<em><b>Mem Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__MEM_SIZE = GQAMPackage.GA_EXEC_HOST__MEM_SIZE;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__UTILIZATION = GQAMPackage.GA_EXEC_HOST__UTILIZATION;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__THROUGHPUT = GQAMPackage.GA_EXEC_HOST__THROUGHPUT;

	/**
	 * The feature id for the '<em><b>Num Cores</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__NUM_CORES = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Core Sync Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT__CORE_SYNC_OVHD = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam Processing Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT_FEATURE_COUNT = GQAMPackage.GA_EXEC_HOST_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tam Processing Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PROCESSING_UNIT_OPERATION_COUNT = GQAMPackage.GA_EXEC_HOST_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamPeripheryUnitImpl <em>Tam Periphery Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamPeripheryUnitImpl
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamPeripheryUnit()
	 * @generated
	 */
	int TAM_PERIPHERY_UNIT = 2;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__RES_MULT = GRMPackage.RESOURCE__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__IS_PROTECTED = GRMPackage.RESOURCE__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__IS_ACTIVE = GRMPackage.RESOURCE__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__BASE_PROPERTY = GRMPackage.RESOURCE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__BASE_INSTANCE_SPECIFICATION = GRMPackage.RESOURCE__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__BASE_CLASSIFIER = GRMPackage.RESOURCE__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__BASE_LIFELINE = GRMPackage.RESOURCE__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__BASE_CONNECTABLE_ELEMENT = GRMPackage.RESOURCE__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__DELAY = GRMPackage.RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT__TYPE = GRMPackage.RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam Periphery Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT_FEATURE_COUNT = GRMPackage.RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tam Periphery Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_UNIT_OPERATION_COUNT = GRMPackage.RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamSensorImpl <em>Tam Sensor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamSensorImpl
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamSensor()
	 * @generated
	 */
	int TAM_SENSOR = 3;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__RES_MULT = TAM_PERIPHERY_UNIT__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__IS_PROTECTED = TAM_PERIPHERY_UNIT__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__IS_ACTIVE = TAM_PERIPHERY_UNIT__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__BASE_PROPERTY = TAM_PERIPHERY_UNIT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__BASE_INSTANCE_SPECIFICATION = TAM_PERIPHERY_UNIT__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__BASE_CLASSIFIER = TAM_PERIPHERY_UNIT__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__BASE_LIFELINE = TAM_PERIPHERY_UNIT__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__BASE_CONNECTABLE_ELEMENT = TAM_PERIPHERY_UNIT__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__DELAY = TAM_PERIPHERY_UNIT__DELAY;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR__TYPE = TAM_PERIPHERY_UNIT__TYPE;

	/**
	 * The number of structural features of the '<em>Tam Sensor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR_FEATURE_COUNT = TAM_PERIPHERY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam Sensor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SENSOR_OPERATION_COUNT = TAM_PERIPHERY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamIOImpl <em>Tam IO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamIOImpl
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamIO()
	 * @generated
	 */
	int TAM_IO = 4;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__RES_MULT = TAM_PERIPHERY_UNIT__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__IS_PROTECTED = TAM_PERIPHERY_UNIT__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__IS_ACTIVE = TAM_PERIPHERY_UNIT__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__BASE_PROPERTY = TAM_PERIPHERY_UNIT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__BASE_INSTANCE_SPECIFICATION = TAM_PERIPHERY_UNIT__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__BASE_CLASSIFIER = TAM_PERIPHERY_UNIT__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__BASE_LIFELINE = TAM_PERIPHERY_UNIT__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__BASE_CONNECTABLE_ELEMENT = TAM_PERIPHERY_UNIT__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__DELAY = TAM_PERIPHERY_UNIT__DELAY;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO__TYPE = TAM_PERIPHERY_UNIT__TYPE;

	/**
	 * The number of structural features of the '<em>Tam IO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO_FEATURE_COUNT = TAM_PERIPHERY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam IO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_IO_OPERATION_COUNT = TAM_PERIPHERY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamActuatorImpl <em>Tam Actuator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamActuatorImpl
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamActuator()
	 * @generated
	 */
	int TAM_ACTUATOR = 5;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__RES_MULT = TAM_PERIPHERY_UNIT__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__IS_PROTECTED = TAM_PERIPHERY_UNIT__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__IS_ACTIVE = TAM_PERIPHERY_UNIT__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__BASE_PROPERTY = TAM_PERIPHERY_UNIT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__BASE_INSTANCE_SPECIFICATION = TAM_PERIPHERY_UNIT__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__BASE_CLASSIFIER = TAM_PERIPHERY_UNIT__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__BASE_LIFELINE = TAM_PERIPHERY_UNIT__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__BASE_CONNECTABLE_ELEMENT = TAM_PERIPHERY_UNIT__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__DELAY = TAM_PERIPHERY_UNIT__DELAY;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR__TYPE = TAM_PERIPHERY_UNIT__TYPE;

	/**
	 * The number of structural features of the '<em>Tam Actuator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR_FEATURE_COUNT = TAM_PERIPHERY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Tam Actuator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ACTUATOR_OPERATION_COUNT = TAM_PERIPHERY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl <em>Tam ECU</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl
	 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamECU()
	 * @generated
	 */
	int TAM_ECU = 6;

	/**
	 * The feature id for the '<em><b>Res Mult</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__RES_MULT = GRMPackage.RESOURCE__RES_MULT;

	/**
	 * The feature id for the '<em><b>Is Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__IS_PROTECTED = GRMPackage.RESOURCE__IS_PROTECTED;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__IS_ACTIVE = GRMPackage.RESOURCE__IS_ACTIVE;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__BASE_PROPERTY = GRMPackage.RESOURCE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__BASE_INSTANCE_SPECIFICATION = GRMPackage.RESOURCE__BASE_INSTANCE_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__BASE_CLASSIFIER = GRMPackage.RESOURCE__BASE_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Base Lifeline</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__BASE_LIFELINE = GRMPackage.RESOURCE__BASE_LIFELINE;

	/**
	 * The feature id for the '<em><b>Base Connectable Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__BASE_CONNECTABLE_ELEMENT = GRMPackage.RESOURCE__BASE_CONNECTABLE_ELEMENT;

	/**
	 * The feature id for the '<em><b>Processing Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__PROCESSING_UNIT = GRMPackage.RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Memory</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__MEMORY = GRMPackage.RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Network Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__NETWORK_INTERFACES = GRMPackage.RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Peripherals</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__PERIPHERALS = GRMPackage.RESOURCE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>RTOS</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU__RTOS = GRMPackage.RESOURCE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Tam ECU</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU_FEATURE_COUNT = GRMPackage.RESOURCE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Tam ECU</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_ECU_OPERATION_COUNT = GRMPackage.RESOURCE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit <em>Tam Memory Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Memory Unit</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit
	 * @generated
	 */
	EClass getTamMemoryUnit();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getMemorySize <em>Memory Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Memory Size</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getMemorySize()
	 * @see #getTamMemoryUnit()
	 * @generated
	 */
	EAttribute getTamMemoryUnit_MemorySize();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getThroughput <em>Throughput</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Throughput</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getThroughput()
	 * @see #getTamMemoryUnit()
	 * @generated
	 */
	EAttribute getTamMemoryUnit_Throughput();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getType()
	 * @see #getTamMemoryUnit()
	 * @generated
	 */
	EReference getTamMemoryUnit_Type();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getAccessDelay <em>Access Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Access Delay</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getAccessDelay()
	 * @see #getTamMemoryUnit()
	 * @generated
	 */
	EAttribute getTamMemoryUnit_AccessDelay();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit <em>Tam Processing Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Processing Unit</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit
	 * @generated
	 */
	EClass getTamProcessingUnit();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getNumCores <em>Num Cores</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Cores</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getNumCores()
	 * @see #getTamProcessingUnit()
	 * @generated
	 */
	EAttribute getTamProcessingUnit_NumCores();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getCoreSyncOvhd <em>Core Sync Ovhd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Core Sync Ovhd</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamProcessingUnit#getCoreSyncOvhd()
	 * @see #getTamProcessingUnit()
	 * @generated
	 */
	EAttribute getTamProcessingUnit_CoreSyncOvhd();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit <em>Tam Periphery Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Periphery Unit</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit
	 * @generated
	 */
	EClass getTamPeripheryUnit();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getDelay()
	 * @see #getTamPeripheryUnit()
	 * @generated
	 */
	EAttribute getTamPeripheryUnit_Delay();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamPeripheryUnit#getType()
	 * @see #getTamPeripheryUnit()
	 * @generated
	 */
	EReference getTamPeripheryUnit_Type();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_ControlUnit.TamSensor <em>Tam Sensor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Sensor</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamSensor
	 * @generated
	 */
	EClass getTamSensor();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_ControlUnit.TamIO <em>Tam IO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam IO</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamIO
	 * @generated
	 */
	EClass getTamIO();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_ControlUnit.TamActuator <em>Tam Actuator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Actuator</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamActuator
	 * @generated
	 */
	EClass getTamActuator();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU <em>Tam ECU</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam ECU</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamECU
	 * @generated
	 */
	EClass getTamECU();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getProcessingUnit <em>Processing Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Processing Unit</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamECU#getProcessingUnit()
	 * @see #getTamECU()
	 * @generated
	 */
	EReference getTamECU_ProcessingUnit();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getMemory <em>Memory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Memory</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamECU#getMemory()
	 * @see #getTamECU()
	 * @generated
	 */
	EReference getTamECU_Memory();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getNetworkInterfaces <em>Network Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Network Interfaces</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamECU#getNetworkInterfaces()
	 * @see #getTamECU()
	 * @generated
	 */
	EReference getTamECU_NetworkInterfaces();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getPeripherals <em>Peripherals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Peripherals</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamECU#getPeripherals()
	 * @see #getTamECU()
	 * @generated
	 */
	EReference getTamECU_Peripherals();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Platform.TAM_ControlUnit.TamECU#getRTOS <em>RTOS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>RTOS</em>'.
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TamECU#getRTOS()
	 * @see #getTamECU()
	 * @generated
	 */
	EReference getTamECU_RTOS();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TAM_ControlUnitFactory getTAM_ControlUnitFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl <em>Tam Memory Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamMemoryUnitImpl
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamMemoryUnit()
		 * @generated
		 */
		EClass TAM_MEMORY_UNIT = eINSTANCE.getTamMemoryUnit();

		/**
		 * The meta object literal for the '<em><b>Memory Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_MEMORY_UNIT__MEMORY_SIZE = eINSTANCE.getTamMemoryUnit_MemorySize();

		/**
		 * The meta object literal for the '<em><b>Throughput</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_MEMORY_UNIT__THROUGHPUT = eINSTANCE.getTamMemoryUnit_Throughput();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_MEMORY_UNIT__TYPE = eINSTANCE.getTamMemoryUnit_Type();

		/**
		 * The meta object literal for the '<em><b>Access Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_MEMORY_UNIT__ACCESS_DELAY = eINSTANCE.getTamMemoryUnit_AccessDelay();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamProcessingUnitImpl <em>Tam Processing Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamProcessingUnitImpl
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamProcessingUnit()
		 * @generated
		 */
		EClass TAM_PROCESSING_UNIT = eINSTANCE.getTamProcessingUnit();

		/**
		 * The meta object literal for the '<em><b>Num Cores</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_PROCESSING_UNIT__NUM_CORES = eINSTANCE.getTamProcessingUnit_NumCores();

		/**
		 * The meta object literal for the '<em><b>Core Sync Ovhd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_PROCESSING_UNIT__CORE_SYNC_OVHD = eINSTANCE.getTamProcessingUnit_CoreSyncOvhd();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamPeripheryUnitImpl <em>Tam Periphery Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamPeripheryUnitImpl
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamPeripheryUnit()
		 * @generated
		 */
		EClass TAM_PERIPHERY_UNIT = eINSTANCE.getTamPeripheryUnit();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_PERIPHERY_UNIT__DELAY = eINSTANCE.getTamPeripheryUnit_Delay();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_PERIPHERY_UNIT__TYPE = eINSTANCE.getTamPeripheryUnit_Type();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamSensorImpl <em>Tam Sensor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamSensorImpl
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamSensor()
		 * @generated
		 */
		EClass TAM_SENSOR = eINSTANCE.getTamSensor();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamIOImpl <em>Tam IO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamIOImpl
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamIO()
		 * @generated
		 */
		EClass TAM_IO = eINSTANCE.getTamIO();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamActuatorImpl <em>Tam Actuator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamActuatorImpl
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamActuator()
		 * @generated
		 */
		EClass TAM_ACTUATOR = eINSTANCE.getTamActuator();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl <em>Tam ECU</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TamECUImpl
		 * @see TAM.TAM_Platform.TAM_ControlUnit.impl.TAM_ControlUnitPackageImpl#getTamECU()
		 * @generated
		 */
		EClass TAM_ECU = eINSTANCE.getTamECU();

		/**
		 * The meta object literal for the '<em><b>Processing Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_ECU__PROCESSING_UNIT = eINSTANCE.getTamECU_ProcessingUnit();

		/**
		 * The meta object literal for the '<em><b>Memory</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_ECU__MEMORY = eINSTANCE.getTamECU_Memory();

		/**
		 * The meta object literal for the '<em><b>Network Interfaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_ECU__NETWORK_INTERFACES = eINSTANCE.getTamECU_NetworkInterfaces();

		/**
		 * The meta object literal for the '<em><b>Peripherals</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_ECU__PERIPHERALS = eINSTANCE.getTamECU_Peripherals();

		/**
		 * The meta object literal for the '<em><b>RTOS</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_ECU__RTOS = eINSTANCE.getTamECU_RTOS();

	}

} //TAM_ControlUnitPackage
