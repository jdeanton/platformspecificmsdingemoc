/**
 */
package TAM.TAM_Task;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see TAM.TAM_Task.TAM_TaskFactory
 * @model kind="package"
 * @generated
 */
public interface TAM_TaskPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TAM_Task";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///TAM/TAM_Task.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "TAM.TAM_Task";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TAM_TaskPackage eINSTANCE = TAM.TAM_Task.impl.TAM_TaskPackageImpl.init();

	/**
	 * The meta object id for the '{@link TAM.TAM_Task.impl.TamMemReqImpl <em>Tam Mem Req</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Task.impl.TamMemReqImpl
	 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamMemReq()
	 * @generated
	 */
	int TAM_MEM_REQ = 0;

	/**
	 * The feature id for the '<em><b>Exec Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__EXEC_TIME = GRMPackage.RESOURCE_USAGE__EXEC_TIME;

	/**
	 * The feature id for the '<em><b>Allocated Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__ALLOCATED_MEMORY = GRMPackage.RESOURCE_USAGE__ALLOCATED_MEMORY;

	/**
	 * The feature id for the '<em><b>Used Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__USED_MEMORY = GRMPackage.RESOURCE_USAGE__USED_MEMORY;

	/**
	 * The feature id for the '<em><b>Power Peak</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__POWER_PEAK = GRMPackage.RESOURCE_USAGE__POWER_PEAK;

	/**
	 * The feature id for the '<em><b>Energy</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__ENERGY = GRMPackage.RESOURCE_USAGE__ENERGY;

	/**
	 * The feature id for the '<em><b>Base Named Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__BASE_NAMED_ELEMENT = GRMPackage.RESOURCE_USAGE__BASE_NAMED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Sub Usage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__SUB_USAGE = GRMPackage.RESOURCE_USAGE__SUB_USAGE;

	/**
	 * The feature id for the '<em><b>Used Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__USED_RESOURCES = GRMPackage.RESOURCE_USAGE__USED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Msg Size</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__MSG_SIZE = GRMPackage.RESOURCE_USAGE__MSG_SIZE;

	/**
	 * The feature id for the '<em><b>Access Freq</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__ACCESS_FREQ = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transf Mem</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__TRANSF_MEM = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Mem</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ__MEM = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Tam Mem Req</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ_FEATURE_COUNT = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Tam Mem Req</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_REQ_OPERATION_COUNT = GRMPackage.RESOURCE_USAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Task.impl.TamInterfaceImplementationImpl <em>Tam Interface Implementation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Task.impl.TamInterfaceImplementationImpl
	 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamInterfaceImplementation()
	 * @generated
	 */
	int TAM_INTERFACE_IMPLEMENTATION = 1;

	/**
	 * The feature id for the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_INTERFACE_IMPLEMENTATION__BASE_PORT = 0;

	/**
	 * The feature id for the '<em><b>Impl Exec Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD = 1;

	/**
	 * The feature id for the '<em><b>Impl Mem Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD = 2;

	/**
	 * The number of structural features of the '<em>Tam Interface Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_INTERFACE_IMPLEMENTATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Tam Interface Implementation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_INTERFACE_IMPLEMENTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Task.impl.TamPeripheryTypeImpl <em>Tam Periphery Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Task.impl.TamPeripheryTypeImpl
	 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamPeripheryType()
	 * @generated
	 */
	int TAM_PERIPHERY_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_TYPE__BASE_CLASSIFIER = 0;

	/**
	 * The number of structural features of the '<em>Tam Periphery Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Tam Periphery Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_PERIPHERY_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Task.impl.TamSharedOSResourcTypeImpl <em>Tam Shared OS Resourc Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Task.impl.TamSharedOSResourcTypeImpl
	 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamSharedOSResourcType()
	 * @generated
	 */
	int TAM_SHARED_OS_RESOURC_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURC_TYPE__BASE_CLASSIFIER = 0;

	/**
	 * The number of structural features of the '<em>Tam Shared OS Resourc Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURC_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Tam Shared OS Resourc Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_SHARED_OS_RESOURC_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Task.impl.TamResourceAccessImpl <em>Tam Resource Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Task.impl.TamResourceAccessImpl
	 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamResourceAccess()
	 * @generated
	 */
	int TAM_RESOURCE_ACCESS = 4;

	/**
	 * The feature id for the '<em><b>Exec Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__EXEC_TIME = GRMPackage.RESOURCE_USAGE__EXEC_TIME;

	/**
	 * The feature id for the '<em><b>Allocated Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__ALLOCATED_MEMORY = GRMPackage.RESOURCE_USAGE__ALLOCATED_MEMORY;

	/**
	 * The feature id for the '<em><b>Used Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__USED_MEMORY = GRMPackage.RESOURCE_USAGE__USED_MEMORY;

	/**
	 * The feature id for the '<em><b>Power Peak</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__POWER_PEAK = GRMPackage.RESOURCE_USAGE__POWER_PEAK;

	/**
	 * The feature id for the '<em><b>Energy</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__ENERGY = GRMPackage.RESOURCE_USAGE__ENERGY;

	/**
	 * The feature id for the '<em><b>Base Named Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__BASE_NAMED_ELEMENT = GRMPackage.RESOURCE_USAGE__BASE_NAMED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Sub Usage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__SUB_USAGE = GRMPackage.RESOURCE_USAGE__SUB_USAGE;

	/**
	 * The feature id for the '<em><b>Used Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__USED_RESOURCES = GRMPackage.RESOURCE_USAGE__USED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Msg Size</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__MSG_SIZE = GRMPackage.RESOURCE_USAGE__MSG_SIZE;

	/**
	 * The feature id for the '<em><b>Num Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__NUM_ACCESS = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Per Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__PER_RESOURCE = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Os Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS__OS_RESOURCE = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Tam Resource Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS_FEATURE_COUNT = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Tam Resource Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_RESOURCE_ACCESS_OPERATION_COUNT = GRMPackage.RESOURCE_USAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Task.impl.TamOperationImpl <em>Tam Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Task.impl.TamOperationImpl
	 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamOperation()
	 * @generated
	 */
	int TAM_OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Exec Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__EXEC_TIME = GRMPackage.RESOURCE_USAGE__EXEC_TIME;

	/**
	 * The feature id for the '<em><b>Allocated Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__ALLOCATED_MEMORY = GRMPackage.RESOURCE_USAGE__ALLOCATED_MEMORY;

	/**
	 * The feature id for the '<em><b>Used Memory</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__USED_MEMORY = GRMPackage.RESOURCE_USAGE__USED_MEMORY;

	/**
	 * The feature id for the '<em><b>Power Peak</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__POWER_PEAK = GRMPackage.RESOURCE_USAGE__POWER_PEAK;

	/**
	 * The feature id for the '<em><b>Energy</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__ENERGY = GRMPackage.RESOURCE_USAGE__ENERGY;

	/**
	 * The feature id for the '<em><b>Base Named Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__BASE_NAMED_ELEMENT = GRMPackage.RESOURCE_USAGE__BASE_NAMED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Sub Usage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__SUB_USAGE = GRMPackage.RESOURCE_USAGE__SUB_USAGE;

	/**
	 * The feature id for the '<em><b>Used Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__USED_RESOURCES = GRMPackage.RESOURCE_USAGE__USED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Msg Size</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__MSG_SIZE = GRMPackage.RESOURCE_USAGE__MSG_SIZE;

	/**
	 * The feature id for the '<em><b>Mem Req</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__MEM_REQ = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Res Access</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION__RES_ACCESS = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tam Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION_FEATURE_COUNT = GRMPackage.RESOURCE_USAGE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tam Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_OPERATION_OPERATION_COUNT = GRMPackage.RESOURCE_USAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link TAM.TAM_Task.impl.TamMemTypeImpl <em>Tam Mem Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TAM.TAM_Task.impl.TamMemTypeImpl
	 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamMemType()
	 * @generated
	 */
	int TAM_MEM_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Base Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_TYPE__BASE_CLASSIFIER = 0;

	/**
	 * The number of structural features of the '<em>Tam Mem Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Tam Mem Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAM_MEM_TYPE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link TAM.TAM_Task.TamMemReq <em>Tam Mem Req</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Mem Req</em>'.
	 * @see TAM.TAM_Task.TamMemReq
	 * @generated
	 */
	EClass getTamMemReq();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Task.TamMemReq#getAccessFreq <em>Access Freq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Access Freq</em>'.
	 * @see TAM.TAM_Task.TamMemReq#getAccessFreq()
	 * @see #getTamMemReq()
	 * @generated
	 */
	EAttribute getTamMemReq_AccessFreq();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Task.TamMemReq#getTransfMem <em>Transf Mem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transf Mem</em>'.
	 * @see TAM.TAM_Task.TamMemReq#getTransfMem()
	 * @see #getTamMemReq()
	 * @generated
	 */
	EAttribute getTamMemReq_TransfMem();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Task.TamMemReq#getMem <em>Mem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mem</em>'.
	 * @see TAM.TAM_Task.TamMemReq#getMem()
	 * @see #getTamMemReq()
	 * @generated
	 */
	EReference getTamMemReq_Mem();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Task.TamInterfaceImplementation <em>Tam Interface Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Interface Implementation</em>'.
	 * @see TAM.TAM_Task.TamInterfaceImplementation
	 * @generated
	 */
	EClass getTamInterfaceImplementation();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Task.TamInterfaceImplementation#getBase_Port <em>Base Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Port</em>'.
	 * @see TAM.TAM_Task.TamInterfaceImplementation#getBase_Port()
	 * @see #getTamInterfaceImplementation()
	 * @generated
	 */
	EReference getTamInterfaceImplementation_Base_Port();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Task.TamInterfaceImplementation#getImplExecOvhd <em>Impl Exec Ovhd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impl Exec Ovhd</em>'.
	 * @see TAM.TAM_Task.TamInterfaceImplementation#getImplExecOvhd()
	 * @see #getTamInterfaceImplementation()
	 * @generated
	 */
	EAttribute getTamInterfaceImplementation_ImplExecOvhd();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Task.TamInterfaceImplementation#getImplMemOvhd <em>Impl Mem Ovhd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impl Mem Ovhd</em>'.
	 * @see TAM.TAM_Task.TamInterfaceImplementation#getImplMemOvhd()
	 * @see #getTamInterfaceImplementation()
	 * @generated
	 */
	EAttribute getTamInterfaceImplementation_ImplMemOvhd();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Task.TamPeripheryType <em>Tam Periphery Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Periphery Type</em>'.
	 * @see TAM.TAM_Task.TamPeripheryType
	 * @generated
	 */
	EClass getTamPeripheryType();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Task.TamPeripheryType#getBase_Classifier <em>Base Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Classifier</em>'.
	 * @see TAM.TAM_Task.TamPeripheryType#getBase_Classifier()
	 * @see #getTamPeripheryType()
	 * @generated
	 */
	EReference getTamPeripheryType_Base_Classifier();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Task.TamSharedOSResourcType <em>Tam Shared OS Resourc Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Shared OS Resourc Type</em>'.
	 * @see TAM.TAM_Task.TamSharedOSResourcType
	 * @generated
	 */
	EClass getTamSharedOSResourcType();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Task.TamSharedOSResourcType#getBase_Classifier <em>Base Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Classifier</em>'.
	 * @see TAM.TAM_Task.TamSharedOSResourcType#getBase_Classifier()
	 * @see #getTamSharedOSResourcType()
	 * @generated
	 */
	EReference getTamSharedOSResourcType_Base_Classifier();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Task.TamResourceAccess <em>Tam Resource Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Resource Access</em>'.
	 * @see TAM.TAM_Task.TamResourceAccess
	 * @generated
	 */
	EClass getTamResourceAccess();

	/**
	 * Returns the meta object for the attribute '{@link TAM.TAM_Task.TamResourceAccess#getNumAccess <em>Num Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Access</em>'.
	 * @see TAM.TAM_Task.TamResourceAccess#getNumAccess()
	 * @see #getTamResourceAccess()
	 * @generated
	 */
	EAttribute getTamResourceAccess_NumAccess();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Task.TamResourceAccess#getPerResource <em>Per Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Per Resource</em>'.
	 * @see TAM.TAM_Task.TamResourceAccess#getPerResource()
	 * @see #getTamResourceAccess()
	 * @generated
	 */
	EReference getTamResourceAccess_PerResource();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Task.TamResourceAccess#getOsResource <em>Os Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Os Resource</em>'.
	 * @see TAM.TAM_Task.TamResourceAccess#getOsResource()
	 * @see #getTamResourceAccess()
	 * @generated
	 */
	EReference getTamResourceAccess_OsResource();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Task.TamOperation <em>Tam Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Operation</em>'.
	 * @see TAM.TAM_Task.TamOperation
	 * @generated
	 */
	EClass getTamOperation();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Task.TamOperation#getMemReq <em>Mem Req</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Mem Req</em>'.
	 * @see TAM.TAM_Task.TamOperation#getMemReq()
	 * @see #getTamOperation()
	 * @generated
	 */
	EReference getTamOperation_MemReq();

	/**
	 * Returns the meta object for the reference list '{@link TAM.TAM_Task.TamOperation#getResAccess <em>Res Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Res Access</em>'.
	 * @see TAM.TAM_Task.TamOperation#getResAccess()
	 * @see #getTamOperation()
	 * @generated
	 */
	EReference getTamOperation_ResAccess();

	/**
	 * Returns the meta object for class '{@link TAM.TAM_Task.TamMemType <em>Tam Mem Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tam Mem Type</em>'.
	 * @see TAM.TAM_Task.TamMemType
	 * @generated
	 */
	EClass getTamMemType();

	/**
	 * Returns the meta object for the reference '{@link TAM.TAM_Task.TamMemType#getBase_Classifier <em>Base Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Classifier</em>'.
	 * @see TAM.TAM_Task.TamMemType#getBase_Classifier()
	 * @see #getTamMemType()
	 * @generated
	 */
	EReference getTamMemType_Base_Classifier();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TAM_TaskFactory getTAM_TaskFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link TAM.TAM_Task.impl.TamMemReqImpl <em>Tam Mem Req</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Task.impl.TamMemReqImpl
		 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamMemReq()
		 * @generated
		 */
		EClass TAM_MEM_REQ = eINSTANCE.getTamMemReq();

		/**
		 * The meta object literal for the '<em><b>Access Freq</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_MEM_REQ__ACCESS_FREQ = eINSTANCE.getTamMemReq_AccessFreq();

		/**
		 * The meta object literal for the '<em><b>Transf Mem</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_MEM_REQ__TRANSF_MEM = eINSTANCE.getTamMemReq_TransfMem();

		/**
		 * The meta object literal for the '<em><b>Mem</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_MEM_REQ__MEM = eINSTANCE.getTamMemReq_Mem();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Task.impl.TamInterfaceImplementationImpl <em>Tam Interface Implementation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Task.impl.TamInterfaceImplementationImpl
		 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamInterfaceImplementation()
		 * @generated
		 */
		EClass TAM_INTERFACE_IMPLEMENTATION = eINSTANCE.getTamInterfaceImplementation();

		/**
		 * The meta object literal for the '<em><b>Base Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_INTERFACE_IMPLEMENTATION__BASE_PORT = eINSTANCE.getTamInterfaceImplementation_Base_Port();

		/**
		 * The meta object literal for the '<em><b>Impl Exec Ovhd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD = eINSTANCE.getTamInterfaceImplementation_ImplExecOvhd();

		/**
		 * The meta object literal for the '<em><b>Impl Mem Ovhd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD = eINSTANCE.getTamInterfaceImplementation_ImplMemOvhd();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Task.impl.TamPeripheryTypeImpl <em>Tam Periphery Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Task.impl.TamPeripheryTypeImpl
		 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamPeripheryType()
		 * @generated
		 */
		EClass TAM_PERIPHERY_TYPE = eINSTANCE.getTamPeripheryType();

		/**
		 * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_PERIPHERY_TYPE__BASE_CLASSIFIER = eINSTANCE.getTamPeripheryType_Base_Classifier();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Task.impl.TamSharedOSResourcTypeImpl <em>Tam Shared OS Resourc Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Task.impl.TamSharedOSResourcTypeImpl
		 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamSharedOSResourcType()
		 * @generated
		 */
		EClass TAM_SHARED_OS_RESOURC_TYPE = eINSTANCE.getTamSharedOSResourcType();

		/**
		 * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_SHARED_OS_RESOURC_TYPE__BASE_CLASSIFIER = eINSTANCE.getTamSharedOSResourcType_Base_Classifier();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Task.impl.TamResourceAccessImpl <em>Tam Resource Access</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Task.impl.TamResourceAccessImpl
		 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamResourceAccess()
		 * @generated
		 */
		EClass TAM_RESOURCE_ACCESS = eINSTANCE.getTamResourceAccess();

		/**
		 * The meta object literal for the '<em><b>Num Access</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAM_RESOURCE_ACCESS__NUM_ACCESS = eINSTANCE.getTamResourceAccess_NumAccess();

		/**
		 * The meta object literal for the '<em><b>Per Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RESOURCE_ACCESS__PER_RESOURCE = eINSTANCE.getTamResourceAccess_PerResource();

		/**
		 * The meta object literal for the '<em><b>Os Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_RESOURCE_ACCESS__OS_RESOURCE = eINSTANCE.getTamResourceAccess_OsResource();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Task.impl.TamOperationImpl <em>Tam Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Task.impl.TamOperationImpl
		 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamOperation()
		 * @generated
		 */
		EClass TAM_OPERATION = eINSTANCE.getTamOperation();

		/**
		 * The meta object literal for the '<em><b>Mem Req</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_OPERATION__MEM_REQ = eINSTANCE.getTamOperation_MemReq();

		/**
		 * The meta object literal for the '<em><b>Res Access</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_OPERATION__RES_ACCESS = eINSTANCE.getTamOperation_ResAccess();

		/**
		 * The meta object literal for the '{@link TAM.TAM_Task.impl.TamMemTypeImpl <em>Tam Mem Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TAM.TAM_Task.impl.TamMemTypeImpl
		 * @see TAM.TAM_Task.impl.TAM_TaskPackageImpl#getTamMemType()
		 * @generated
		 */
		EClass TAM_MEM_TYPE = eINSTANCE.getTamMemType();

		/**
		 * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAM_MEM_TYPE__BASE_CLASSIFIER = eINSTANCE.getTamMemType_Base_Classifier();

	}

} //TAM_TaskPackage
