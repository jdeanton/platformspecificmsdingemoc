/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;

import TAM.TAM_Task.TamMemType;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.StorageResource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Memory Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getMemorySize <em>Memory Size</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getThroughput <em>Throughput</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getType <em>Type</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getAccessDelay <em>Access Delay</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamMemoryUnit()
 * @model
 * @generated
 */
public interface TamMemoryUnit extends StorageResource {
	/**
	 * Returns the value of the '<em><b>Memory Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memory Size</em>' attribute.
	 * @see #setMemorySize(String)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamMemoryUnit_MemorySize()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getMemorySize();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getMemorySize <em>Memory Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memory Size</em>' attribute.
	 * @see #getMemorySize()
	 * @generated
	 */
	void setMemorySize(String value);

	/**
	 * Returns the value of the '<em><b>Throughput</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Throughput</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Throughput</em>' attribute.
	 * @see #setThroughput(String)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamMemoryUnit_Throughput()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getThroughput();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getThroughput <em>Throughput</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Throughput</em>' attribute.
	 * @see #getThroughput()
	 * @generated
	 */
	void setThroughput(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(TamMemType)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamMemoryUnit_Type()
	 * @model ordered="false"
	 * @generated
	 */
	TamMemType getType();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(TamMemType value);

	/**
	 * Returns the value of the '<em><b>Access Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Delay</em>' attribute.
	 * @see #setAccessDelay(String)
	 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamMemoryUnit_AccessDelay()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getAccessDelay();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_ControlUnit.TamMemoryUnit#getAccessDelay <em>Access Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Delay</em>' attribute.
	 * @see #getAccessDelay()
	 * @generated
	 */
	void setAccessDelay(String value);

} // TamMemoryUnit
