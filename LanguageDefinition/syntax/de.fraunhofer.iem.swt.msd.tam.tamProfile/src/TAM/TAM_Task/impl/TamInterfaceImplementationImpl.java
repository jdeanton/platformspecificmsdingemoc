/**
 */
package TAM.TAM_Task.impl;

import TAM.TAM_Task.TAM_TaskPackage;
import TAM.TAM_Task.TamInterfaceImplementation;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.uml2.uml.Port;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tam Interface Implementation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Task.impl.TamInterfaceImplementationImpl#getBase_Port <em>Base Port</em>}</li>
 *   <li>{@link TAM.TAM_Task.impl.TamInterfaceImplementationImpl#getImplExecOvhd <em>Impl Exec Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Task.impl.TamInterfaceImplementationImpl#getImplMemOvhd <em>Impl Mem Ovhd</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TamInterfaceImplementationImpl extends MinimalEObjectImpl.Container implements TamInterfaceImplementation {
	/**
	 * The cached value of the '{@link #getBase_Port() <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Port()
	 * @generated
	 * @ordered
	 */
	protected Port base_Port;

	/**
	 * The default value of the '{@link #getImplExecOvhd() <em>Impl Exec Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplExecOvhd()
	 * @generated
	 * @ordered
	 */
	protected static final String IMPL_EXEC_OVHD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImplExecOvhd() <em>Impl Exec Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplExecOvhd()
	 * @generated
	 * @ordered
	 */
	protected String implExecOvhd = IMPL_EXEC_OVHD_EDEFAULT;

	/**
	 * The default value of the '{@link #getImplMemOvhd() <em>Impl Mem Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplMemOvhd()
	 * @generated
	 * @ordered
	 */
	protected static final String IMPL_MEM_OVHD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImplMemOvhd() <em>Impl Mem Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplMemOvhd()
	 * @generated
	 * @ordered
	 */
	protected String implMemOvhd = IMPL_MEM_OVHD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TamInterfaceImplementationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TAM_TaskPackage.Literals.TAM_INTERFACE_IMPLEMENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getBase_Port() {
		if (base_Port != null && base_Port.eIsProxy()) {
			InternalEObject oldBase_Port = (InternalEObject)base_Port;
			base_Port = (Port)eResolveProxy(oldBase_Port);
			if (base_Port != oldBase_Port) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__BASE_PORT, oldBase_Port, base_Port));
			}
		}
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetBase_Port() {
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Port(Port newBase_Port) {
		Port oldBase_Port = base_Port;
		base_Port = newBase_Port;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__BASE_PORT, oldBase_Port, base_Port));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImplExecOvhd() {
		return implExecOvhd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplExecOvhd(String newImplExecOvhd) {
		String oldImplExecOvhd = implExecOvhd;
		implExecOvhd = newImplExecOvhd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD, oldImplExecOvhd, implExecOvhd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImplMemOvhd() {
		return implMemOvhd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplMemOvhd(String newImplMemOvhd) {
		String oldImplMemOvhd = implMemOvhd;
		implMemOvhd = newImplMemOvhd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD, oldImplMemOvhd, implMemOvhd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__BASE_PORT:
				if (resolve) return getBase_Port();
				return basicGetBase_Port();
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD:
				return getImplExecOvhd();
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD:
				return getImplMemOvhd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__BASE_PORT:
				setBase_Port((Port)newValue);
				return;
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD:
				setImplExecOvhd((String)newValue);
				return;
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD:
				setImplMemOvhd((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__BASE_PORT:
				setBase_Port((Port)null);
				return;
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD:
				setImplExecOvhd(IMPL_EXEC_OVHD_EDEFAULT);
				return;
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD:
				setImplMemOvhd(IMPL_MEM_OVHD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__BASE_PORT:
				return base_Port != null;
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_EXEC_OVHD:
				return IMPL_EXEC_OVHD_EDEFAULT == null ? implExecOvhd != null : !IMPL_EXEC_OVHD_EDEFAULT.equals(implExecOvhd);
			case TAM_TaskPackage.TAM_INTERFACE_IMPLEMENTATION__IMPL_MEM_OVHD:
				return IMPL_MEM_OVHD_EDEFAULT == null ? implMemOvhd != null : !IMPL_MEM_OVHD_EDEFAULT.equals(implMemOvhd);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (implExecOvhd: ");
		result.append(implExecOvhd);
		result.append(", implMemOvhd: ");
		result.append(implMemOvhd);
		result.append(')');
		return result.toString();
	}

} //TamInterfaceImplementationImpl
