package de.fraunhofer.iem.swt.msd.tam.preprocessing.blackBox;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.NamedElement;

public class TamBlackBox {
	
	public TamBlackBox() {
		super();
	}

	public static void attachMessage(NamedElement sysMsg, Message msg){
		((EObjectResolvingEList) sysMsg.getValue(sysMsg.getAppliedStereotype("TAM_SimulationProfile::SystemMessage"), "messages")).add(msg);
	}
	
	public static void addExclusiveResource(NamedElement sysMsg, Element resouce){
		((EObjectResolvingEList) sysMsg.getValue(sysMsg.getAppliedStereotype("TAM_SimulationProfile::SystemMessage"), "exclusiveResource")).add(resouce);
	}
	
	public static void addResource(NamedElement sysMsg, Element resouce){
		((EObjectResolvingEList) sysMsg.getValue(sysMsg.getAppliedStereotype("TAM_SimulationProfile::SystemMessage"), "requiredResources")).add(resouce);
	}
	
	public static void addHigherPrio(NamedElement sysMsg, NamedElement higherPrio){
		((EObjectResolvingEList) sysMsg.getValue(sysMsg.getAppliedStereotype("TAM_SimulationProfile::SystemMessage"), "higherPrio")).add(higherPrio);
	}
	
	public static void addLowerPrio(NamedElement sysMsg, NamedElement lowerPrio){
		((EObjectResolvingEList) sysMsg.getValue(sysMsg.getAppliedStereotype("TAM_SimulationProfile::SystemMessage"), "lowerPrio")).add(lowerPrio);
	}
	//public static void addTamCon(Element unificationSet, Message msg){
	//	((EObjectResolvingEList) unificationSet.getValue(unificationSet.getAppliedStereotype("TAM_SimulationProfile::UnificationSet"), "messages")).add(msg);
	//}
}
