/**
 */
package TAM.TAM_Platform.TAM_Communication;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaCommChannel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Protocol</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getTransmOvhd <em>Transm Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getMsgCtrlOvhd <em>Msg Ctrl Ovhd</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommTxT <em>Comm Tx T</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommRcvT <em>Comm Rcv T</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamProtocol()
 * @model
 * @generated
 */
public interface TamProtocol extends GaCommChannel {
	/**
	 * Returns the value of the '<em><b>Transm Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transm Ovhd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transm Ovhd</em>' attribute.
	 * @see #setTransmOvhd(String)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamProtocol_TransmOvhd()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getTransmOvhd();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getTransmOvhd <em>Transm Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transm Ovhd</em>' attribute.
	 * @see #getTransmOvhd()
	 * @generated
	 */
	void setTransmOvhd(String value);

	/**
	 * Returns the value of the '<em><b>Msg Ctrl Ovhd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msg Ctrl Ovhd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msg Ctrl Ovhd</em>' attribute.
	 * @see #setMsgCtrlOvhd(String)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamProtocol_MsgCtrlOvhd()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getMsgCtrlOvhd();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getMsgCtrlOvhd <em>Msg Ctrl Ovhd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msg Ctrl Ovhd</em>' attribute.
	 * @see #getMsgCtrlOvhd()
	 * @generated
	 */
	void setMsgCtrlOvhd(String value);

	/**
	 * Returns the value of the '<em><b>Comm Tx T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comm Tx T</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comm Tx T</em>' attribute.
	 * @see #setCommTxT(String)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamProtocol_CommTxT()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getCommTxT();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommTxT <em>Comm Tx T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comm Tx T</em>' attribute.
	 * @see #getCommTxT()
	 * @generated
	 */
	void setCommTxT(String value);

	/**
	 * Returns the value of the '<em><b>Comm Rcv T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comm Rcv T</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comm Rcv T</em>' attribute.
	 * @see #setCommRcvT(String)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamProtocol_CommRcvT()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getCommRcvT();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamProtocol#getCommRcvT <em>Comm Rcv T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comm Rcv T</em>' attribute.
	 * @see #getCommRcvT()
	 * @generated
	 */
	void setCommRcvT(String value);

} // TamProtocol
