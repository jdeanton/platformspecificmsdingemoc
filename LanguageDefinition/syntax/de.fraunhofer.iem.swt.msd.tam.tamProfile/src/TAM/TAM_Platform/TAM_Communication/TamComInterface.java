/**
 */
package TAM.TAM_Platform.TAM_Communication;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaExecHost;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.TimerResource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Com Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComInterface#getArbitrationT <em>Arbitration T</em>}</li>
 *   <li>{@link TAM.TAM_Platform.TAM_Communication.TamComInterface#getConnection <em>Connection</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComInterface()
 * @model
 * @generated
 */
public interface TamComInterface extends GaExecHost, TimerResource {
	/**
	 * Returns the value of the '<em><b>Arbitration T</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arbitration T</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arbitration T</em>' attribute.
	 * @see #setArbitrationT(String)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComInterface_ArbitrationT()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getArbitrationT();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamComInterface#getArbitrationT <em>Arbitration T</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arbitration T</em>' attribute.
	 * @see #getArbitrationT()
	 * @generated
	 */
	void setArbitrationT(String value);

	/**
	 * Returns the value of the '<em><b>Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection</em>' reference.
	 * @see #setConnection(TamComConnection)
	 * @see TAM.TAM_Platform.TAM_Communication.TAM_CommunicationPackage#getTamComInterface_Connection()
	 * @model ordered="false"
	 * @generated
	 */
	TamComConnection getConnection();

	/**
	 * Sets the value of the '{@link TAM.TAM_Platform.TAM_Communication.TamComInterface#getConnection <em>Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection</em>' reference.
	 * @see #getConnection()
	 * @generated
	 */
	void setConnection(TamComConnection value);

} // TamComInterface
