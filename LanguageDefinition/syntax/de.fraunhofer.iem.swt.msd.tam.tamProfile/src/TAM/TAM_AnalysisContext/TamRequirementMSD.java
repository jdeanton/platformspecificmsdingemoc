/**
 */
package TAM.TAM_AnalysisContext;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaScenario;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Requirement MSD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link TAM.TAM_AnalysisContext.TamRequirementMSD#getActivationCount <em>Activation Count</em>}</li>
 *   <li>{@link TAM.TAM_AnalysisContext.TamRequirementMSD#getConstraintsSatisfied <em>Constraints Satisfied</em>}</li>
 * </ul>
 *
 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamRequirementMSD()
 * @model
 * @generated
 */
public interface TamRequirementMSD extends GaScenario {
	/**
	 * Returns the value of the '<em><b>Activation Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activation Count</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activation Count</em>' attribute.
	 * @see #setActivationCount(String)
	 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamRequirementMSD_ActivationCount()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getActivationCount();

	/**
	 * Sets the value of the '{@link TAM.TAM_AnalysisContext.TamRequirementMSD#getActivationCount <em>Activation Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activation Count</em>' attribute.
	 * @see #getActivationCount()
	 * @generated
	 */
	void setActivationCount(String value);

	/**
	 * Returns the value of the '<em><b>Constraints Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints Satisfied</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints Satisfied</em>' attribute.
	 * @see #setConstraintsSatisfied(String)
	 * @see TAM.TAM_AnalysisContext.TAM_AnalysisContextPackage#getTamRequirementMSD_ConstraintsSatisfied()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getConstraintsSatisfied();

	/**
	 * Sets the value of the '{@link TAM.TAM_AnalysisContext.TamRequirementMSD#getConstraintsSatisfied <em>Constraints Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraints Satisfied</em>' attribute.
	 * @see #getConstraintsSatisfied()
	 * @generated
	 */
	void setConstraintsSatisfied(String value);

} // TamRequirementMSD
