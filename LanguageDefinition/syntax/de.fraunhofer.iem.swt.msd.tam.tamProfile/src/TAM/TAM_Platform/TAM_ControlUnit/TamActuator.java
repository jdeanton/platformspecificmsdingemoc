/**
 */
package TAM.TAM_Platform.TAM_ControlUnit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tam Actuator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see TAM.TAM_Platform.TAM_ControlUnit.TAM_ControlUnitPackage#getTamActuator()
 * @model
 * @generated
 */
public interface TamActuator extends TamPeripheryUnit {
} // TamActuator
